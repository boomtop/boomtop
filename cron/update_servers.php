<?php
#SLEEP-TIMEOUT 1
error_reporting(E_ALL);
ini_set('display_errors', 'On');
echo 'Start: ' . date('Y-m-d H:i:s') . PHP_EOL;
require dirname(__FILE__) . '/../configuration.php';

include ROOT . '/includes/class/mysql.php';
include ROOT . '/includes/functions.php';
include ROOT . '/includes/minecraftquery.php';
include ROOT . '/includes/server_functions.php';

$db = new database( DB_HOST, DB_USER, DB_PASS, DB_BASE );

$q = $db->query("SELECT * FROM `servers` WHERE `last_updated` < DATE_SUB(NOW(), INTERVAL 30 SECOND) ORDER BY `last_updated` ASC LIMIT 1");

if( !$q ){
    echo 'End: ' . date('Y-m-d H:i:s') . PHP_EOL;
    exit;
}

$server = $db->fetch($q);

if( !$server ){
    echo 'End: ' . date('Y-m-d H:i:s') . PHP_EOL;
    exit;
}

$db->query("UPDATE `servers` SET `last_updated` = NOW() WHERE `id` = " . (int)$server['id']);

switch( strtolower($server['type']) ){
    case 'cs':
    case 'csgo':
        echo 'Updating ' . $server['type'] . ' ' . $server['hostname'] . ':' . $server['port'] . PHP_EOL;;
        try{
            $serverData = query_live('source', $server['hostname'], $server['port'], 'sp');
            $status = update_server_data( $server['id'], $serverData);
        }catch(Exception $e ){
            echo 'ERROR: ' . $e->getMessage() . PHP_EOL;
            $status = false;
        }
        echo 'Update status: ' . ($status ? 'Success' : 'Failed') . PHP_EOL;
        break;
    case 'mc':
        echo 'Updating ' . $server['type'] . ' ' . $server['hostname'] . ':' . $server['port'] . PHP_EOL;;
        $Query = new MinecraftQuery();

        try{
            $Query->Connect( $server['hostname'], $server['port'], 1 );
            $Info = $Query->GetInfo();
            if( !empty( $Info ) )
            {
                $status = update_minecraft_server_data( $server['id'], $Info);
                echo 'Update status: ' . ($status ? 'Success' : 'Failed') . PHP_EOL;
            }
        }
        catch( MinecraftQueryException $e ){
            echo 'ERROR: ' . $e->getMessage() . PHP_EOL;
            $status = false;
        }
        break;
}
echo 'End: ' . date('Y-m-d H:i:s') . PHP_EOL;