<?php
require dirname(__FILE__) . '/../configuration.php';
	
include ROOT . '/includes/class/mysql.php';
	
$db = new database( DB_HOST, DB_USER, DB_PASS, DB_BASE );

if( date( 'd' ) == 01 )
{
	$db->query( 'UPDATE pages SET `out` = 0' );
	$db->query( 'UPDATE pages SET `in` = 0' );
	$db->query( 'UPDATE servers SET rating = 0' );
	$db->query( 'TRUNCATE TABLE in_log' );
	$db->query( 'TRUNCATE TABLE out_log' );
}
?>