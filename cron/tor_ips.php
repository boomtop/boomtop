<?php
require dirname(__FILE__) . '/../configuration.php';

include ROOT . '/includes/class/mysql.php';

$db = new database( DB_HOST, DB_USER, DB_PASS, DB_BASE );

$data = file_get_contents('https://www.dan.me.uk/torlist/');
$ips = explode("\n", $data);
foreach($ips as $ip)
{
    $db->query("INSERT IGNORE INTO `tor_ips` SET `ip` = " . ip2long($ip));
}