<?php
ini_set( 'display_errors', false );
ini_set( 'display_startup_errors', false );
error_reporting( false);

require "configuration.php";
include ROOT . '/includes/class/page.php';
include ROOT . "/includes/class/mysql.php";
$db = new database( DB_HOST, DB_USER, DB_PASS, DB_BASE );
$page = new page();
$id = (int)$_GET['id'];
if( intval( $id ) )
{
	$res = $db->query( "SELECT * FROM pages WHERE id = " . $id );
	$row = $db->fetch($res);
	
	if( $row['id'] )
	{
?>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<link rel="stylesheet" href="<?php echo BASE ?>/style/counter.css" type="text/css" />
<div onClick='window.open("<?php echo BASE ?>/vote/<?php echo $row['id']; ?>/");' class='counters'>
<?php
	if( $row['counter_style'] == 1 )
	{
		$query = $db->query( "SELECT * FROM out_log WHERE `page_id` = " . $row['id'] . " AND `time` >= " . strtotime( "Today" ) );
		$today_visits = $db->rows( $query );
?>
	<div class='counter_1' align='center'>
		<div class='visits'><?php echo $today_visits; ?></div>
		<div class='text'>Apmeklētāji</div>
	</div>
<?php
	}
	elseif( $row['counter_style'] == 2 )
	{
?>
	<div class='counter_2'>
		<div class='text'>
			<div class='left'><span>IN</span><b><?php echo $row['in']; ?></b></div>
			<div class='right'><span>OUT</span><b><?php echo $row['out']; ?></b></div>
		</div>
	</div>
<?php
	}
	elseif( $row['counter_style'] == 3 )
	{
?>
	<div class='counter_3'></div>
<?php
	}
?>
</div>
<?php
	}
}
?>