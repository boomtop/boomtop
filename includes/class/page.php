<?php
class page
{
  private $page_title;
  private $javascripts = array();
  private $stylesheets = array();
	
	
  function __construct($print=true)
  {
    $this->get_settings();
    $this->print_page = $print;
  }

  function LOAD_THEME($file)
  {
    $this->SITE_THEME_FILE = $file;
    $this->SITE_THEME_PATH = str_replace('/index.php', '', $this->SITE_THEME_FILE);
    $this->SITE_THEME_URL = str_replace(ROOT, BASE, $this->SITE_THEME_PATH);

  }
  function add_user_link($href, $title)
  {
    $this->user_links[] = array('url'=>$href, 'title'=>$title);
  }
  
  function get_settings()
  {
    global $db;
    $res = $db->query("SELECT * FROM `settings`");
    while($row=$db->fetch($res))
    {
      $this->settings[$row['setting']] = $row['value'];
    }
  }
  function get_setting($setting)
  {
    return $this->settings[$setting];
  }
  function need_login()
  {
	global $lang;
    if(!IS_USER)
    {
      $content = '<h1>' . $lang['need_login_title'] . '</h1>';
      $content .= error( $lang['need_login_text'] );
      $this->set_page_title( $lang['need_login_title'] );
      $this->set_page_content($content);
    }
  }
  
  function load_admin_page($page, $title, $path=false)
  {
    $path = $path ? $path : ROOT . "/pages/" . 'admin/' . $page . '.php';
    $this->admin_pages[$page] = array('title'=>$title, 'path'=>$path);
  }
  
  function add_javascript($url = true, $script = false)
  {
    if(!$script)
    {
      $this->javascripts[] = '<script type="text/javascript" src="' . $url . '"></script> ';
    }
    else
    {
    $this->javascripts[] = '<script type="text/javascript">' . $script . '</script>';
    }
  }
  
  function load_javascript()
  {
    foreach($this->javascripts as $script)
    {
      echo $script . "\n";
    }
  }
  function add_stylesheet($url, $media = false)
  {
      $this->stylesheets[] = '<link rel="stylesheet" href="' . $url . '" type="text/css" '.($media?'media="'.$media.'"':'').'  />';
  }
  function load_stylesheet()
  {
    foreach($this->stylesheets as $css)
    {
      echo $css . "\n";
    }
  }
  function set_page_content($content, $force = false)
  {
    if(!$this->page_content OR $force)
    {
      $this->page_content = $content;
    }
  }

  function __destruct()
  {   
    if($this->print_page)
    require $this->SITE_THEME_FILE;
  }
  
  function set_page_title($title, $force = false)
  {
    if(!$this->page_title OR $force)
      $this->page_title = $title;  
  }
  
  function load_panel($id,$title = 'Nosaukums', $file, $content = false)
  {
    $this->panels[$id] = array('title'=>$title, 'file'=>$file, 'content'=>$content);
  }
  function load_page($page, $array)
  {
		$this->site_pages[$page] = $array;
  }
  function load_slider($slider)
  {
    global $db;
    $res = $db->query('SELECT * FROM panels WHERE sidebar = ' . $db->esc($slider) . ' ORDER BY `order` ASC');
    $sliders = array();
    while($row=$db->fetch($res))
    {
      if(array_key_exists($row['panel_id'], $this->panels))
        $sliders[] = $this->panels[$row['panel_id']];
    }
    return $sliders;
  }
}

?>