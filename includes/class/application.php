<?php
class application
{
    public $path;
    public $url;
	
    function __construct($dir)
    {
        $this->path = ROOT . "/application/" . $dir;
        $this->url = str_replace(ROOT, BASE, $this->path);
    }
    
    function __destruct()
    {
        global $page, $db, $users;
        require $this->path . '/index.php';
    }
}

?>