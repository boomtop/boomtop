<?php
class database
{
  private $db_connection;
  public $query_count = 0;
  private $settings = array();
  private  $tables = array();
  function __construct($host, $user, $pass, $db)
  {
    $this->connect($host, $user, $pass);
    $this->select_db($db);
    $this->query("SET NAMES 'utf8'");
    $this->settings['db'] = $db;
    $res = $this->query('show tables');
    while($row=mysql_fetch_array($res))
    {
		$this->tables[] = $row[0];
    }
  }
  
  function select_db($db)
  {
    @mysql_select_db($db, $this->db_connection);
  }
  function connect($host, $user, $pass)
  {
    $this->db_connection = @mysql_connect($host, $user, $pass);
  }
  function query($query)
  {
    ++$this->query_count;
    $sql = @mysql_query($query, $this->db_connection);
    if($sql)
    {
      $this->add_query($query);
    }
    else
    {
      $this->add_query($query, mysql_error());
    }
    return $sql;
  }
  
  private function add_query($query, $error = false)
  {
    if($error)
    {
      $this->query_dead[] = array($query, $error);
    }
    else
    {
      $this->query_ok[] = array($query, false);
    }
  }
  
  public  function dbformat(&$val)
    {
      $val = '`'.$val.'`';
    }
  
  function insert_array($table, $array)
  {
    $fields = array();
    $values = array();
    foreach($array as $field => $value)
    {
      $fields[] = $field;
      if( $value instanceof stdClass && isset($value->value) ){
        $values[] = $value->value;
      }else{
        $values[] = $this->esc($value);
      }
    }
    array_walk($fields, array($this, 'dbformat'));
    $query = "INSERT INTO `$table` (". implode(", ", $fields) .") VALUES (".implode(", ", $values).")";
    if($this->query($query))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  
  function update_array($table, $array, $where = false)
  {
    $where = $where ? " WHERE $where" : '';
    $update = array();
    foreach($array as $field => $value)
    {
      $update[] = '`'.$field . '` = ' . $this->esc($value);
    }
    $query = "UPDATE `$table` SET " . implode(', ', $update) . $where;
    if($this->query($query))
    {
      return true;
    }
    else
    {
      return false;
    }

  }
  
  function table_exists($table)
  {
	return in_array($table, $this->tables);
  }

  function result($query)
  {
    return @mysql_result($query, 0);
  }
  
  function rows($query)
  {
    return @mysql_num_rows($query);
  }
  function last_id()
  {
    return @mysql_insert_id();
  }
  function fetch($query)
  {
    return @mysql_fetch_array($query, MYSQL_ASSOC);
  }
  function esc($value)
  {
    $value = htmlspecialchars($value);
    if (get_magic_quotes_gpc())
    {
      $value = stripslashes($value);
    }
    if(!is_numeric($value))
    {
      $value = "'" . mysql_real_escape_string($value) . "'";
    }
    return $value;
  }

  function count($table, $field, $where = false, $prefix = 'WHERE ')
  {
	$res = $this->query("SELECT count($field) as `count` FROM `$table` " . ($where ? $prefix . $where : ''));
	$row = $this->fetch($res);
	return $row['count'];
  }
  
  function __destruct()
  {
    @mysql_close($this->db_connection);
  }
  
}

?>