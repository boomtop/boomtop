<?php

abstract class suncore
{
	private static $_config = [];

	static function config_add($items)
	{
		self::$_config = array_merge(self::$_config, $items);
	}

	static function config($key)
	{
		$key_arr = explode("/", $key);

		if (empty($key_arr) === false) {
			$temp = self::$_config;

			foreach ($key_arr as $key_item) {
				if (array_key_exists($key_item, $temp)) {
					$temp = $temp[$key_item];
				} else {
					return false;
				}
			}

			return $temp;
		} else {
			if (array_key_exists($key, self::$_config)) {
				return self::$_config[$key];
			}
		}
	}

	static function result($code, $price, $payment_type)
	{
		global $page;

		return file_get_contents('http://authorize.digipay.lv/confirm.php?code=' . $code . '&id=' . $page->get_setting('suncore_client_id') . '&price=' . $price,
			false, null, 0, 1);
	}

	static function server_shop($server_id, $payment_type = "", $unlock_code = 0, $price)
	{
		global $db, $lang;

		if ($payment_type == 'sms' || $payment_type == 'paypal') {
			$answer = suncore::result($unlock_code, $price, $payment_type);

			if ($payment_type == 'sms') {
				$votes = [
					21 	=> 25,
					45 	=> 50,
					71 	=> 80,
					107 => 110,
					135	=> 135,
					180 => 170,
					215 => 215,
					285 => 270,
					427 => 380,
					711 => 550,
				];
			} elseif ($payment_type == 'paypal') {
				$votes = [
					100 => 45,
					200 => 95,
					400 => 210,
					600 => 350,
					800 => 500,
					1000 => 750,
				];
			}

			if ($answer === 'k') {
				$db->query('UPDATE servers SET `rating` = `rating` + ' . $votes[$price] . '  WHERE id = ' . $server_id);
				update_top();
				return [
					true,
					'Kods pieņemts, <font color="black">' . $votes[$price] . '</font> balsis tika pieskaitītas serverim!'
				];

			} else {
				return [
					false,
					'Kods netika pieņemts. Tas ir nederīgs, jeb jau iztērēts.'
				];
			}
		}

	}

	static function payment_check($page_id, $payment_type = "", $unlock_code = 0, $price)
	{
		global $db, $lang;
		if ($payment_type == 'sms' || $payment_type == 'paypal') {
			$answer = suncore::result($unlock_code, $price, $payment_type);

			if ($payment_type == 'sms') {
				$price_in = [
					14 	=> 15,
					21 	=> 25,
					45 	=> 50,
					71 	=> 80,
					107 => 110,
					135 => 135,
					180 => 170,
					215 => 215,
					285 => 270,
					427 => 380,
					711 => 550,
				];
			} elseif ($payment_type == 'paypal') {
				$price_in = [
					100 	=> 45,
					200 	=> 95,
					400 	=> 210,
					600 	=> 350,
					800		=> 500,
					1000	=> 750,
				];
			}

			if ($answer === 'k') {
				/*** Insert data ***/
				$in_data = [
					'page_id' => $page_id,
					'ip' => $_SERVER['REMOTE_ADDR'],
					'payment_type' => $payment_type,
					'unlock_code' => $unlock_code,
					'price' => $price,
					'in' => $price_in[$price],
					'date' => time(),
				];
				$db->insert_array('in_buyers', $in_data);

				/*** Update data ***/
				$db->query('UPDATE pages SET `in` = `in` + ' . $price_in[$price] . '  WHERE id = ' . $page_id);

				return [
					true,
					"" . $lang['code_accept-1'] . ", <span style='color:black;'>+$price_in[$price] IN</span> " . $lang['code_accept-2']
				];

			} else {
				return [
					false,
					$lang['code_failed']
				];
			}
		}

	}

	static function array_values_empty()
	{
		$args = func_get_args();
		$array = $args[0];
		unset($args[0]);


		if (is_array($args) === true) {
			foreach ($args as $value) {
				if (empty($array[$value]) === true) {
					return true;
				}
			}
		}

		return false;
	}

	static function post($key, $default = "")
	{
		if (isset($_POST[$key]) === true) {
			return $_POST[$key];
		}

		return $default;
	}


	static function selected($key, $selected_value)
	{
		if (self::post($key) == $selected_value) {
			return "selected='selected'";
		}

		return "";
	}
}