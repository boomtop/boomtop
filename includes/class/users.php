<?php
class users
{
  public $users = array();
  
  function __construct()
  {
    define( 'IS_USER' , $this->login_status() );
    if(IS_USER)
    {
      define( 'USER_ID' , (int)get_cookie('user_id'));
    }
    else
    {
      define( 'USER_ID', 0 );
    }
  }
    
  

  
  function info($user_id = false, $force = false)
  {
    global $db, $themes;
    $user_id = !$user_id? USER_ID : (int)$user_id;
    if(!array_key_exists($user_id, $this->users) OR $force)
    {
      $query = $db->query("SELECT users.*, COUNT(pages.id) AS `my_sites` FROM users 
											  LEFT JOIN pages ON pages.user_id = users.id  
											  WHERE users.id = $user_id GROUP BY users.id") or die(mysql_error());
      $row=$db->fetch($query);
      if($row)
      {
      $this->users[$user_id] = $row;
		return $this->users[$user_id];
    }
    else
		return array('id'=>0);
    }
    else
		return $this->users[$user_id];
  }
  
  
  function login_status()
  {
    if(get_cookie('user_id') && get_cookie('pass_hash'))
    {
      global $db;
      $user_id = $db->esc(get_cookie('user_id'));
      $password = $db->esc(get_cookie('pass_hash'));
      return $db->count('users', 'id', ' id = ' . $user_id . ' AND password = ' . $password);
    }
    else
    {
      return false;
    }
  }
 
  function __destruct()
  {
   unset($this->users);
  }
}

?>