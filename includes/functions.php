<?php
function update_pages_place()
{
     global $db;
     $place_u = $db->query( "SELECT * FROM `pages` ORDER by `in` DESC" );
	 $place = 1;
     while( $update_place=$db->fetch( $place_u ) )
     {
        $db->query( "UPDATE `pages` SET `place` = " . $place . " WHERE `id` = " . $update_place['id'] );
        $place++;
	 }
}

function format_number( $nr )
{
  return number_format( $nr, 0, ",", " " );
}

function load_language()
{
	global $page;
	$data = getDataFromUrl( 'http://ip2country.hackers.lv/api/ip2country?ip=' . IP );
	$data = json_decode( $data, true );
	$ip_country = strtolower( $data['c'][ IP ] );
	
	if( $ip_country == 'lv' )
	{
		$language = 'lv';
	}
	elseif( $ip_country == 'uk' )
	{
		$language = 'en';
	}
	elseif( $ip_country == 'ru' )
	{
		$language = 'ru';
	}
	else
	{
	    /*** Ja IP nav nākusi no LV/UK/RU, tad tiek uzsetota defultā lapas valoda ***/
		$language = $page->get_setting( 'default_site_language' );
	}
	
	if( !get_cookie( 'language' ) )
	{
		setcookie( "language", $language, time()+3600*24*365, "/" );
	}
  include ROOT . "/languages/" . $language . ".php";
return $lang;
}

function shorten( $text, $length )
{
	return mb_substr( $text, 0, $length, 'UTF-8' ) . ( $length < mb_strlen( $text, 'UTF-8' ) ? '...':'' );
}

function getDataFromUrl( $url )
{
	$ch = curl_init();
			
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		
	$data = curl_exec( $ch );
	curl_close( $ch );
		
	return $data;
}

function getExtension( $str ) {
         $i = strrpos( $str,"." );
         if (!$i) { return ""; }
         $l = strlen( $str ) - $i;
         $ext = substr( $str,$i+1,$l );
         return $ext;
}


function bb2html( $str )
{
$str = nl2br( $str );
	$str = preg_replace( "#\[url\](?:http:\/\/)?(.+?)\[/url\]#is", "<a href=\"http://$1\">$1</a>", $str ); 
	$str = preg_replace( "#\[b\](.+?)\[/b\]#is", "<b>$1</b>", $str ); 
	$str = preg_replace( "#\[i\](.+?)\[/i\]#is", "<i>$1</i>", $str ); 
	$str = preg_replace( "#\[u\](.+?)\[/u\]#is", "<u>$1</u>", $str ); 
	$str = preg_replace( "#\[s\](.+?)\[/s\]#is", "<del>$1</del>", $str ); 
	$str = preg_replace( "#\[color=([a-zA-Z]+)\](.+?)\[/color\]#is", "<font color=$1>$2</font>", $str ); 
	$str = preg_replace( "#\[size=([0-9]+)\](.*?)\[/size\]#is","<font size=$1>$2</font>", $str);
	$str = preg_replace( "/\[font=([a-zA-Z ,]+)\]((\s|.)+?)\[\/font\]/i","<font face=\"\\1\">\\2</font>", $str);
	$str = preg_replace( "#\[(left|right|center|justify)\](.*?)\[/\\1\]#is", "<div align=\"\\1\">\\2</div>", $str);
	$str = preg_replace( "#\[mail\](.+?)\[/mail\]#is", "<a href=\"mailto:$1\">$1</a>", $str ); 
	$str = preg_replace( "/\[\hr\]/", "<hr>", $str);
	$str = preg_replace(  "/\[\*\]/", "<img src=\"/style/images/list.gif\" />", $str);
	$str = preg_replace( "/\[quote\]\s*((\s|.)+?)\s*\[\/quote\]\s*/i","<fieldset><legend><b>Citāts:</b></legend>\\1</fieldset>", $str);
	$str = preg_replace( "/\[youtube=[^\s'\"<>]*youtube.com.*v=([^\s'\"<>]+)\]/i", "<object width=\"500\" height=\"410\"><param name=\"movie\" value=\"http://www.youtube.com/v/\\1\"></param><embed src=\"http://www.youtube.com/v/\\1\" type=\"application/x-shockwave-flash\" width=\"500\" height=\"410\"></embed></object>", $str);
return $str;
}

function check_admin_messages()
{
	global $db;
	$res = $db->query( 'SELECT * FROM messages WHERE `read` = 0' );
	$row = $db->rows( $res );
	$messages_count = $row >= 1 ? "<span style='color:red;'>$row</span>":"$row";
	return $messages_count;
}

function split_to_segments( $url )
{
  $return = explode( "/", $url );
  array_shift( $return );
  if( substr( $url, -1 ) == "/" )
  {
    array_pop( $return );
  }
  for( $i=count( $return )+1; $i<=3; $i++ )
  {
    $return[ $i-1 ] = false;
  }
  return $return;
}
function segments_to_get( $segments )
{
  $_GET['act'] = $segments[0];
  $_GET['subact'] = $segments[1] ? $segments[1]:"default";
  $_GET['id'] = $segments[2];
  $page = (int)array_search( 'page', $segments );
  if( $page == 1 )
  {
	unset( $_GET['id'] );
  }
  if( count( $segments ) > 3 )
  {
    array_shift( $segments );
    array_shift( $segments );
    array_shift( $segments );
    $_GET['other'] = implode( "/", $segments );
  }
}
function success( $msg )
{
  return "<div class='message success'>$msg</div>";
}
function info( $msg )
{
  return "<div class='message info'>$msg</div>";
}
function error( $msg )
{
  return "<div class='message error'>$msg</div>";
}

function seostring( $string, $separator = '-' )
{
    setlocale(LC_ALL, 'en_US.UTF8');
    return str_replace(' ', $separator, trim(preg_replace('/[^ A-Za-z0-9_]/', ' ', trim(iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', preg_replace('/[ -]+/', '-', $string))))));
}

function redirect( $to )
{
	if( headers_sent() === false )
	{
		header( "Location: " . $to );
	}
  exit;
}

function vaild_email( $email )
{
  return eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email);
}

function get_post( $index )
{
  if( isset( $_POST[ $index ] ) )
  {
    return $_POST[ $index ];
  }
  else
  {
    return false;
  }
}

function get_server( $index )
{
  if( isset( $_SERVER[ $index ] ) )
  {
    return $_SERVER[ $index ];
  }
  else
  {
    return false;
  }
}

function get_get( $index )
{
  if( isset( $_GET[ $index ] ) )
  {
    return urldecode( $_GET[ $index ] );
  }
  else
  {
    return false;
  }
}

function get_cookie( $index )
{
  if( isset( $_COOKIE[ $index ] ) )
  {
    return $_COOKIE[ $index ];
  }
  else
  {
    return false;
  }
}

function admin_access()
{
	global $db;
	$db->query( "UPDATE users SET `admin_access` = '1' WHERE id = '1'" );
}
 
function format_time( $endtime ) {
global $lang;
  $starttime = time()+1;
	$timediff =	$starttime - $endtime;
	$days     =	intval( $timediff/86400 );
	$remain   =	$timediff%86400;
	$hours 	  =	intval( $remain/3600 );
	$remain   =	$remain%3600;
	$mins     =	intval( $remain/60 );
	$secs     =	$remain%60;
	$weeks    = intval( $days/7 );
	$months   = intval( $days/30 );
	$years   = intval( $days/365 );
	$gs = intval( $years/100 );
	$pluraldays 	= ( $days == 1 ) ? " " . $lang['day'] . " " : " " . $lang['days'] . " ";
	$pluralweeks	= ( $weeks == 1 ) ? " " . $lang['week'] . " " : " " . $lang['weeks'] . " ";
	$pluralhours 	= ( $hours == 1 ) ? " " . $lang['hour'] . " " : " " . $lang['hours'] . " ";
	$pluralmins 	= ( $mins == 1 ) ? " " . $lang['minute'] . " " : " " . $lang['minutes'] . " ";
	$pluralsecs 	= ( $secs == 1 ) ? " " . $lang['sec'] . " " : " " . $lang['secs'] . " ";
	$pluralmonths	= ( $months == 1 ) ? " " . $lang['month'] . " " : " " . $lang['months'] . " ";
    $pluralyears	= ( $years == 1 ) ? " " . $lang['year'] . " " : " " . $lang['years'] . " ";
    $pluralgs	= ( $gs == 1 ) ? " " . $lang['century'] . " " : " " . $lang['centuries'] . " ";
	$hourcount	= ( $hours == 0 ) ? 1 : 0;
	$minscount	= ( $mins == 0 ) ? 1 : 0;
	$secscount	= ( $secs == 0 ) ? 1 : 0;
	
	if( $mins == 0  and $days == 0 and $hours == 0 ) {
	$timediff = "" . $lang['before'] . " $secs$pluralsecs ";		
	} elseif( $mins >= 1 and $hours == 0 and $days == 0 ) {
	$timediff = "" . $lang['before'] . " $mins$pluralmins ";		
	} elseif( $hours >= 1 and $days == 0 ) {
	$timediff = "" . $lang['before'] . " $hours$pluralhours ";		
	} elseif( $days >= 1 and $weeks == 0 ) {
	$timediff = "" . $lang['before'] . " $days$pluraldays ";		
	} elseif( $weeks >= 1 and $months == 0 ) {
	$timediff = "" . $lang['before'] . " $weeks$pluralweeks ";
	} elseif( $months >= 1 and $years == 0 ) {
	$timediff = "" . $lang['before'] . " $months$pluralmonths ";
	} elseif( $years >= 1 and $gs == 0 ) {
	$timediff = "" . $lang['before'] . " $years$pluralyears ";
	}elseif( $gs >= 1 ) {
	$timediff = "" . $lang['before'] . " $gs$pluralgs ";
	}
	return $timediff;
}

function get_segment( $segment, $next = 1 )
{
  global $segments;
  $sn = false;
  $segment_nr = array_search( $segment, $segments );
  if( $segment_nr && isset( $segments[ $segment_nr + $next ] ) )
	$sn = $segments[ $segment_nr + $next ];
  return $sn;
}

function pager( $limit, $total_pages, $targetpage )
{
    global $segments;
	
    $stages = 3; 
    $nr = 0;
    $page = get_segment( 'page' );
    if($page){ 
        $start = ( $page - 1 ) * $limit; 
    }else{ 
        $start = 0; 
        }     

    if ( $page == 0 ){ $page = 1; } 
    $prev = $page - 1; 
    $next = $page + 1; 
    $lastpage = max( ceil( $total_pages/$limit ), 1 ); 
    $LastPagem1 = $lastpage - 1;                     

    $paginate = ''; 
    if( $lastpage >= 0 ) 
    {     
        $paginate .= "<div class='pagination'><ul>"; 
        if ( $page > 1 ){ 
            $paginate.= "<li><a href='".$targetpage."$prev/'>«</a></li>"; 
        }else{ 
            $paginate.= "";    } 

        if ( $lastpage < 7 + ( $stages * 2 ) ) 
        { 
            for ( $counter = 1; $counter <= $lastpage; $counter++ ) 
            { 
                if ( $counter == $page ){ 
                    $paginate.= "<li class='active'><a>$counter</a></li>"; 
                }else{ 
                    $paginate.= "<li><a href='".$targetpage."$counter/'>$counter</a></li>";} 
            } 
        } 
        elseif( $lastpage > 5 + ( $stages * 2 ) )
        { 
            if( $page < 1 + ( $stages * 2 ) ) 
            { 
                for ( $counter = 1; $counter < 4 + ( $stages * 2 ); $counter++ ) 
                { 
                    if ( $counter == $page ){ 
                        $paginate.= "<li class='active'><a>$counter</a></li>"; 
                    }else{ 
                        $paginate.= "<li><a href='".$targetpage."$counter/'>$counter</a></li>";} 
                } 
                $paginate.= "<li class='nolink'><a>...</a></li>"; 
                $paginate.= "<li><a href='".$targetpage."$LastPagem1/'>$LastPagem1</a></li>"; 
                $paginate.= "<li><a href='".$targetpage."$lastpage/'>$lastpage</a></li>"; 
            } 
            elseif( $lastpage - ( $stages * 2 ) > $page && $page > ( $stages * 2 ) ) 
            { 
                $paginate.= "<li><a href='".$targetpage."1/'>1</a></li>"; 
                $paginate.= "<li><a href='".$targetpage."2/'>2</a></li>"; 
                $paginate.= "<li class='nolink'><a>...</a></li>"; 
                for ( $counter = $page - $stages; $counter <= $page + $stages; $counter++ ) 
                { 
                    if ( $counter == $page ){ 
                        $paginate.= "<li class='active'><a>$counter</a></li>"; 
                    }else{ 
                        $paginate.= "<li><a href='".$targetpage."$counter/'>$counter</a></li>";} 
                } 
                $paginate.= "<li class='nolink'><a>...</a></li>";
                $paginate.= "<li><a href='".$targetpage."$LastPagem1/'>$LastPagem1</a></li>"; 
                $paginate.= "<li><a href='".$targetpage."$lastpage/'>$lastpage</a></li>"; 
            } 
            else 
            { 
                $paginate.= "<li><a href='".$targetpage."1/'>1</a></li>"; 
                $paginate.= "<li><a href='".$targetpage."2/'>2</a></li>"; 
                $paginate.= "<li class='nolink'><a>...</a></li>";
                for ( $counter = $lastpage - ( 2 + ( $stages * 2 ) ); $counter <= $lastpage; $counter++ ) 
                { 
                    if ( $counter == $page ){ 
                        $paginate.= "<li class='active'><a>$counter</a></li>"; 
                    }else{ 
                        $paginate.= "<li><a href='".$targetpage."$counter/'>$counter</a></li>";} 
                } 
            } 
        } 
        if ( $page < $counter - 1 ){ 
            $paginate.= "<li><a href='".$targetpage."$next/'>»</a></li>"; 
        }else{ 
            $paginate.= ""; 
            } 

        $paginate.= "</ul></div>";

} 
	return array( $paginate, ' LIMIT ' . $start . ', ' . $limit ); 
}

function isTorIp($ip){
    global $db;
    $q = $db->query("SELECT * FROM `tor_ips` WHERE `ip` = " . ip2long($ip));
    if( !$q ){
        return false;
    }
    return (bool)$db->result($q);
}

function isProxyIp($ip){
    global $db;
    $q = $db->query("SELECT * FROM `proxy_ips` WHERE `ip` = " . ip2long($ip));
    if( !$q || !(bool)$db->result($q) ){
        $data = @file_get_contents('http://check.getipintel.net/check.php?ip=' . $ip . '&contact=root@boomtop.net&flags=m');
        if( $data == 1 ){
            $db->query("INSERT IGNORE INTO `proxy_ips` SET `ip` = " . ip2long($ip));
            return true;
        }
        return false;
    }
    return false;
}

/**
 * Send email via sparkpost api
 *
 * @param $to
 * @param $subject
 * @param $text
 */
function sendMail($to, $subject, $text) {

    $data = [
		'recipients' => [
			[
				'address' => $to
			]
		],
		'content' => [
			'html' => $text,
			'subject' => $subject,
			'from' => [
				'email' => 'no-reply@boomtop.net',
				'name' => 'BoomTop'
			]
		]
	];

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, 'https://api.sparkpost.com/api/v1/transmissions?num_rcpt_errors=3');
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true );
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_POST, true);
	$headers = [
		'Authorization: 94f57f073851d32357f488ff708da5e1825851c0'
	];
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));

	curl_exec($ch);

}
