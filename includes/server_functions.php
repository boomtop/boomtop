<?php
function server_format_time($seconds) {
	if ($seconds === "") { return ""; }
	$n = $seconds < 0 ? "-" : "";
	$seconds = abs($seconds);
	$h = intval($seconds / 3600);
	$m = intval($seconds / 60  ) % 60;
	$s = intval($seconds       ) % 60;
	$h = str_pad($h, "2", "0", STR_PAD_LEFT);
	$m = str_pad($m, "2", "0", STR_PAD_LEFT);
	$s = str_pad($s, "2", "0", STR_PAD_LEFT);
	return "{$n}{$h}:{$m}:{$s}";
}

function update_top()
{
	global $db;
	$res = $db->query('SELECT * FROM servers ORDER BY rating DESC');
	$i=0;
	while($row=$db->fetch($res))
	{
		$i++;
		$db->query('UPDATE servers SET top_place = ' . $i . ' WHERE id = ' . $row['id']);
	}
}

function data_unpack($string, $format) {
	list(,$string) = @unpack($format, $string);
	return $string;
}

function cutbyte(&$buffer, $length) {
	$string = substr($buffer, 0, $length);
	$buffer = substr($buffer, $length);
	return $string;
}

function cutstring(&$buffer, $start_byte = 0, $end_marker = "\x00") {
	$buffer = substr($buffer, $start_byte);
	$length = strpos($buffer, $end_marker);
	if ($length === FALSE) { $length = strlen($buffer); }
	$string = substr($buffer, 0, $length);
	$buffer = substr($buffer, $length + strlen($end_marker));
	return $string;
}

function query(&$server, &$need, &$fp) {

	$challenge_code = isset($need['challenge']) ? $need['challenge'] : "\x00\x00\x00\x00";

	if     ($need['s']) { fwrite($fp, "\xFF\xFF\xFF\xFF\x54Source Engine Query\x00"); }
	elseif ($need['p']) { fwrite($fp, "\xFF\xFF\xFF\xFF\x55{$challenge_code}");       }
	elseif ($need['e']) { fwrite($fp, "\xFF\xFF\xFF\xFF\x56{$challenge_code}");       }


	$packet_temp  = array();
	$packet_type  = 0;
	$packet_count = 0;
	$packet_total = 4;

	do {
		$packet = fread($fp, 4096);

		if     ($need['s']) { if ($packet[4] == "D")                                           { continue; } }
		elseif ($need['p']) { if ($packet[4] == "m" || $packet[4] == "I")                      { continue; } }
		elseif ($need['e']) { if ($packet[4] == "m" || $packet[4] == "I" || $packet[4] == "D") { continue; } }


		if     (substr($packet, 0,  5) == "\xFF\xFF\xFF\xFF\x41") { $need['challenge'] = substr($packet, 5,  4); return TRUE; } // REPEAT WITH GIVEN CHALLENGE CODE
		elseif (substr($packet, 0,  4) == "\xFF\xFF\xFF\xFF")     { $packet_total = 1;                     $packet_type = 1;       } // SINGLE PACKET - HL1 OR HL2
		elseif (substr($packet, 9,  4) == "\xFF\xFF\xFF\xFF")     { $packet_total = ord($packet[8]) & 0xF; $packet_type = 2;       } // MULTI PACKET  - HL1 ( TOTAL IS LOWER NIBBLE OF BYTE )
		elseif (substr($packet, 12, 4) == "\xFF\xFF\xFF\xFF")     { $packet_total = ord($packet[8]);       $packet_type = 3;       } // MULTI PACKET  - HL2
		elseif (substr($packet, 18, 2) == "BZ")                   { $packet_total = ord($packet[8]);       $packet_type = 4;       } // BZIP PACKET   - HL2

		$packet_count ++;
		$packet_temp[] = $packet;
	}
	while ($packet && $packet_count < $packet_total);

	if ($packet_type == 0) { return $server['s'] ? TRUE : FALSE; } // UNKNOWN RESPONSE ( SOME SERVERS ONLY SEND [s] )

	$buffer = array();

	foreach ($packet_temp as $packet) {
		if     ($packet_type == 1) { $packet_order = 0; }
		elseif ($packet_type == 2) { $packet_order = ord($packet[8]) >> 4; $packet = substr($packet, 9);  } // ( INDEX IS UPPER NIBBLE OF BYTE )
		elseif ($packet_type == 3) { $packet_order = ord($packet[9]);      $packet = substr($packet, 12); }
		elseif ($packet_type == 4) { $packet_order = ord($packet[9]);      $packet = substr($packet, 18); }

		$buffer[$packet_order] = $packet;
	}

	ksort($buffer);

	$buffer = implode("", $buffer);

	if ($packet_type == 4) {
		if (!function_exists("bzdecompress")) {
			$server['e']['bzip2'] = "unavailable"; $need['e'] = FALSE;
			return TRUE;
		}

		$buffer = bzdecompress($buffer);
	}

	$header = cutbyte($buffer, 4);

	if ($header != "\xFF\xFF\xFF\xFF") { return FALSE; } // SOMETHING WENT WRONG

	$response_type = cutbyte($buffer, 1);

	if ($response_type == "I") {
		$server['e']['netcode']     = ord(cutbyte($buffer, 1));
		$server['s']['name']        = cutstring($buffer);
		$server['s']['map']         = cutstring($buffer);
		$server['s']['game']        = cutstring($buffer);
		$server['e']['description'] = cutstring($buffer);
		$server['e']['appid']       = data_unpack(cutbyte($buffer, 2), "S");
		$server['s']['players']     = ord(cutbyte($buffer, 1));
		$server['s']['playersmax']  = ord(cutbyte($buffer, 1));
		$server['e']['bots']        = ord(cutbyte($buffer, 1));
		$server['e']['dedicated']   = cutbyte($buffer, 1);
		$server['e']['os']          = cutbyte($buffer, 1);
		$server['s']['password']    = ord(cutbyte($buffer, 1));
		$server['e']['anticheat']   = ord(cutbyte($buffer, 1));
		$server['e']['version']     = cutstring($buffer);
	} elseif ($response_type == "m") {
		$server_ip                  = cutstring($buffer);
		$server['s']['name']        = cutstring($buffer);
		$server['s']['map']         = cutstring($buffer);
		$server['s']['game']        = cutstring($buffer);
		$server['e']['description'] = cutstring($buffer);
		$server['s']['players']     = ord(cutbyte($buffer, 1));
		$server['s']['playersmax']  = ord(cutbyte($buffer, 1));
		$server['e']['netcode']     = ord(cutbyte($buffer, 1));
		$server['e']['dedicated']   = cutbyte($buffer, 1);
		$server['e']['os']          = cutbyte($buffer, 1);
		$server['s']['password']    = ord(cutbyte($buffer, 1));

		if (ord(cutbyte($buffer, 1))) {
			$server['e']['mod_url_info']     = cutstring($buffer);
			$server['e']['mod_url_download'] = cutstring($buffer);
			$buffer = substr($buffer, 1);
			$server['e']['mod_version']      = data_unpack(cutbyte($buffer, 4), "l");
			$server['e']['mod_size']         = data_unpack(cutbyte($buffer, 4), "l");
			$server['e']['mod_server_side']  = ord(cutbyte($buffer, 1));
			$server['e']['mod_custom_dll']   = ord(cutbyte($buffer, 1));
		}
		$server['e']['responsetype'] = $response_type;

		$server['e']['anticheat'] = ord(cutbyte($buffer, 1));
		$server['e']['bots']      = ord(cutbyte($buffer, 1));
	} elseif($response_type == "D") {
		$returned = ord(cutbyte($buffer, 1));

		$player_key = 0;

		while ($buffer) { 
			$server['p'][$player_key]['pid']   = ord(cutbyte($buffer, 1));
			$server['p'][$player_key]['name']  = cutstring($buffer);
			$server['p'][$player_key]['score'] = data_unpack(cutbyte($buffer, 4), "l");
			$server['p'][$player_key]['time']  = server_format_time(data_unpack(cutbyte($buffer, 4), "f"));
			$player_key ++;
		}
	} elseif($response_type == "E") {
		$returned = data_unpack(cutbyte($buffer, 2), "S");

		while ($buffer) {
			$item_key   = strtolower(cutstring($buffer));
			$item_value = cutstring($buffer);

			$server['e'][$item_key] = $item_value;
		}
	}

	if ($need['s'] && !$need['e']) { $server['e'] = array(); }

	if     ($need['s']) { $need['s'] = FALSE; }
	elseif ($need['p']) { $need['p'] = FALSE; }
	elseif ($need['e']) { $need['e'] = FALSE; }

	return TRUE;
}

function query_live($type, $ip, $port,$request) {
	if (preg_match("/[^0-9a-z\.\-\[\]\:]/i", $ip)) {
		throw new Exception("PROBLEM: INVALID IP OR HOSTNAME");
	}

	if (!intval($port)) {
		throw new Exception("PROBLEM: INVALID QUERY PORT");
	}

	$server                 = array();
	$server['s']            = array();
	$server['e']            = array();
	$server['p']            = array();
	$server['b']            = array();
	$server['b']['type']    = $type;
	$server['b']['ip']      = $ip;
	$server['b']['real_ip'] = gethostbyname($ip);
	$server['b']['port']    = $port;
	$server['b']['status']  = 1;

	$status = query_direct($server, $request);

	if (!$status) {
		$server['b']['status'] = 0;
	} else {
		if (!$server['s']) { unset($server['s']); }
		if (!$server['e']) { unset($server['e']); }
		if (!$server['p'] && $server['s']['players'] != "0" && strpos($request, "p") === FALSE) { unset($server['p']); }
	}

	if (!empty($server['s'])) {
		if (!$server['s']['game']) { $server['s']['game'] = $type; }

		$tmp = str_replace("\\", "/", $server['s']['map']); // REMOVE ANY
		$tmp = explode("/", $tmp);                          // FOLDERS
		$server['s']['map'] = array_pop($tmp);              // FROM MAP

		if (strtolower($server['s']['password']) == "false") { $server['s']['password'] = 0; }
		if (strtolower($server['s']['password']) == "true")  { $server['s']['password'] = 1; }

		$server['s']['password']   = intval($server['s']['password']);
		$server['s']['players']    = intval($server['s']['players']);
		$server['s']['playersmax'] = intval($server['s']['playersmax']);

		if ($server['s']['players']    < 0) { $server['s']['players']    = 0; }
		if ($server['s']['playersmax'] < 0) { $server['s']['playersmax'] = 0; }
	}  

	return $server;
}

function query_direct(&$server, $request) {
	$fp = @fsockopen("udp://{$server['b']['ip']}", $server['b']['port'], $errno, $errstr, 1);
	if (!$fp) { return FALSE; }

	$timeout = 0;

	stream_set_timeout($fp, $timeout, $timeout ? 0 : 500000);
	stream_set_blocking($fp, TRUE);

	$need      = array();
	$need['s'] = strpos($request, "s") !== FALSE ? TRUE : FALSE;
	$need['e'] = strpos($request, "e") !== FALSE ? TRUE : FALSE;
	$need['p'] = strpos($request, "p") !== FALSE ? TRUE : FALSE;

	if ($need['e'] && !$need['s']) { $need['s'] = TRUE; }

	do { 
		$need_check = $need;
		$status = @query($server, $need, $fp);

		if (!$status) { break; }
		if ($need_check == $need) { break; }

		if ($need['p'] && $server['s']['players'] == "0") { $need['p'] = FALSE; }
	}
	while ($need['s'] == TRUE || $need['e'] == TRUE || $need['p'] == TRUE);


	@fclose($fp);

	return $status;
} 

function update_server_data($server_id, $server_data)
{
	global $db;
	
	if($server_data['b']['status'] == 0 )
	{
		$result = $db->query('UPDATE servers SET last_conn = '.time().', total_cons = total_cons + 1, online = 0 WHERE id = ' . intval($server_id));
	}
	else
	{
		$u_data['online'] = 1;
		$u_data['last_conn'] = time();
		$u_data['last_succ_conn'] = time();
		$u_data['title'] = $server_data['s']['name'];
		$u_data['seo_title'] = seostring($server_data['s']['name']);
		$u_data['map'] = strtolower($server_data['s']['map']);
		$u_data['players'] = $server_data['s']['players'];
		$u_data['max_players'] = $server_data['s']['playersmax'];
		$players = array();
		if( ! empty($server_data['p'])
			AND count($server_data['p']) > 0 )
		{
			foreach($server_data['p'] as $key => $val)
			{
				$players[] = array(
					'name' => htmlspecialchars($val['name']),
					'score' => $val['score'],
					'time' => $val['time']
				);
			}
		}		
		
		$result = $db->update_array('servers', $u_data, 'id = ' . intval($server_id)) or die(mysql_error());
		
		$db->query('UPDATE servers SET total_cons = total_cons + 1, last_conn = '.time().', player_info = \''.serialize($players).'\', successful_cons = successful_cons + 1 WHERE id = ' . intval($server_id));
	}
	
	if($result)
		return true;
	else
		return false;
}

function update_minecraft_server_data($server_id, $server_data)
{
	global $db;
	
	if( empty( $server_data ) )
	{
		$result = $db->query('UPDATE servers SET last_conn = '.time().', total_cons = total_cons + 1, online = 0 WHERE id = ' . intval($server_id));
	}
	else
	{
		$u_data['online'] = 1;
		$u_data['last_conn'] = time();
		$u_data['last_succ_conn'] = time();
		$u_data['title'] = $server_data['HostName'];
		$u_data['seo_title'] = seostring($server_data['HostName']);
		$u_data['map'] = strtolower($server_data['Map']);
		$u_data['players'] = $server_data['Players'];
		$u_data['max_players'] = $server_data['MaxPlayers'];
		
		$result = $db->update_array('servers', $u_data, 'id = ' . intval($server_id)) or die(mysql_error());
		
		$db->query('UPDATE servers SET total_cons = total_cons + 1, last_conn = '.time().', successful_cons = successful_cons + 1 WHERE id = ' . intval($server_id));
	}
	
	if($result)
		return true;
	else
		return false;
}