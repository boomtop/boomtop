<h1><i class="icon-plus"></i> <?php echo $lang['add_page']; ?></h1>
<?php
$page->need_login();
$page->set_page_title( $lang['add_page'] );
define( "MAX_SIZE", "300" );
if( get_post( 'add_page' ) )
{
 $error = array();
    if( !get_post( 'title' ) )
    {
        $error[] = $lang['add-page_title-need'];
    }
    if( !get_post( 'url' ) )
    {
        $error[] = $lang['add-page_url-need'];
    }
    if( !get_post( 'description' ) )
    {
        $error[] = $lang['add-page_descr-need'];
    }
	if( !get_post( 'category' ) )
    {
        $error[] = $lang['add-page_cate-need'];
    }
    if( !get_post( 'counter_style' ) )
    {
        $error[] = $lang['add-page_counter-need'];
    }
   
	if( $_FILES[ 'banner' ][ 'name' ] )
	{
	$image = $_FILES['banner']['name'];
	$filename = stripslashes($_FILES['banner']['name']);
	$extension = getExtension($filename);
 	$extension = strtolower($extension);
	
	if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
 	{
 		$error[] = $lang['add-page_ext-allowed'];
 	}
 
	 $size = filesize($_FILES['banner']['tmp_name']);

	if( $size > MAX_SIZE*1024 )
	{
		$error[] = $lang['add-page_image-size'];
	}
	
    list( $width, $height ) = getimagesize( $_FILES['banner']['tmp_name'] );
	
	if( $width != 468 AND $height != 60 )
	{
		$error[] = 'Banera bildes izmēri nedrīkst būt mazāki vai lielāki par 468x60.';
	}

	$image_name = time() . '.' . $extension;
	$banner = $image_name;
		
	if(!count($error) >= 1)
	{
		move_uploaded_file($_FILES[ 'banner' ][ 'tmp_name' ], ROOT . "/style/images/banners/" . $banner);    
	}
	}
	elseif(get_post( 'banner_url' ))
	{
		define('allowed_url_images', 'jpg|jpeg|gif|png'); 
		define('image_directory', 'style/images/banners'); 
	
		if(!preg_match('#^http://.*([^/]+\.('.allowed_url_images.'))$#', get_post('banner_url'), $file)) { 
			$error[] = $lang['add-page_ext-url-allowed']; 
		} 
 
		if(!$image = @file_get_contents(get_post('banner_url'))) { 
			$error[] = $lang['add-page_get-content'];
		}
	
		list( $width, $height ) = getimagesize( get_post('banner_url') );
		
		if( $width != 468 AND $height != 60 )
		{
			$error[] = 'Banera bildes izmēri nedrīkst būt mazāki vai lielāki par 468x60.';
		}	
	
		$image_name_url = time() . '.' . $file[1];
		$banner = $image_name_url;
 
		if(!count($error) >= 1)
		{
		if(!$f = fopen(image_directory . '/' . $banner, 'w')) { 
			$error[] = $lang['add-page_open-file']; 
		}
 
		if (fwrite($f, $image) === FALSE) { 
			$error[] = $lang['add-page_upload-to-server']; 
		}
 
		fclose($f);
	  }
	}
	else
	{
		$banner = 'no_banner';
	}
	
	if(count($error) >= 1)
	{
	echo "<div class='message error'>";
	foreach($error as $err)
	{
		echo "$err<br />";
	}
	echo "</div>";
	}
	else
	{
  
        $insert_data = array(
					  'title'=>get_post('title'),
                      'url'=>get_post('url'),
                      'description'=>get_post('description'),
					  'banner'=>$banner,
                      'counter_style'=>get_post('counter_style'),
                      'category'=>(int)get_post('category'),
                      'user_id'=>USER_ID,
                      'added'=>time()
                      );
        $db->insert_array( 'pages', $insert_data );
	echo success( $lang['add-page_succes'] );
      
	  $res = $db->query( 'SELECT * FROM pages ORDER BY id DESC LIMIT 1' );
      $row = $db->fetch( $res );
	  // HTML code for website
	echo '<div class="message info">
    <h3>' . $lang['html_code_for_your_site'] . '</h3>
<textarea readonly="readonly" style="width: 500px; min-height: 87px;cursor:auto;">
&lt;!-- begin ' . WEBSITE_NAME . ' stats counter --&gt;
&lt;iframe src="' . BASE . '/counter.php?id=' . $row['id'] . '" width="88" height="53" border="0" frameborder="0"&gt;&lt;/iframe&gt;
&lt;!-- end ' . WEBSITE_NAME . ' stats counter --&gt;</textarea>
   <br />
   <span style="color: red;">
   ' . $lang['copy_this_html_code'] . '
   </span>
   </div>
   ';
    }
}
?>
<link rel="stylesheet" href="<?php echo BASE ?>/style/counter.css" type="text/css" />
<div class="left-content drop-shadow lifted">
<form method="post" enctype="multipart/form-data">
  <table class="ipbtable" cellspacing="1" style="width: 570px;margin-bottom: -15px;">
		<tr><td class="row2" align="right" style="width:120px;"><?php echo $lang['add-page_site-name']; ?> <font color="red">*</font></td><td class="row1"><input type="text" name="title" style="width:415px;position:relative;top:5px;" value="<?php echo $_REQUEST['title']; ?>" /></td></tr>
		<tr><td class="row2" align="right" ><?php echo $lang['add-page_site-url']; ?> <font color="red">*</font></td><td class="row1"><input type="text" name="url" style="width:415px;position:relative;top:5px;" value="<?php echo $_REQUEST['url']; ?>" /></td></tr>
		<tr><td class="row2" align="right"><?php echo $lang['add-page_banner']; ?><br /><small>(<?php echo $lang['add-page_not-required']; ?>)</small></td><td class="row1">
		<div class="upload_type">
			<a href="javascript:;" class="upload_from_pc" class="upload-item"><?php echo $lang['add-page_upload-from-pc']; ?></a><a href="javascript:;" class="upload_from_internet"><?php echo $lang['add-page_upload-from-url']; ?></a>
				<div style="height:8px;"></div>
				<div id="upload_from_pc" class="upload-item">
					<input type="file" name="banner" />
				</div>
				<div id="upload_from_internet" style="display: none;" class="upload-item">
					<input type="text" name="banner_url" style="width:415px;position:relative;top:5px;" value="<?php echo $_REQUEST['banner_url']; ?>" />
			  </div>
		</div>
		</td></tr>
		<tr><td class="row2" align="right"><?php echo $lang['add-page_descr']; ?> <font color="red">*</font></td><td class="row1"><textarea name="description" style="width:415px;position:relative;top:5px; min-height: 100px;" /><?php echo $_REQUEST['description']; ?></textarea></td></tr>
		<tr><td class="row2" align="right"><?php echo $lang['add-page_category']; ?> <font color="red">*</font></td><td class="row1">
		<select name="category" style="position:relative;top:5px;padding:2px; height: 25px;border:1px solid #ccc;" />
		<option disabled="disabled" selected="selected">--- <?php echo $lang['choose']; ?> --- </option>
		<?php
		$cat = $db->query( 'SELECT * FROM categories ORDER BY id ASC' );
		while( $categories=$db->fetch( $cat ) )
		{
		?>
			<option value="<?php echo $categories['id'] ?>"><?php echo $categories['title'] ?></option>
		<?php
		}
		?>
		</select></td></tr>
		<tr><td class="row2" align="right"><?php echo $lang['counter_style']; ?> <font color="red">*</font></td>
		<td class="row1">
		<div style="float:left; width:430px;">
		<div style="float:left; width:100px;">
			<input style="margin-left:35px;margin-bottom:5px;" type="radio" name="counter_style" value="1" />
			<div class='counters'>
				<div class='counter_1' align='center'>
					<div class='visits' style="padding-top:20px">0</div>
					<div class='text' style="padding-top:1px">Apmeklētāji</div>
				</div>
			</div>
		</div>
		<div style="float:left; width:100px;">
			<input style="margin-left:35px;margin-bottom:5px;" type="radio" name="counter_style" value="2" />
			<div class='counters'>
				<div class='counter_2'>
					<div class='text'>
						<div class='left'><span>IN</span><b>0</b></div>
						<div class='right'><span>OUT</span><b>0</b></div>
					</div>
				</div>
			</div>
		</div>
		<div style="float:left; width:100px;">
			<input style="margin-left:35px;margin-bottom:5px;" type="radio" name="counter_style" value="3" />
			<div class='counters'>
				<div class='counter_3'></div>
			</div>
		</div>
		</div>
		</td></tr>
		<tr><td class="row3"></td><td class="row3"><input type="submit" class="btn btn-success" value="<?php echo $lang['add_page']; ?>" name="add_page" /></td></tr>
  </table>
</form>
</div>