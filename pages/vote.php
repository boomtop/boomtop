<?php
	$id = (int)get_get( 'subact' );

	if(!$id)	
    redirect('/'); // Redirect if id does not exist
	
	$res = $db->query( "SELECT * FROM pages WHERE id = $id" );
	$row = $db->fetch( $res );
	
if( $row['id'] )
{
$page->set_page_title( '' . $lang['vote_for_page'] . ' - ' . $row['title'] . '' );
?>
<h1><i class="icon-thumbs-up"></i> <?php echo $lang['vote_for_page']; ?> "<?php echo $row['title'] ?>"</h1>
<?php

$vote_name = substr( md5( IP . 'Te bija sLIDe' ), 0, 8 );
$referer = get_server('HTTP_REFERER');

if( get_post( $vote_name ) && $referer && in_array(parse_url($referer, PHP_URL_HOST), ['boomtop.net', 'www.boomtop.net']) && !isTorIp($_SERVER['REMOTE_ADDR']) && !isProxyIp($_SERVER['REMOTE_ADDR']) )
{
   $ip = $db->esc(IP);
   
   $time = time();

   $query = $db->query( "SELECT `page_id` FROM `in_log` WHERE `page_id` = " . $id . " AND `time` >= " . strtotime( 'today' ) . " AND ip = " . $ip . "" );
   
	if( $db->rows( $query ) )
	{
		echo error( $lang['already_voted'] );
	}
	else
	{
		$value = new stdClass;
		$value->value = '\'' . json_encode([
			'post' => $_POST,
			'server' => $_SERVER,
			'get' => $_GET
		]) . '\'';
	
	   // Insert data if rows = 0 witch your IP (last 24 hours)
	    $insert_data = array(
					  'page_id'=>$id,
                      'ip'=>IP,
                      'time'=>$time,
		              'data'=>$value
                      );					
        $db->insert_array( 'in_log', $insert_data );

		$db->insert_array( 'in', [
			'ip' => ip2long(IP),
			'page_id' => (int)$id
		] );
		
	  // Update data 
		$db->query( 'UPDATE pages SET `in` = `in` + 1  WHERE id = ' . $id );
		echo success( $lang['vote_thanks'] );
	 // Redirect to home page if vote accepted
	header("refresh:1;url=" . BASE . "/");
	
   }
}
?>
<div class="left-content drop-shadow lifted">
<form method="post" name="<?php echo $vote_name; ?>">
	<br />
	<br />
	<center>
	<input type="submit" class="btn btn-danger btn-large" name="<?php echo $vote_name; ?>" value="<?php echo strtoupper( $lang['vote_for_page'] ); ?> +1"/>
	</center>
</form>
<br />
<center>
<?php echo $lang['vote_more_votes-1']; ?> (<i><b><?php echo $row['title'] ?></b></i>) <?php echo $lang['vote_more_votes-2']; ?> <a href="<?php echo BASE ?>/buy-in/<?php echo $row['id'] ?>/"><?php echo $lang['vote_more_votes-3']; ?></a>!
</center>
<br /><br />
</div>
<?php
}
else
{
    // If page with id does not exist
	$page->set_page_title( $lang['page_not_exist'] );
	echo '<h1>' . $lang['page_not_exist'] . '</h1>';
	echo error( $lang['page_not_exist_text'] );
}
?>