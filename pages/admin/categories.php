<?php
if( get_get('id') == 'delete' AND get_get('other') )
{
	$db->query( "DELETE FROM categories WHERE id = " . $db->esc( get_get( 'other' ) ) );
	echo success( $lang['cat-deleted'] );
}
if( get_get('id') == 'edit' AND get_get('other') )
{
	include ROOT . '/pages/admin/categories/edit.php';
}
elseif( get_get('id') == 'create' )
{
	include ROOT . '/pages/admin/categories/create.php';
}
else
{
?>
<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/categories/create"><?php echo $lang['add_cat']; ?></a></li>
		<li class="sepr">|</li>	
		<li><a href="<?php echo BASE ?>/admin/categories/" class="active"><?php echo $lang['categories']; ?></a></li>
	</li>
</div>
<div style="margin:5px auto;"></div>
<table class="ipbtable" cellspacing="1" style="width:570px;">
<?php
$res = $db->query( "SELECT * FROM categories ORDER BY `id` DESC" );
$i = 0;
while( $row = $db->fetch( $res ) )
{
		echo '<tr style="border-bottom: 1px solid #dbdbdb; vertical-align:middle;"> 
			   <td class="row2" style="font-weight:bold; background-color:#D8D8D8; vertical-align:middle; width: 85px;">
				 <center>
					' . $lang['categories'] . '<br />ID: ' . $row['id'] . '
				 </center>
				</td>
				<td class="row2">
					<b>' . $lang['cat_name'] . ':</b> <font color="black">' . $row['title'] . '</font>
				<br />
					<b>' . $lang['cat_url'] . ':</b> <a href="' . BASE . '/category/' . $row['id'] . '/' . $row['seostring'] . '/">' . $row['title'] . '</a>
				</td>
				<td align="center" class="row1" style="border-left: 1px solid #dbdbdb; width: 75px;">
				<div style="height:7px;"></div>
					<a href="' . BASE . '/admin/categories/edit/' . $row['id'] . '">
						<img title="' . $lang['edit'] . '" src="' . BASE . '/style/images/edit.png" alt="" />
					</a> 
				      <a href="javascript:;" onclick="if (confirm(\'' . $lang['cat_delete'] . '\')) document.location.href=\'' . BASE . '/admin/categories/delete/' . $row['id'] . '\' ">
						<img title="' . $lang['delete'] . '" src="' . BASE . '/style/images/delete.png" alt="" />
					  </a>
				   </td>
			   </tr>';
$i++;
}

if($i == 0) 
echo error( $lang['cat-empty'] );

echo '</table>';
}
?>