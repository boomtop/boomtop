<?php
if( get_get('id') == 'delete' AND get_get('other') )
{
    $db->query( "UPDATE pages SET `in` = `in` - 1  WHERE id = " . $db->esc( get_get( 'other' ) ) );
	$db->query( "DELETE FROM in_log WHERE page_id = " . $db->esc( get_get( 'other' ) ) );
	echo success( $lang['vote_deleted'] );
	header("refresh:1;url=" . BASE . "/admin/votes/");
}
else
{
?>
<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/votes/" class="active"><?php echo $lang['votes_for_pages']; ?></a></li>
	</li>
</div>
<div style="margin:5px auto;"></div>

<table class="ipbtable" style="width:570px;" cellspacing="1">
	<tr>
		<td class="row2x" style="width:110px;"><?php echo $lang['page']; ?></td>
		<td class="row2x" style="width:100px;">IP</td>
		<td class="row2x" align="center" style="width:100px;"><?php echo $lang['voted']; ?></td>
		<td class="row2x" align="center" style="width:55px;"><?php echo $lang['options']; ?></td>
	</tr>
<?php
$count = $db->count( 'in_log', 'page_id' );
$limit = 50;
list( $pager, $limit )=pager( $limit, $count, BASE . '/admin/votes/page/' );
$res = $db->query( "SELECT * FROM in_log ORDER BY `time` DESC " . $limit );
$i = 0;
while( $row = $db->fetch( $res ) )
{
	$query = $db->query( "SELECT * FROM pages WHERE id = " . $row['page_id'] );
	$site = $db->fetch( $query );
		if($site['url'] == '')
		{
			$site_url = '<i>' . $lang['votes_deleted-page'] . '</i>';
		}
		else
		{
			$site_url = '<a href="'.$site['url'].'" target="_blank">'.$site['title'].'</a>';
		}
		echo '<tr>
			<td class="row2">
			  ' . $site_url . '
			</td>
		    <td class="row2">
			  ' . $row['ip'] . '
			</td>
			<td class="row2" align="center">
				' . format_time( $row[ 'time' ] ) . '
			</td>
			<td class="row2" align="center">
				<a href="javascript:;" onclick="if (confirm(\'' . $lang['vote-delete'] . '\')) document.location.href=\'' . BASE . '/admin/votes/delete/' . $row['page_id'] . '\' ">
					<img title="' . $lang['delete'] . '" src="' . BASE . '/style/images/delete.png" alt="" />
				</a>
			</td>
			</tr>';
$i++;
}

echo '</table>';

if( $i == 0 ) 
echo error( $lang['votes-empty'] );

if( $i != 0 )
echo $pager;
}
?>