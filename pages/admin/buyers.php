<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/buyers/" class="active"><?php echo $lang['all_in_buyers']; ?></a></li>
	</li>
</div>
<div style="margin:5px auto;"></div>

<table class="ipbtable" style="width:570px;" cellspacing="1">
	<tr>
		<td class="row2x" style="width:110px;"><?php echo $lang['page']; ?></td>
		<td class="row2x" style="width:100px;"><?php echo $lang['buy-in_payment-type']; ?></td>
		<td class="row2x" align="center" style="width:100px;"><?php echo $lang['all_in_buyers-unlock']; ?></td>
		<td class="row2x" align="center" style="width:55px;"><?php echo $lang['all_in_buyers-price']; ?></td>
		<td class="row2x" align="center" style="width:55px;">IN+</td>
		<td class="row2x" align="center" style="width:55px;">IP</td>
		<td class="row2x" align="center" style="width:55px;"><?php echo $lang['buyer']; ?></td>
	</tr>
<?php
$count = $db->count( 'in_buyers', 'id' );
$limit = 25;
list( $pager, $limit )=pager( $limit, $count, BASE . '/admin/buyers/page/' );
$res = $db->query( "SELECT * FROM in_buyers ORDER BY `date` DESC " . $limit );
$i = 0;
while( $row = $db->fetch( $res ) )
{
	$sms_array = array(
				'sms'=>'SMS',
				'paypal'=>'PayPal'
	);
	
	$query = $db->query( "SELECT * FROM pages WHERE id = " . $row['page_id'] );
	$site = $db->fetch( $query );
		if($site['url'] == '')
		{
			$site_url = '<i>' . $lang['votes_deleted-page'] . '</i>';
		}
		else
		{
			$site_url = '<a href="'.$site['url'].'" target="_blank">'.$site['title'].'</a>';
		}
	
		if( $row['payment_type'] == 'sms' )
		{
			$new_value = number_format( $row['price'] * 0.01, 2, '.', ' ' );
			$new_value = number_format( $new_value / 0.702804, 2, '.', ' ');
		}
		else
		{
			$new_value = number_format( $row['price'] * 0.01, 2, '.', ' ' );
		}
	
		echo '<tr>
			   <td class="row2">
				 ' . $site_url . '
			   </td>
			   <td class="row2">
			     <center>' . $sms_array[ $row['payment_type'] ] . '</center>
			   </td>
			   <td class="row2" align="center">
				 ' . $row['unlock_code'] . '
			   </td>
			   <td class="row2" align="center">
				 ' . $new_value . ' €
			   </td>
			   <td class="row2" align="center">
			     +' . $row['in'] . '
			   </td>
			   <td class="row2" align="center">
				 ' . $row['ip'] . '
			   </td>
			   <td class="row2" align="center">
				 ' . format_time( $row['date'] ) . '
			   </td> 
			 </tr>';
$i++;
}

echo '</table>';

if( $i == 0 ) 
echo error( $lang['buyers-empty'] );

if( $i != 0 )
echo $pager;
?>