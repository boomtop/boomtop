<?php
if( get_get('id') == 'delete' AND get_get('other') )
{
	$db->query( "DELETE FROM messages WHERE id = " . $db->esc( get_get( 'other' ) ) );
	echo success( $lang['message-deleted'] );
}
if( get_get('id') == 'view' AND get_get('other') )
{
	include ROOT . '/pages/admin/messages/view.php';
}
elseif( get_get('id') == 'reply' AND get_get('other') )
{
	include ROOT . '/pages/admin/messages/reply.php';
}
else
{
?>
<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/messages/" class="active"><?php echo $lang['messages_sent']; ?></a></li>
	</li>
</div>
<div style="margin:5px auto;"></div>
<table class="ipbtable" cellspacing="1" style="width:570px;">
<?php
$count = $db->count( 'messages', 'id' );
$limit = 20;
list( $pager, $limit )=pager( $limit, $count, BASE . '/admin/messages/page/' );
$res = $db->query( "SELECT * FROM messages ORDER BY `id` DESC " . $limit );
$i = 0;
while( $row = $db->fetch( $res ) )
{
	if( $row['read'] == 0 ) 
	{
		$message_type = '<blink><span style="color:red;">' . $lang['message_unread'] . '</span></blink>';
	}
	else
	{
		$message_type = '';
	}
	
		echo '<tr style="border-bottom: 1px solid #dbdbdb; vertical-align:middle;"> 
				<td class="row2" style="font-weight:bold; background-color:#D8D8D8; vertical-align:middle; width: 85px;">
				  <center>
					' . $lang['message_id'] . ' ' . $row['id'] . '
				  </center>
				</td>
				<td class="row2">
					<b>' . $lang['message_name'] . ':</b> ' . $row['name'] . ' | <b>' . $lang['message_email'] . ':</b> <i>' . $row['email'] . '</i>
				<br />
					<b>' . $lang['message_subject'] . ':</b> ' . $row['subject'] . ' (<a href="' . BASE . '/admin/messages/view/' . $row['id'] . '">' . $lang['message_view'] . '</a>) ' . $message_type . '
				</td>
				<td align="center" class="row1" style="border-left: 1px solid #dbdbdb; width: 75px;">
					<div style="height:2px;"></div>
					<a href="javascript:;" onclick="if (confirm(\'' . $lang['message-delete'] . '\')) document.location.href=\'' . BASE . '/admin/messages/delete/' . $row['id'] . '\' ">
					  <img title="' . $lang['delete'] . '" src="' . BASE . '/style/images/delete.png" alt="" />
					</a>
				</td>
			 </tr>';
$i++;
}

echo '</table>';

if($i == 0) 
echo error( $lang['messages-empty'] );

if( $i != 0 )
echo $pager;
}
?>