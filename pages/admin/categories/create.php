<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/categories/create" class="active"><?php echo $lang['add_cat']; ?></a></li>
		<li class="sepr">|</li>	
		<li><a href="<?php echo BASE ?>/admin/categories/"><?php echo $lang['categories']; ?></a></li>
	</li>
</div>
<div style="margin:5px auto;"></div>
<?php
if( get_post( 'add_cat' ) )
{
  if( get_post( 'category_title' ) != '' )
  {
     $insert_data = array(
					  'title'=>get_post('category_title'),
					  'seostring'=>seostring(get_post('category_title'))
                      );
     $db->insert_array( 'categories', $insert_data );
	echo success( $lang['creat_cat-success'] );
  }
  else
  {  
	echo error( $lang['cat-title-empty'] );
  }
}
?>
<form method="post">
	<table class="ipbtable" cellspacing="1" style="width: 570px;">
		<tr><td class="row2"><?php echo $lang['cat_name']; ?></td><td class="row1"><input type="text" name="category_title" style="width:290px;position:relative;top:5px;" value="" /></td></tr>
		<tr><td class="row3"></td><td class="row3"><input type="submit" value="<?php echo $lang['creat_cat']; ?>" class="btn btn-success" name="add_cat" /></td></tr>
	</table>
</form>