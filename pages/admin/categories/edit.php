<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/categories/edit/<?php echo (int)get_get( 'other' ); ?>" class="active"><?php echo $lang['edit_cat']; ?>:</a> ID: <?php echo (int)get_get( 'other' ); ?></li>
	</li>
</div>
<div style="margin:5px auto;"></div>
<?php
$id = (int)get_get( 'other' );
$res = $db->query( "SELECT * FROM categories WHERE id = " . $id );
$row = $db->fetch( $res );
if( $row['id'] )
{
if( get_post( 'edit_cat' ) )
{
  if( get_post( 'category_title' ) != '' )
  {
     $update_data = array(
					  'title'=>get_post('category_title'),
					  'seostring'=>seostring(get_post('category_title'))
                      );
     $db->update_array( 'categories', $update_data, ' id = ' . $id );
	echo success( $lang['cat-edit-success'] );
  }
  else
  {  
	echo error( $lang['cat-title-empty'] );
  }
}
?>
<form method="post">
	<table class="ipbtable" cellspacing="1" style="width: 570px;">
		<tr><td class="row2"><?php echo $lang['cat_name']; ?></td><td class="row1"><input type="text" name="category_title" style="width:290px;position:relative;top:5px;" value="<?php echo $row['title'] ?>"></td></tr>
		<tr><td class="row3"></td><td class="row3"><input type="submit" class="btn btn-success" value="<?php echo $lang['edit_cat']; ?>" name="edit_cat"></td></tr>
	</table>
</form>
<?php
}
else
{
	echo error( $lang['cat_id_exist'] );
}
?>