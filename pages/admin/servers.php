<?php
if( get_get('id') == 'delete' AND get_get('other') )
{
	$db->query( "DELETE FROM servers WHERE id = " . $db->esc( get_get( 'other' ) ) );
	echo success( 'Serveris veiksmīgi izdzēsts' );
	header("refresh:1;url=" . BASE . "/admin/servers/");
}
if( get_get('id') == 'edit' AND get_get('other') )
{
	include ROOT . '/pages/admin/servers/edit.php';
}
else
{
?>
<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/servers/" class="active">Visi pievienotie serveri</a></li>
	</li>
</div>
<div style="margin:5px auto;"></div>

<table class="ipbtable" style="width:570px;" cellspacing="1">
	<tr>
		<td class="row2x" style="width:110px;">Servera nosaukums</td>
		<td class="row2x" style="width:100px;">IP</td>
		<td class="row2x" align="center" style="width:100px;">Pievienots</td>
		<td class="row2x" align="center" style="width:55px;"><?php echo $lang['options']; ?></td>
	</tr>
<?php
$count = $db->count( 'servers', 'id' );
$limit = 50;
list( $pager, $limit )=pager( $limit, $count, BASE . '/admin/servers/page/' );
$res = $db->query( "SELECT * FROM servers ORDER BY `id` DESC " . $limit );
$i = 0;
while( $row = $db->fetch( $res ) )
{
		echo '<tr>
			<td class="row2">
			  ' . $row['title'] . '
			</td>
		    <td class="row2">
			  ' . $row['hostname'] . '
			</td>
			<td class="row2" align="center">
				' . format_time( $row[ 'created_on' ] ) . '
			</td>
			<td class="row2" align="center">
			    <a href="' . BASE . '/admin/servers/edit/' . $row['id'] . '">
				  <img title="' . $lang['edit'] . '" src="' . BASE . '/style/images/edit.png" alt="" />
				</a> 
				<a href="javascript:;" onclick="if (confirm(\'Esi drošs, ka vēlies dzēst šo serveri?\')) document.location.href=\'' . BASE . '/admin/servers/delete/' . $row['id'] . '\' ">
					<img title="' . $lang['delete'] . '" src="' . BASE . '/style/images/delete.png" alt="" />
				</a>
			</td>
			</tr>';
$i++;
}

echo '</table>';

if( $i == 0 ) 
echo error('Nav pievienots neviens serveris.');

if( $i != 0 )
echo $pager;
}
?>