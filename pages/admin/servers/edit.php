<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/servers/edit/<?php echo (int)get_get( 'other' ); ?>" class="active">Labot serveri:</a> ID: <?php echo (int)get_get( 'other' ); ?></li>
	</li>
</div>
<div style="margin:5px auto;"></div>
<?php
$id = (int)get_get( 'other' );
$res = $db->query( "SELECT * FROM servers WHERE id = $id" );
$row = $db->fetch( $res );

if( ! $row['id'] )
{
	redirect( BASE . '/admin/servers/' );
}

if( get_post('edit_server') )
{
    if( get_post('ip') && (int)get_post('port') )
    {
		$server['b']['type']    = 'source';
			$server['b']['ip']      = get_post('ip');
			$server['b']['real_ip'] = gethostbyname( get_post('ip') );
			$server['b']['port']    = get_post('port');
			$server['b']['status']  = 1;

			$status = query_direct($server, 's');
			
			if($status)
			{
				$data = array(
						'hostname'=>get_post('ip'),
						'port'=>get_post('port'),
						'homepage'=>get_post('website'),
						);
						
				$db->update_array( 'pages', $data, ' id = ' . $id );
				
				$server = @query_live('source', get_post('ip'), get_post('port'), 'sp');
				update_server_data( $row['id'], $server);
				
				echo success('Serveris veiksmīgi labots!');
			}
			else
			{
				echo error('Serverim jābūt ieslēgtam, lai labotu serveri!');
			}
    }
    else
	{
		echo error('Servera hostneims/ip un ports ir jānorāda obligāti!');
	}
}
?>
<form method="post">
    <table class="ipbtable" cellspacing="1" style="width: 570px;">
        <tr>
            <td class="row2" align="right" style="width:120px;">Hostneims / IP <font color="red">*</font></td>
            <td class="row3"><input style="width: 150px;position:relative;top:5px;" type="text" value="<?php echo $row['hostname']; ?>" name="ip"></td>
        </tr>
        <tr>
            <td class="row2" align="right">Ports <font color="red">*</font></td>
            <td class="row3"><input type="text" style="width: 50px;position:relative;top:5px;" name="port" value="<?php echo $row['port']; ?>"></td>
        </tr>
        <tr>
            <td class="row2" align="right">Mājaslapa</td>
            <td class="row3"><input type="text" style="width: 250px;position:relative;top:5px;" value="<?php echo $row['homepage']; ?>" name="website"></td>
        </tr>
        <tr>
            <td class="row3" align="right"></td>
            <td class="row3"><input class="btn btn-success" type="submit" name="edit_server" value="Labot"></td>
        </tr>
    </table>
</form>