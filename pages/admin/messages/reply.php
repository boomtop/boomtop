<?php 
$id = (int)get_get( 'other' );
$res = $db->query( "SELECT * FROM messages WHERE id = " . $id );
$row = $db->fetch( $res );
?>
<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/messages/reply/<?php echo $id; ?>" class="active"><?php echo $lang['answer_to-message']; ?>:</a> <?php echo $row['subject']; ?></li>
	</li>
</div>
<div style="margin:5px auto;"></div>
<?php
if( $row['id'] )
{
if( get_post( 'send_message' ) )
{
	$error = array();
	if( !get_post( 'subject' ) )
	{
		$error[] = $lang['contact_subject-need'];
	}
	if( !get_post( 'description' ) )
	{
		$error[] = $lang['contact_subject-descr'];
	}
	
	foreach( $error as $err )
	{
		echo error($err);
	}
	
	if( count($error) == 0 )
	{
		$to = $row['email'];
		$subject = $_POST['subject'];
		$message = $_POST['description'];
			// Send answer to e-mail
			mail($to, $subject, $message, "From: " . $page->get_setting( 'site_email' ) . "");
	  echo success( $lang['answer-sent-success'] );
	}
}
?>
<form method="post">
	<table class="ipbtable" cellspacing="1" style="width:570px;">
		<tr><td style="width:100px;" class="row2"><?php echo $lang['receiver']; ?>:</td><td class="row1"><?php echo $row['name']; ?> (<i><b><?php echo $row['email']; ?></b></i>)</td></tr>
		<tr><td class="row2"><?php echo $lang['message_subject']; ?>:</td><td class="row1"><input type="text" name="subject" style="width:250px;position:relative;top:5px;" value="RE: <?php echo $row['subject']; ?>"></td></tr>
		<tr><td class="row2"><?php echo $lang['view_descr']; ?>:</td><td class="row1">
		<textarea name="description" style="width: 457px; min-height: 150px;position:relative;top:5px;">
=================== <?php echo $row['name']; ?> <?php echo $lang['writte']; ?> ===================
<?php echo "$row[description]\n" ?>
=================== <?php echo $row['name']; ?> <?php echo $lang['writte']; ?> ===================
<?php echo $lang['message_answer']; ?>:</textarea>
		</td></tr>
		<tr><td class="row3"></td><td class="row3"><input class="btn btn-success" type="submit" name="send_message" value="<?php echo $lang['sent_message']; ?>"></td></tr>
	</table>
</form>
<?php
}
else
{
	echo error( $lang['message-id-exist'] );
}
?>