<?php 
$id = (int)get_get( 'other' );
$res = $db->query( "SELECT * FROM messages WHERE id = " . $id );
$row = $db->fetch( $res );
?>
<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/messages/view/<?php echo $id; ?>" class="active"><?php echo $lang['message-view_id']; ?>:</a> <?php echo $row['subject']; ?></li>
	</li>
</div>
<div style="margin:5px auto;"></div>
<?php
if( $row['id'] )
{
	$db->query( "UPDATE `messages` SET `read` = 1 WHERE id = " . $id );
?>
<table class="ipbtable" cellspacing="1" style="width:570px;">
	<tr><td style="width:50px;" class="row2"><b><?php echo $lang['view_sent-time']; ?>:</b></td><td class="row1"><?php echo format_time( $row['sent_time'] ); ?></td></tr>
	<tr><td class="row2"><b><?php echo $lang['view_sender']; ?>:</b></td><td class="row1"><?php echo $row['name']; ?> (<i><b><?php echo $row['email']; ?></b></i>)</td></tr>
	<tr><td class="row2"><b><?php echo $lang['view_descr']; ?>:</b></td><td class="row3"><span style="font-size:13px;"><?php echo bb2html($row['description']); ?></span></td></tr>
	<tr><td class="row3"></td><td class="row3"><div style="margin:3px auto;"></div>
	<input style="margin-left:155px;" class="btn btn-success" onclick="document.location='<?php echo BASE ?>/admin/messages/reply/<?php echo $row['id']; ?>';" type="button" value="<?php echo $lang['answer_to-message']; ?>">
	<div style="margin:4px auto;"></div>
	</td></tr>
</table>
<?php
}
else
{
	echo error( $lang['message-id-exist'] );
}
?>