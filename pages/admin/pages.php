<?php
// Delete page banner
$del_q = $db->query( "SELECT * FROM pages WHERE id = " . $db->esc( get_get( 'other' ) ) );
$delete = $db->fetch( $del_q );

if( get_get('id') == 'delete' AND get_get('other') )
{
	$db->query( "DELETE FROM pages WHERE id = " . $db->esc( get_get( 'other' ) ) );
	@unlink(ROOT . "/style/images/banners/" . $delete['banner']);
	echo success( $lang['my-sites_deleted'] );
}
if( get_get('id') == 'delete_banner' AND (int)get_get('other') )
{
	$db->query( 'UPDATE pages SET `banner` = \'no_banner\'  WHERE id = ' . (int)get_get('other') );
	@unlink(ROOT . "/style/images/banners/" . $delete['banner']);
	redirect( BASE . '/admin/pages/' );
}
if( get_get('id') == 'edit' AND get_get('other') )
{
	include ROOT . '/pages/admin/pages/edit.php';
}
elseif( get_get('id') == 'add-in' AND get_get('other') )
{
	include ROOT . '/pages/admin/pages/add-in.php';
}
else
{
?>
<div id="submenux">
	<li>
		<li><a href="<?php echo BASE ?>/admin/pages/" class="active"><?php echo $lang['pages_all']; ?></a></li>
	</li>
</div>
<div style="margin:5px auto;"></div>
<table class="ipbtable" cellspacing="1" style="width:570px;">
<?php
$count = $db->count( 'pages', 'id' );
$limit = 50;
list( $pager, $limit )=pager( $limit, $count, BASE . '/admin/pages/page/' );
$res = $db->query( "SELECT * FROM pages ORDER BY `added` DESC " . $limit );
$i = 0;
while( $row = $db->fetch( $res ) )
{
$user = $users->info( $row['user_id'] );
$site_banner = $row['banner'] == 'no_banner' ? "style/images/no_banner.jpg":"style/images/banners/" . $row['banner'];	

		echo '<td class="row2">
			  <span style="color:black;font-size:15px;">
				<b>' . $lang['pages_title'] . ':</b> <a href="' . $row['url'] . '" target="_blank" />' . $row['title'] . '</a>
			  </span>
			  <br />
				<img width="468px" height="60px" src="' . BASE . '/' . $site_banner . '" />
			  <br />
			  <center><a style="color:red;" href="javascript:;" onclick="if (confirm(\'Vai esi drošs, ka vēlies dzēst šo banneri?\')) document.location.href=\'' . BASE . '/admin/pages/delete_banner/' . $row['id'] . '\'">[ Dzēst banneri ]</a></center>
		
				<b>' . $lang['pages_descr'] . ':</b> <span style="font-size:12px;">' . $row['description'] . '</span>
			  <br />
			  <span style="font-size:11px;">
			    <b>' . $lang['pages_added'] . '</b> ' . format_time($row['added']) . ', ' . $lang['pages_user'] . ': <i>' . $user['username'] . '</i>
			  </span>
			  </td>
			  <td align="center" class="row1" style="border-left: 1px solid #dbdbdb; width: 75px;">
			   <div style="height:7px;"></div>
			    <a href="' . BASE . '/admin/pages/add-in/' . $row['id'] . '">
				  <img title="' . $lang['edit'] . '" src="' . BASE . '/style/images/add.png" alt="" />
				</a>
			    <a href="' . BASE . '/admin/pages/edit/' . $row['id'] . '">
				  <img title="' . $lang['edit'] . '" src="' . BASE . '/style/images/edit.png" alt="" />
				</a> 
				  <a href="javascript:;" onclick="if (confirm(\'' . $lang['my-sites_delete'] . '\')) document.location.href=\'' . BASE . '/admin/pages/delete/' . $row['id'] . '\' ">
				    <img title="' . $lang['delete'] . '" src="' . BASE . '/style/images/delete.png" alt="" />
				  </a>
			    </td>
			</tr>';
$i++;
}

echo '</table>';

if( $i == 0 ) 
echo error( $lang['sites_empty'] );

if( $i != 0 )
echo $pager;
}
?>