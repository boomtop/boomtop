<div id="submenux">
    <li><a href="<?php echo BASE ?>/admin/pages/add-in/<?php echo (int)get_get( 'other' ); ?>" class="active"><?php echo $lang['add-in_site_page']; ?>:</a> ID: <?php echo (int)get_get( 'other' ); ?></li>
</div>
<div style="margin:5px auto;">
    <?php
    $id = (int)get_get( 'other' );
    if( !$id ){
        echo error($lang['page_not_exist_text']);
    }else{
        $res = $db->query( "SELECT * FROM pages WHERE id = $id" );
        $row = $db->fetch( $res );
        if( !$row ){
            echo error($lang['page_not_exist_text']);
        }else{
            if (get_post('add_in') && (int)get_post('in')) {
                $in = (int)get_post('in');
                $db->query("UPDATE `pages` SET `in` = `in` + $in WHERE `id` = $id");
                echo success($lang['add-in-success']);
            }
            ?>
            <form method="post">
                <table class="ipbtable" cellspacing="1" style="width: 570px;">
                    <tr>
                        <td class="row2" align="right" width="95px;"><?php echo $lang['add-in_site_page']; ?></td>
                        <td class="row1">
                            <input type="text" name="in" style="width:425px;position:relative;top:5px;" value="0"/>
                        </td>
                    </tr>
                    </tr>
                    <tr>
                        <td class="row3"></td>
                        <td class="row3">
                            <input class="btn btn-success" type="submit" value="<?php echo $lang['add-in_site_page']; ?>" name="add_in"/>
                        </td>
                    </tr>
                </table>
            </form>
            <?php
        }
    }
    ?>
</div>