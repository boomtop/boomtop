<?php
if( get_post('update_settings') )
{
    foreach( $_POST as $key => $value )
    {
        $db->query( "UPDATE settings SET value = " . $db->esc($value) . " WHERE setting = " . $db->esc($key) );
    }
    echo success( $lang['settings-updated'] );
}

echo '
<form method="post">
	<table class="ipbtable" cellspacing="1" style="width: 570px;">';
	$res = $db->query('SELECT * FROM settings WHERE setting NOT IN (\'default_site_language\')');
	$i = 0;
	while($row=$db->fetch($res))
	{
    $i++;
    echo '<tr>
			<td class="row2" style="width:150px;">' . $row['title'] . '</td>
			<td class="row1"><input type="text" value="' . $row['value'] . '" style="width:180px;position:relative;top:5px;" name="' . $row['setting'] . '"></td>
		 </tr>';
	}
	$res = $db->query('SELECT * FROM settings WHERE setting IN (\'default_site_language\')');
	while($row=$db->fetch($res))
	{
	echo '<tr>
			<td class="row2" style="width:150px;">' . $row['title'] . '</td>
			<td class="row1">';
			$languages = array(
					'lv'=>$lang['lang-lv'],
					'en'=>$lang['lang-en'],
					'ru'=>$lang['lang-ru']
					);	
			echo "<select style='position:relative;top:5px;padding:2px; height: 25px;border:1px solid #ccc;' name='{$row['setting']}'>";
				foreach( $languages as $language=>$value ) {

					$languages =''; 
					
					if( $row['value'] == $language ) { $languages = 'selected'; }
					echo "<option " . $languages . " value='" . $language . "'>" . $value . "</option>";
				};
			echo '</select>
			</td>
		 </tr>';
	}
	echo '<tr>
			<td class="row3"></td>
			<td class="row3"><input type="submit" class="btn btn-success" name="update_settings" value="' . $lang['edit_setting'] . '"></input></td>
		 </tr>';
	echo '
	</table>
</form>';
?>