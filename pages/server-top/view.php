<?php
$id = (int)get_get('id');
$msg = '';
$ip = $db->esc(IP);
$time = time();
$balsojis = $db->count('server_ratings', 'type', " type = 'server' AND parentid = $id AND ip = " . $ip . " AND date >= " . strtotime( 'today' ) . "");

$vote_name = substr( md5( IP . 'Te bija sLIDe' ), 0, 8 );
$referer = get_server('HTTP_REFERER');

if(get_post($vote_name) && $referer && in_array(parse_url($referer, PHP_URL_HOST), ['boomtop.net', 'www.boomtop.net']) && !isTorIp($_SERVER['REMOTE_ADDR']) && !isProxyIp($_SERVER['REMOTE_ADDR']))
{
        if(!$balsojis)
        {
            $db->query('UPDATE servers SET rating = rating+1 WHERE id = ' . $id);
			
            $data = array('type'=>'server',
                          'parentid'=>$id,
                          'ip'=>IP,
                          'date'=>time(),
                          'value'=>'5');
						  
            $db->insert_array('server_ratings', $data);
					
            $msg = success($lang['serv-voting']);
            update_top();
            $balsojis = true;
        }
        else
        {
            $msg = error($lang['serv-voted']);
        }
}

$res = $db->query('SELECT * FROM servers WHERE id = ' . $id);
$row = $db->fetch($res);

if( ! $row['id'] )
{
	redirect( BASE . '/server-top/' );
}

if( substr($row['homepage'], 0, 4) != 'http' ){
	$row['homepage'] = 'http://' . $row['homepage'];
}

$page->set_page_title( 'Serveris - ' . $row['title'] );

echo '<h1><i class="icon-th-list"></i> <a href="' . BASE . '/server-top/">' . $lang['top-serv'] . '</a> » ' . ( ( empty( $row['title'] ) ) ? $row['hostname'] : $row['title'] ) . '</h1>';

$max = $db->fetch($db->query('SELECT rating FROM servers ORDER BY rating DESC LIMIT 1'));
$MAX_VOTES = $max['rating'];
$map_dir = '/maps/';
$map_path = ROOT . '/style/images' . $map_dir . $row['map'] . '.jpg';
$map_url = BASE . '/style/images' . $map_dir . $row['map'] . '.jpg';
$map = file_exists( $map_path ) ? $map_url : BASE . '/style/images/maps/nopicture-' . $row['type'] . '.jpg';

echo $msg;

if( $row['top_place'] == 1 )
{
	$server_place = '<img title="1 vieta" src="' . BASE . '/style/images/first_place.jpg">';
}
elseif( $row['top_place'] == 2 )
{
	$server_place = '<img title="2 vieta" src="' . BASE . '/style/images/second_place.jpg">';
}
elseif( $row['top_place'] == 3 )
{
	$server_place = '<img title="3 vieta" src="' . BASE . '/style/images/third_place.jpg">';
}
else
{
	$server_place = $row['top_place'];
}

$types = array(
	'cs' => 'Counter-Strike',
	'csgo' => 'Counter-Strike: GO',
	'MC' => 'Minecraft',	
);
?>
<div class="left-content drop-shadow lifted">
	<div style="float:left; width:70%;">
        <table class="ipbtable" style="float:left; width:70%;">
			<tr><td class="row2" align="right" style="width:90px;"><?php echo $lang['serv-place']; ?>:</td><td class="row3"><?php echo $server_place; ?></td></tr>
            <tr><td class="row2" align="right"><?php echo $lang['serv-type']; ?>:</td><td class="row3"><?php echo $types[ $row['type'] ]; ?></td></tr>
            <tr><td class="row2" align="right"><?php echo $lang['serv-host']; ?>:</td><td class="row3"><?php echo $row['hostname'] . ':' . $row['port']; ?></td></tr>
			<tr><td class="row2" align="right"><?php echo $lang['serv-players']; ?>:</td><td class="row3"><span style="color:green;"><?=$row['players'] . '</span> / <span style="color:red;">' . $row['max_players']?></span></td></tr>
            <tr><td class="row2" align="right"><?php echo $lang['serv-map']; ?>:</td><td class="row3"><?=$row['map']?></td></tr>
            <tr><td class="row2" align="right"><?php echo $lang['serv-web']; ?>:</td><td class="row3"><a href="<?php echo $row['homepage']; ?>" target="_blank"><?php echo $row['homepage']; ?></a></td></tr>
            <tr><td class="row2" align="right"><?php echo $lang['serv-uptime']; ?>:</td><td class="row3"><?=@round($row['successful_cons']/$row['total_cons']*100)?>%</td></tr>       
            <tr><td class="row2" align="right"><?php echo $lang['serv-status']; ?>:</td><td class="row3"><?=$row['online'] ? '<span style="color:green;">Ieslēgts</span>':'<span style="color:red;">Izslēgts</span>'?></td></tr>
            <tr><td class="row2" align="right"><?php echo $lang['serv-rr']; ?>:</td><td class="row3"><?=format_time($row['last_conn'])?></td></tr>
            <tr><td class="row2" align="right"><?php echo $lang['serv-votes']; ?>:</td><td class="row3"><img title="" src="<?php echo BASE; ?>/style/images/thumbs_up.png" style="height: 16px;"> <span style="position: relative; top: 1px; left: 1px;"><?php echo $row['rating']; ?> (<a href="<?php echo BASE; ?>/buy-votes/<?php echo (int)$id; ?>/"><?php echo $lang['serv-buyvotes']; ?></a>)</span></td></tr>
			<tr><td class="row2" align="right"><?php echo $lang['serv-vote2']; ?>:</td><td class="row3">
            <?php
            if(!$balsojis)
            {
            ?>
            <form method="post"><input type="submit" class="btn btn-success" name="<?php echo $vote_name; ?>" style="position:relative;top:10px;" value="<?php echo $lang['serv-vote']; ?>"></form>
            <?php
            }
            else
            {
				echo $lang['serv-voted'];
            }
            ?>
            </td></tr>
            <tr><td class="row2" align="right"><?php echo $lang['serv-url']; ?>:</td><td class="row3"><input style="cursor:auto;width:275px;position:relative;top:6px;" readonly type="text" value="<?=BASE . '/server-top/view/' . $id . '/' . $row['seo_title']?>"></td></tr>
		</table>
	</div>
    <div style="float:right;">
        <?php
        	echo  '<img src="'.$map.'" alt="" />';
        ?>
        <table class="ipbtable" cellspacing="0" cellpadding="0" style="width: 160px;margin-top:5px;">
            <tr bgcolor="#e9f3f8"><th bgcolor="#e9f3f8" style='text-align:left;padding:5px;'><?php echo $lang['serv-nick']; ?></th><th bgcolor="#e9f3f8" style='text-align:right;padding:5px;'><?php echo ( $row['type'] == 'cs' or $row['type'] == 'csgo' ) ? $lang['serv-frags'] : $lang['serv-img']?></th></tr>
        <?php			
			if( $row['type'] == 'cs' or $row['type'] == 'csgo' )
			{
				$players = unserialize($row['player_info']);
				
				usort($players,create_function('$a, $b', 'return $a["score"] > $b["score"] ? 0 : 1;'));
				$nr = 0;
				foreach($players as $player)
				{
					$nr++;
					echo sprintf('<tr class="%s"><td>%s</td><td style="text-align:right;">%s</td></tr>',($nr%2==0?'row3':'row2'), shorten( $player['name'], 14 ), $player['score']);
				}
			}
			elseif( $row['type'] == 'MC' )
			{
				$Query = new MinecraftQuery( );
				
				try
				{
					$Query->Connect( $row['hostname'], $row['port'], 1 );
				}
				catch( MinecraftQueryException $e )
				{
					$Error = $e->getMessage();
				}
				
				$Players = $Query->GetPlayers();
				
				$nr = 0;

				if( $row['players'] > 0 )
				{
					foreach( $Players as $Player )
					{
						$nr++;
						echo sprintf('<tr class="%s"><td>%s</td><td style="text-align:right;">%s</td></tr>',($nr%2==0?'row3':'row2'), shorten( $Player, 14 ), '<img src="https://minotar.net/avatar/' . $Player . '/16.png" alt="" />');
					}
				}
			}
        ?>
        </table>
    </div>
	<div class="clearfix"></div>
</div>
