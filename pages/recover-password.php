<?php
/**
 * @var $lang   - array
 * @var $db     - database object
 */

$page->set_page_title($lang['recover-password-subject']);

include ROOT . '/pages/recover-password/controller.php';
$recoverPassword = new RecoverPassword($db);

if (!empty(get_post('send_email'))) {
    $email = $recoverPassword->postEmail(get_post('email'));
    if (!$email) {
        $msg = error($lang['recover-password-not_found']);
    } else {
        $msg = success( $lang['recover-password-email-ok'] );
    }
}

if (!empty(get_post('change_password'))) {
    $changePassword = $recoverPassword->changePassword($_POST, get_get('id'));
    if (!empty($changePassword['error'])) {
        $msg = error($changePassword['error']);
    } else {
        $msg = success( $lang['recover-password-ok'] );
    }
}
?>

<h1><?php echo $lang['recover-password-subject']; ?></h1>

<?php

if (isset($msg)) {
    echo $msg;
}

if (!empty(get_get('subact') && get_get('subact') == 'hash' && !empty(get_get('id'))) && $recoverPassword->validHash(get_get('id'))) {
    include ROOT . '/pages/recover-password/password.php';
} else {
    include ROOT . '/pages/recover-password/email.php';
}
