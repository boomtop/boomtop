<?php
$id = (int)get_get( 'id' );
$res = $db->query( "SELECT * FROM pages WHERE id = " . $id );
$row = $db->fetch( $res );
$user = $users->info();
define( "MAX_SIZE", "300" );
if( $row['id'] )
{
if( $user['id'] == $row['user_id'] )
{
if( get_post( 'edit_page' ) )
{
 $error = array();
  if( !get_post( 'title' ) )
    {
         $error[] = $lang['add-page_title-need'];
    }
    if( !get_post( 'url' ) )
    {
         $error[] = $lang['add-page_url-need'];
    }
    if( !get_post( 'description' ) )
    {
         $error[] = $lang['add-page_descr-need'];
    }

	if(	$_FILES[ 'banner' ][ 'name' ] != '' )
   {
	$image = $_FILES['banner']['name'];
	$filename = stripslashes($_FILES['banner']['name']);
	$extension = getExtension($filename);
 	$extension = strtolower($extension);
	
	if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif")) 
 	{
 		$error[] = $lang['add-page_ext-allowed'];
 	}
 
	 $size = filesize($_FILES['banner']['tmp_name']);

	if( $size > MAX_SIZE*1024 )
	{
		$error[] = $lang['add-page_image-size'];
	}
	
    list( $width, $height ) = getimagesize( $_FILES['banner']['tmp_name'] );
	
	if( $width != 468 AND $height != 60 )
	{
		$error[] = 'Banera bildes izmēri nedrīkst būt mazāki vai lielāki par 468x60.';
	}

	$image_name = time() . '.' . $extension;
	$banner = $image_name;
		
	if(!count($error) >= 1)
	{
		move_uploaded_file($_FILES[ 'banner' ][ 'tmp_name' ], ROOT . "/style/images/banners/" . $banner);    
	}
	}
	elseif( get_post( 'banner_url' ) != '' )
	{
		define('allowed_url_images', 'jpg|jpeg|gif|png'); 
		define('image_directory', 'style/images/banners'); 
	
		if(!preg_match('#^http://.*([^/]+\.('.allowed_url_images.'))$#', get_post('banner_url'), $file)) { 
			$error[] = $lang['add-page_ext-url-allowed']; 
		}
 
		if(!$image = @file_get_contents(get_post('banner_url'))) { 
			$error[] = $lang['add-page_get-content'];
		}
	
		list( $width, $height ) = getimagesize( get_post('banner_url') );
		
		if( $width != 468 AND $height != 60 )
		{
			$error[] = 'Banera bildes izmēri nedrīkst būt mazāki vai lielāki par 468x60.';
		}	
	
		$image_name_url = time() . '.' . $file[1];
		$banner = $image_name_url;
 
		if(!count($error) >= 1)
		{
		if(!$f = fopen(image_directory . '/' . $banner, 'w')) { 
			$error[] = $lang['add-page_open-file']; 
		}
 
		if (fwrite($f, $image) === FALSE) { 
			$error[] = $lang['add-page_upload-to-server']; 
		}
 
		fclose($f);
	  }
	}
	else
	{
		$query = $db->query( "SELECT `banner` FROM pages WHERE id = " . $id );
		$update = $db->fetch( $query );
		$banner = $update[ 'banner' ];
	}
  
	if(count($error) >= 1)
	{
	echo "<div class='message error' style='width:490x;'>";
	foreach($error as $err)
	{
		echo "$err<br />";
	}
	echo "</div>";
	}
	else
	{
        $update_data = array(
					  'title'=>get_post('title'),
                      'url'=>get_post('url'),
                      'description'=>get_post('description'),
					  'banner'=>$banner,
                      'category'=>get_post('page_cat'),
					  'counter_style'=>get_post('counter_style')
                      );
        $db->update_array( 'pages', $update_data, ' id = ' . $id );
		
	echo success( $lang['edit-my-site-success'] );
  }
}
$site_banner = $row['banner'] == 'no_banner' ? "style/images/no_banner.jpg":"style/images/banners/" . $row['banner'];

$visits = $db->query( "SELECT * FROM out_log WHERE `page_id` = " . $row['id'] . " AND `time` >= " . strtotime( "Today" ) );
$today_visits = $db->rows( $visits );
?>
<div class="left-content drop-shadow lifted">
<div id="submenux">
	<li>
		<li><a href="<?php echo BASE; ?>/my-sites/edit/<?php echo (int)get_get( 'id' ); ?>" class="active"><?php echo $lang['edit_site_page']; ?>:</a> ID: <?php echo (int)get_get( 'id' ); ?></li>
	</li>
</div>
<div style="margin:5px auto;"></div>
<link rel="stylesheet" href="<?php echo BASE ?>/style/counter.css" type="text/css" />
<form method="post" enctype="multipart/form-data">
	<table class="ipbtable" cellspacing="1" style="width: 570px;margin-bottom: -15px;">
		<tr><td class="row2" align="right" width="95px;"><?php echo $lang['add-page_site-name']; ?></td><td class="row1"><input type="text" name="title" style="width:425px;position:relative;top:5px;" value="<?php echo $row['title']; ?>" /></td></tr>
		<tr><td class="row2" align="right" width="95px;"><?php echo $lang['add-page_site-url']; ?></td><td class="row1"><input type="text" name="url" style="width:425px;position:relative;top:5px;" value="<?php echo $row['url']; ?>" /></td></tr>
		<tr><td class="row2" align="right" width="95px;"><?php echo $lang['add-page_now-banner']; ?></td><td class="row1"><img style="width:440px;" src="<?php echo BASE ?>/<?php echo $site_banner; ?>" /></td></tr>
		<tr><td class="row2" align="right"><?php echo $lang['add-page_banner']; ?></td><td class="row1">
		<div class="upload_type">
			<a href="javascript:;" class="upload_from_pc" class="upload-item"><?php echo $lang['add-page_upload-from-pc']; ?></a><a href="javascript:;" class="upload_from_internet"><?php echo $lang['add-page_upload-from-url']; ?></a>
				<div style="height:8px;"></div>
				<div id="upload_from_pc" class="upload-item">
					<input type="file" name="banner" />
				</div>
				<div id="upload_from_internet" style="display: none;" class="upload-item">
					<input type="text" name="banner_url" style="width:420px;position:relative;top:5px;" value="" />
			  </div>
		</div>
		</td></tr>
		<tr><td class="row2" align="right" width="95px;"><?php echo $lang['add-page_descr']; ?></td><td class="row1"><textarea name="description" style="width: 420px; position:relative;top:5px; min-height: 100px;" /><?php echo $row['description']; ?></textarea></td></tr>
		<tr><td class="row2" align="right"><?php echo $lang['add-page_category']; ?></td><td class="row1">
		<select name="page_cat" style="position:relative;top:5px;padding:2px; height: 25px;border:1px solid #ccc;" />
		<?php
		$cat_q = $db->query( "SELECT * FROM categories ORDER BY `id` ASC" );
		while( $category = $db->fetch( $cat_q ) )
		{
			$cat = $db->query( "SELECT * FROM pages WHERE id = " . (int)get_get( 'id' ) );
			$cat_id = $db->fetch( $cat );
			$selected = $cat_id[ 'category' ] == $category[ 'id' ] ? 'selected':'';
			?>
			<option value="<?php echo $category['id']; ?>" <?php echo $selected; ?>><?php echo $category['title']; ?></option>
			<?php
		}
		?>
		</select>
		<tr><td class="row2" align="right"><?php echo $lang['counter_style']; ?> <font color="red">*</font></td>
		<td class="row1">
		<div style="float:left; width:430px;">
		<div style="float:left; width:100px;">
			<input <?php echo $row['counter_style'] == 1 ? 'checked ' : ''; ?>style="margin-left:35px;margin-bottom:5px;" type="radio" name="counter_style" value="1" />
			<div class='counters'>
				<div class='counter_1' align='center'>
					<div class='visits' style="padding-top:20px"><?php echo $today_visits; ?></div>
					<div class='text' style="padding-top:1px">Apmeklētāji</div>
				</div>
			</div>
		</div>
		<div style="float:left; width:100px;">
			<input <?php echo $row['counter_style'] == 2 ? 'checked ' : ''; ?>style="margin-left:35px;margin-bottom:5px;" type="radio" name="counter_style" value="2" />
			<div class='counters'>
				<div class='counter_2'>
					<div class='text'>
						<div class='left'><span>IN</span><b><?php echo $row['in']; ?></b></div>
						<div class='right'><span>OUT</span><b><?php echo $row['out']; ?></b></div>
					</div>
				</div>
			</div>
		</div>
		<div style="float:left; width:100px;">
			<input <?php echo $row['counter_style'] == 3 ? 'checked ' : ''; ?>style="margin-left:35px;margin-bottom:5px;" type="radio" name="counter_style" value="3" />
			<div class='counters'>
				<div class='counter_3'></div>
			</div>
		</div>
		</div>
		</td></tr>
		<tr><td class="row3"></td><td class="row3"><input class="btn btn-success" type="submit" value="<?php echo $lang['edit_site_page']; ?>" name="edit_page" /></td></tr>
	</table>
</form>
</div>
<?php
}
else
{
	echo error( $lang['edit-mysite_denied'] );
}
}
else
{
	echo error( $lang['page_not_exist_text'] );
}
?>