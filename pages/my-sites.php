<?php $user = $users->info(); ?>
<h1><i class="icon-tasks"></i> <?php echo $lang['your_added_pages']; ?> (<span style="color:darkblue;"><?php echo $user['my_sites']; ?></span>)</h1>
<?php
$page->need_login();

$page->set_page_title( $lang['your_added_pages'] );

$del_q = $db->query( "SELECT * FROM pages WHERE id = " . $db->esc( get_get( 'id' ) ) );
$delete = $db->fetch( $del_q );

if( get_get('subact') == 'delete' AND get_get('id') AND get_cookie('user_id') == $delete['user_id'] )
{
	$db->query( "DELETE FROM pages WHERE id = " . $db->esc( get_get( 'id' ) ) );
	@unlink(ROOT . "/style/images/banners/" . $delete['banner']);
	redirect( BASE . '/my-sites/' );
}
if( get_get('subact') == 'edit' AND get_get('id') )
{
	include ROOT . '/pages/my-sites/edit.php';
}
else
{
$count = $db->count( 'pages', 'id' );
$limit = 25;
list( $pager, $limit )=pager( $limit, $count, BASE . '/my-sites/page/' );
$res = $db->query( "SELECT * FROM pages WHERE user_id = '" . get_cookie('user_id') . "' ORDER BY added DESC " . $limit );
$i = 0;
echo '<div class="left-content drop-shadow lifted">';
while( $row = $db->fetch( $res ) )
{
	if(	$row['place'] == 1 ){ $place_img = '<div id="top_site_rank_1st">' . $row['place'] . '</div>'; }
	elseif( $row['place'] == 2 ){ $place_img = '<div id="top_site_rank_2st">' . $row['place'] . '</div>'; }
	elseif( $row['place'] == 3 ){ $place_img = '<div id="top_site_rank_3st">' . $row['place'] . '</div>'; }
	else{ $place_img = '<div id="top_site_rank_random">' . $row['place'] . '</div>'; }
	$site_banner = $row['banner'] == 'no_banner' ? "style/images/no_banner.jpg":"style/images/banners/" . $row['banner'];
?>
<table class="ipbtable_new" style="width: 570px;<?php if( $i == 0 ) { echo 'border-top: 1px solid #dbdbdb;'; }?>">
	<tr><td class="top_row">
    <div style="float:left; width:40px; padding-top:18px;">
		    <?php echo $place_img; ?>
			<div style="height:10px;"></div>
			<a href="<?php echo BASE ?>/my-sites/edit/<?php echo $row['id']; ?>"><img title="<?php echo $lang['edit'] ?>" src="<?php echo BASE ?>/style/images/edit.png" alt="" /></a> 
			<a href="javascript:;" onclick="if (confirm('<?php echo $lang['my-sites_delete']; ?>')) document.location.href='<?php echo BASE ?>/my-sites/delete/<?php echo $row['id']; ?>' "><img title="<?php echo $lang['delete'] ?>" src="<?php echo BASE ?>/style/images/delete.png" /></a>
		</div>
        <div style="float:left; width:468px;">
            <span style="color:black;font-size:15px;">
			<a href="<?php echo BASE ?>/out/<?php echo $row['id']; ?>/" target="_blank" /><?php echo $row['title']; ?></a>
			</span>
			<span style="float:right;">
				<a style="color:darkblue;" href="<?php echo BASE ?>/buy-in/<?php echo $row['id']; ?>/" ><?php echo $lang['more_in']; ?> IN?</a>
			</span><br />
			<a href="<?php echo BASE ?>/out/<?php echo $row['id']; ?>/" target="_blank" /><img width="468px" height="60px" src="<?php echo BASE ?>/<?php echo $site_banner; ?>" /></a>
			<br />
            <span style="font-size:12px;"><?php echo $row['description']; ?></span>
			<br />
			<a href="javascript:showhide('show_html_code_<?php echo $row['id']; ?>')">
				<img src="<?php echo BASE ?>/style/images/list.gif"> <?php echo $lang['my-sites_html_code']; ?>
			</a>
		<div id="show_html_code_<?php echo $row['id']; ?>" style="display: none;">
<textarea readonly="readonly" style="width: 500px; min-height: 87px;cursor:auto;">
&lt;!-- begin <?php echo WEBSITE_NAME ?> stats counter --&gt;
&lt;iframe src="<?php echo BASE ?>/counter.php?id=<?php echo $row['id']; ?>" width="88" height="53" border="0" frameborder="0"&gt;&lt;/iframe&gt;
&lt;!-- end <?php echo WEBSITE_NAME ?> stats counter --&gt;</textarea>
		</div>
		</div>
        <div style="float:left; width:45px; padding-top:17px;">
        <table class="toptable" cellspacing="0" style="width: 50px;">
            <tr><td class="top_in" title="IN"><?php echo $row['in']; ?></td></tr>
            <tr><td class="top_out" title="OUT"><?php echo $row['out']; ?></td></tr>
        </table>
		</div>
	</tr></td>
</table>
<?php
$i++;
}

if( $i == 0 )
echo '<span style="padding-left: 5px;">' . $lang['my-sites_empty'] . '</span>';

echo '</div>';

if( $i != 0 )
echo $pager;
}
?>