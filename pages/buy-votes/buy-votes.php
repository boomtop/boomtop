<?php
if( get_post('buy-votes') )
{
	$price = 0;

	$fields_empty = suncore::array_values_empty( $_POST );

	$keys = array( 'server_id', 'payment-type', 'currency-sms', 'currency-paypal', 'code'  );

	if( empty($_POST["payment-type"]) === false && $fields_empty === false )
	{
		$fields_empty = true;
		
		if( empty($_POST[ "currency-" . ( $_POST["payment-type"] ) ]) === false )
		{
			$price = $_POST[ "currency-" . ( $_POST["payment-type"] ) ];
			$fields_empty = false;
		}
	}
	else
	{
		$fields_empty = true;
	}

	if( $fields_empty == false )
	{
	
		list($type, $message) = suncore::server_shop(
			$row['id'],
			$_POST[ "payment-type" ],
			$_POST[ "code" ],
			$price
		);
		
	}
}
?>
<div class="suncore-content suncore-featured-shadow-bg">
	<div class="suncore-adverts-panel suncore-wiget">
		<?php if( isset($type, $message) === true ): ?>
			<div class="message <?php echo ($type ? "success" : "error"); ?>">
				<?php echo $message; ?>
			</div>
		<?php endif; ?>
		<form action="#" method="post" class="suncore-form" >
			<center>
				<label for="payment-type"><?php echo $lang['buy-in_payment-type']; ?></label>
				<select class="payment" name="payment-type" id="payment-type">
					<option value="">- <?php echo $lang['choose']; ?> -</option>
						<?php
							echo isset( $config['sms_times'] ) ? '<option ' . suncore::selected("payment-type", "sms") . ' value="sms">SMS</option>' : '';
							echo isset( $config['paypal_times'] ) ? '<option ' . suncore::selected("payment-type", "paypal") . ' value="paypal">PayPal</option>' : '';
						?>
				</select>
			</center>
			<?php if( suncore::config('paypal_times') !== false ): ?>
		<center>	
			<p class="time sms">
				<label for="currency-sms">Izvēlies balsu lielumu</label>
				<select class="in" name="currency-sms" id="currency-sms">
					<?php
						foreach( $config['sms_times']['lv'] as $price => $time )
						{
							$new_value = number_format( $price * 0.01, 2, '.', ' ' );
							$new_value = number_format( $new_value, 2, '.', ' ');
							
							echo '<option ' . suncore::selected("currency-sms", $price) . ' value="' . $price . '">' . $new_value . ' EUR - ' . $time . ' balsis</option>';
						}
					?>
				</select>
			</p>
		</center>
			<?php endif; ?>
		<center>
			<?php if( suncore::config('paypal_times') !== false ): ?>
			<p class="time paypal">
				<label for="currency-paypal">Izvēlies balsu lielumu</label>
				<select class="in" name="currency-paypal" id="currency-paypal">
				<?php
					foreach( $config['paypal_times'] as $price => $time )
					{
						$new_price = number_format( $price * 0.01, 2, '.', ' ' );
						
						echo '<option ' . suncore::selected("currency-paypal", $price) . ' value="' . $price . '">'.$new_price.' EUR - ' . $time . ' balsis</option>';
					}
				?>
				</select>
		</center>
			<?php endif; ?>

				<div class="suncore-adverts-paytype sms">
					<p>
					<?php echo $lang['buy-in_sms-payment']; ?>
					</p>
				</div>
				
				<div class="suncore-adverts-paytype paypal">
					<p><?php echo $lang['buy-in_paypal-payment']; ?> <br /><br />
						<a href="javascript:;" onclick="submitPaypal();">
							<img src="<?php echo BASE ?>/themes/<?php echo SITE_THEME ?>/suncore/images/paypal_click_01.gif" alt="" />
						</a>
					</p>
				</div>

			<div class="suncore-adverts-submit-button">
				<div class="suncore-unlock-code">
					<label for="code"><?php echo $lang['buy-in_unlock_code']; ?></label>
					<input type="text" name="code" id="code"/>
				</div>
				<input type="submit" name="buy-votes" value="<?php echo $lang['buy-in_accept']; ?>" />
			</div>
		</form>

		<div class="suncore-rules">
			<div class="suncore-rules-title">Izlasi!</div>
			<ul class="suncore-rules-list suncore-list-dot">
				<li>Nauda pēc balsu pasūtīšanas netiek atmaksāta;</li>
				<li>Jautājumu gadījumā lūdzu sazināties ar <a href="/contacts/">administrāciju</a></li>
			</ul>
		</div>
	</div>
</div>