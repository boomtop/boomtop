<?php
$page->need_login();
$page->set_page_title( $lang['admin_panel'] );
$user = $users->info();
if( $user[ 'admin_access' ] == 1 )
{
if( !array_key_exists( get_get( 'subact' ), $page->admin_pages ) )
{
?>
<h1><i class="icon-wrench"></i> <?php echo $lang['admin_panel']; ?></h1>
<?php
echo success( $lang['admin_choose'] );
echo '<div class="left-content drop-shadow lifted">';
foreach( $page->admin_pages as $key => $arr )
 {
        echo '<div class="admin">
			<a href="' . BASE . '/admin/' . $key . '/">
			<img src="' . BASE . '/style/images/admin/' . $key . '.png" /><br />
				' . $arr['title'] . '
			</a>
		 </div>';
 }
 echo '<div class="clearfix"></div>';
echo '</div>';
}
else
{
	    echo '<h1>
				<a href="' . BASE . '/admin/">' . $lang['admin_panel'] . '</a> » ' . $page->admin_pages[ get_get( 'subact' ) ] [ 'title' ] . '
			</h1>';
			
	echo '<div class="left-content drop-shadow lifted">';
	
    include( $page->admin_pages[ get_get( 'subact' ) ] [ 'path' ] );
	
	echo '</div>';
}
}
else
{
	redirect( BASE );
}
?>