<h1><i class="icon-plus"></i> Pievienot serveri</h1>
<?php
$page->need_login();
$page->set_page_title( 'Pievienot serveri' );

if( get_post('add_server') )
{
    if( get_post('ip') && (int)get_post('port') )
    {
		if($db->rows($db->query("SELECT id FROM servers WHERE hostname = " . $db->esc(get_post('ip')) . " AND type = " . get_post('server_type') . " AND port = " . (int) get_post('port'))))
		{
			echo error('Šāds serveris jau ir serveru sarakstā!');
		}
        else
        {
			if( get_post('server_type') == 'cs' or get_post('server_type') == 'csgo' )
			{
				$server['b']['type']    = 'source';
				$server['b']['ip']      = get_post('ip');
				$server['b']['real_ip'] = gethostbyname( get_post('ip') );
				$server['b']['port']    = get_post('port');
				$server['b']['status']  = 1;

				$status = query_direct($server, 's');
				
				if($status)
				{
					$data = array(
							'hostname'=>get_post('ip'),
							'port'=>get_post('port'),
							'creator_id'=>get_cookie('user_id'),
							'created_on'=>time(),
							'homepage'=>get_post('website'),
							'type'=>get_post('server_type')
							);
					$db->insert_array('servers', $data);

					try{
						$server = @query_live('source', get_post('ip'), get_post('port'), 'sp');
						update_server_data( mysql_insert_id(), $server);

						update_top();

						echo success('Serveris veiksmīgi pievienots!');
					}catch(Exception $e){
						echo error('Radās kļūda pievienojot serveri!');
					}
				}
				else
				{
					echo error('Serverim jābūt ieslēgtam, lai to pievienotu!');
				}
			}
			elseif( get_post('server_type') == 'mc' )
			{
				$Query = new MinecraftQuery( );
				
				try
				{
					$Query->Connect( get_post('ip'), get_post('port'), 1 );
				}
				catch( MinecraftQueryException $e )
				{
					$Error = $e->getMessage();
				}
				
				$Info = $Query->GetInfo();
				
				if( ! empty( $Info ) )
				{					
					$data = array(
								'hostname'=>get_post('ip'),
								'port'=>get_post('port'),
								'creator_id'=>get_cookie('user_id'),
								'created_on'=>time(),
								'homepage'=>get_post('website'),
								'type'=>'MC'
								);
								
					$db->insert_array('servers', $data);
										
					update_minecraft_server_data( mysql_insert_id(), $Info);
											
					update_top();
						
					echo success('Serveris veiksmīgi pievienots!');
				}
				else
				{
					echo error('Serverim jābūt ieslēgtam, lai to pievienotu!');
				}
			}
        }
    }
    else
	{
		echo error('Servera hostneims/ip un ports ir jānorāda obligāti!');
	}
}
?>
<div class="left-content drop-shadow lifted">
<form method="post">
    <table class="ipbtable" cellspacing="1" style="width: 570px;margin-bottom: -15px;">
		<tr>
			<td class="row2" align="right">Servera tips <font color="red">*</font></td>
			<td class="row3">
			<select name="server_type" style="position:relative;top:5px;padding:2px; height: 25px;border:1px solid #ccc;" />
				<option value="cs">Counter-Strike</option>
				<option value="csgo">Counter-Strike: GO</option>
				<option value="mc">Minecraft</option>
			</select>
			</td>
		</tr>
        <tr>
            <td class="row2" align="right" style="width:120px;">Hostneims / IP <font color="red">*</font></td>
            <td class="row3"><input style="width: 150px;position:relative;top:5px;" type="text" name="ip"></td>
        </tr>
        <tr>
            <td class="row2" align="right">Ports <font color="red">*</font></td>
            <td class="row3"><input type="text" style="width: 50px;position:relative;top:5px;" name="port"></td>
        </tr>
        <tr>
            <td class="row2" align="right">Mājaslapa</td>
            <td class="row3"><input type="text" style="width: 250px;position:relative;top:5px;" name="website"></td>
        </tr>
        <tr>
            <td class="row3" align="right"></td>
            <td class="row3"><input class="btn btn-success" type="submit" name="add_server" value="Pievienot"></td>
        </tr>
    </table>
</form>
</div>