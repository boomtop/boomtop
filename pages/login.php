<h1><i class="icon-user"></i> <?php echo $lang['login']; ?></h1>
<?php
$page->set_page_title( $lang['login'] );

if( !IS_USER )
{
    if( get_post( 'login' ) )
    {
	$username = $db->esc(get_post('username'));
	$password = $db->esc(md5(md5(get_post('password'))));
	$res = $db->query("SELECT id, password FROM users WHERE username = $username AND password = $password");
    if($db->rows($res))
    {
      $row = $db->fetch($res);
		setcookie('user_id', $row['id'], time()+9999999, '/');
		setcookie('pass_hash', $row['password'], time()+9999999, '/');
		header('location: ' . BASE);
    }
    else
    {
       echo error( $lang['login_wrong-username'] );
   }
}
?>
<div class="left-content drop-shadow lifted">
<form method="post">
    <table class="ipbtable" cellspacing="1" style="width: 570px;margin-bottom: -18px;">
		<tr><td class="row2" align="right" style="width:90px;"><?php echo $lang['username']; ?></td><td class="row2"><input type="text" name="username" style="width:250px;position:relative;top:5px;" /></td></tr>
        <tr><td class="row2" align="right"><?php echo $lang['signup_pass']; ?></td><td class="row2"><input type="password" name="password" onkeypress="capLock(event)" style="width:250px;position:relative;top:5px;" /></td></tr>
        <tr><td class="row3"></td><td class="row3">
                <input class="btn btn-success" type="submit" name="login" value="<?php echo $lang['login']; ?>">
                <?php echo $lang['recover-password-info']; ?>
                <a href="<?php echo HOME;?>/recover-password/"><?php echo $lang['recover-password-subject']; ?></a>
            </td></tr>
        <tr>
        </tr>
    </table>
</form>
</div>
<div id="divMayus" style="visibility:hidden">
<div class="message error">
	<?php echo $lang['login_caps-on']; ?>
</div></div>
<?php
}
else
{
   echo error( $lang['login_already'] );
}
?>