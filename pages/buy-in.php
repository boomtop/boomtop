<?php
$id = (int)get_get( 'subact' );
$res = $db->query( "SELECT * FROM pages WHERE id = " . $id );
$row = $db->fetch( $res );
if( $row['id'] )
{
?>
<h1><?php echo $lang['buy_in-for-page']; ?> <i><?php echo $row['title']; ?></i> IN+</h1>
<?php
	$page->set_page_title( '' . $lang['buy_in-for-page'] . ' ' . $row['title']. ' IN+' );
	require ROOT . '/pages/buy-in/index.php';
}
else
{
	$page->set_page_title( $lang['page_not_exist'] );
	echo '<h1>' . $lang['page_not_exist'] . '</h1>';
	echo error( $lang['page_not_exist_text'] );
}
?>