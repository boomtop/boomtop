<?php
    /**
     * @var $lang - global array
     */
?>
<div class="left-content drop-shadow lifted">
    <form method="post" autocomplete="off">
        <table class="ipbtable" cellspacing="1" style="width: 570px;margin-bottom: -15px;">
            <tbody>
                <tr>
                    <td class="row2" align="right">
                        <label for="email"><?php echo $lang['signup_email'];?>:</label>
                    </td>
                    <td class="row1">
                        <input type="text" name="email" id="email"/>
                    </td>
                </tr>
                <tr>
                    <td class="row3"></td>
                    <td class="row3">
                        <input class="btn btn-success" type="submit" name="send_email" value="<?php echo $lang['recover-password-send-mail']; ?>" />
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
