<?php

class RecoverPassword {

    /**
     * @var database
     */
    private $db;

    /**
     * @var
     */
    private $user;

    /**
     * @var array of global $lang scope
     */
    private $lang;

    /**
     * @param database $db
     */
    function __construct (database $db)
    {

        global $lang;

        $this->db = $db;
        $this->lang = $lang;

    }

    /**
     * @param $hash
     * @return bool
     */
    public function validHash ($hash)
    {

        $validHash = $this->db->result($this->db->query("
          SELECT
            COUNT(*)
          FROM
            `user_pw_recovery`
          WHERE
            `hash` = '" . mysql_real_escape_string($hash) . "'
        "));

        if ($validHash) {
            return true;
        }

        return false;
    }

    /**
     * @param $email
     * @return bool
     */
    public function postEmail ($email)
    {

        $this->getUser($email);
        if (empty($this->user)) {
            return false;
        }

        $this->sendEmail();

        return true;
    }

    /**
     * @param $data
     * @param $hash
     * @return array|bool
     */
    public function changePassword ($data, $hash)
    {

        if (empty($data['new_password']) || empty($data['new_password2'])) {
            return [
                'error' => $this->lang['password_need']
            ];
        }

        if ($data['new_password'] != $data['new_password2']) {
            return [
                'error' => $this->lang['password_mach']
            ];
        }

        $user_id = $this->db->result($this->db->query("
            SELECT
              `user_id`
            FROM
              `user_pw_recovery`
            WHERE
              `hash` = '" . mysql_real_escape_string($hash) . "'"));

        $newPassword = md5(md5(get_post('new_password')));

        $this->db->query("UPDATE `users` SET `password` = '" . $newPassword . "' WHERE `id` = " . $user_id);
        $this->db->query("DELETE FROM `user_pw_recovery` WHERE `hash` = '" . $hash . "'");

        return true;
    }

    /**
     * Sends email with recover link to user
     */
    private function sendEmail ()
    {

        $hash = $this->generateHash();

        $this->db->query("
            INSERT INTO
              `user_pw_recovery`
            SET
              `user_id` = " . $this->user['id'] . ",
              `hash` = '" . $hash . "',
              `date` = UTC_TIMESTAMP()
            ON DUPLICATE KEY UPDATE
              `hash` = '" . $hash . "',
              `date` = UTC_TIMESTAMP()
        ");

        $link = HOME . '/recover-password/hash/'. $hash;
        $message = str_replace('%link', $link, $this->lang['recover-password-msg']);

        sendMail($this->user['email'], $this->lang['recover-password-subject'], $message);

    }

    /**
     * Generate unique user recovery link hash
     * @return string
     */
    private function generateHash ()
    {

        return $this->user['id'] . substr(sha1(rand()), 0, 30);

    }

    /**
     * @param $email
     */
    private function getUser ($email)
    {

        $user = $this->db->fetch($this->db->query("
          SELECT
            `id`,
            `email`
          FROM
            `users`
          WHERE
            `email` = '" . mysql_real_escape_string($email). "'
        "));

        if ($user) {
            $this->user = $user;
        }
    }

}