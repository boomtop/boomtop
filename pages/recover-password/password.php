<?php
/**
 * @var $lang - global array
 */
?>
<div class="left-content drop-shadow lifted">
    <form method="post" autocomplete="off">
        <table class="ipbtable" cellspacing="1" style="width: 570px;margin-bottom: -15px;">
            <tbody>
                <tr>
                    <td class="row2" align="right">
                        <label for="new_password"><?php echo $lang['signup_pass'];?>:</label>
                    </td>
                    <td class="row1">
                        <input type="password" name="new_password" id="new_password"/>
                    </td>
                </tr>
                <tr>
                    <td class="row2" align="right">
                        <label for="new_password2"><?php echo $lang['signup_pass_again'];?>:</label>
                    </td>
                    <td class="row1">
                        <input type="password" name="new_password2" id="new_password2"/>
                    </td>
                </tr>
                <tr>
                    <td class="row3"></td>
                    <td class="row3">
                        <input class="btn btn-success" type="submit" name="change_password" value="<?php echo $lang['recover-password-send-mail']; ?>" />
                    </td>
                </tr>
            </tbody>
        </table>
    </form>
</div>
