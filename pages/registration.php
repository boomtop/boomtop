<h1><i class="icon-plus"></i> <?php echo $lang['signup']; ?></h1>
<?php
$page->set_page_title( $lang['signup'] );

if( !IS_USER )
{
if( get_post('registration') )
{
	$error = array();	
	if( get_post( 'username' ) )
	{
	if( strlen( get_post( 'username' ) ) >= 3 )
	{
	if( strlen( get_post( 'username' ) ) <= 30 )
	{
	$username = get_post( 'username' );
	$username_check = $db->query( "SELECT id FROM users WHERE username = $username" );
	if( $db->rows( $username_check ) == 1)
	{
		$error[] = $lang['username_exist'];
	}
	$symbols = array( ".", ",", "?", "!", "<", ">", "#" );
	if( in_array(substr( get_post( 'username' ) , -1 ), $symbols))
	{
		$error[] = $lang['username_allowed_symbol'];
	}
	}
	else
	{
		$error[] = $lang['username_learge'];
	}
	}
	else
	{
		$error[] = $lang['username_better_3'];
	}
	}
	else
	{
		$error[] = $lang['username_need'];
	}
	
	if( get_post( 'password' ) )
	{
	if( get_post( 'password' ) == get_post( 'password2' ) )
	{
	}
	else
	{
		$error[] = $lang['password_mach'];
	}
	}
	else
	{
		$error[] = $lang['password_need'];
	}
	
	if( get_post( 'email' ) )
	{
	if( vaild_email( get_post( 'email' ) ) )
	{
	$email = get_post('email');
	$email_check = $db->query( "SELECT id FROM users WHERE email = $email" );
	if( $db->rows( $email_check ) == 1)
	{
		$error[] = $lang['email_exist'];
	}
	}
	else
	{
		$error[] = $lang['email_valid'];
	}
	}
	else
	{
		$error[] = $lang['email_need'];
	}
	
	if( get_post( 'code' ) )
	{
		if( get_post( 'code' ) != 10 )
		{
			$error[] = '3+7=10';
		}
	}
	else
	{
		$error[] = '3+7=?';
	}
	
	if(count($error) >= 1)
	{
	echo "<div class='message error'>";
	foreach($error as $err)
	{
		echo "$err<br />";
	}
	echo "</div>";
	}
	else
	{
	 // Insert new user in database
		$insert_data = array(
						'username'=>get_post('username'),
						'password'=>md5(md5(get_post('password'))),
						'email'=>get_post('email'),
						'ip'=>IP,
						'date_joined'=>time()
                      );
					  
		$db->insert_array( 'users', $insert_data );
	echo success( $lang['signup_success'] );
	}
}
?>
<div class="left-content drop-shadow lifted">
<form method="post" autocomplete="off">
	<table class="ipbtable" cellspacing="1" style="width: 570px;margin-bottom: -15px;">
		<tr><td class="row2" align="right" style="width:90px;"><?php echo $lang['username']; ?> <font color="red">*</font></td><td class="row1"><input type="text" name="username" value="" style="width:250px;position:relative;top:5px;" /></td></tr>
		<tr><td class="row2" align="right"><?php echo $lang['signup_pass']; ?> <font color="red">*</font></td><td class="row1"><input type="password" name="password" style="width:250px;position:relative;top:5px;" /></td></tr>
		<tr><td class="row2" align="right"><?php echo $lang['signup_pass_again']; ?> <font color="red">*</font></td><td class="row1"><input type="password" name="password2" style="width:250px;position:relative;top:5px;" /></td></tr>
		<tr><td class="row2" align="right"><?php echo $lang['signup_email']; ?> <font color="red">*</font></td><td class="row1"><input type="text" name="email" value="" style="width:250px;position:relative;top:5px;" /></td></tr>
		<tr><td class="row2" align="right">3+7=? <font color="red">*</font></td><td class="row1"><input type="text" name="code" value="" style="width:50px;position:relative;top:5px;" /></td></tr>
		<tr><td class="row3" align="right"></td><td class="row3"><input class="btn btn-success" type="submit" name="registration" value="<?php echo $lang['signup_submit']; ?>"></td></tr>
	</table>
</form>
</div>
<?php
}
else
{
	echo error( $lang['signup_denied'] );
}
?>