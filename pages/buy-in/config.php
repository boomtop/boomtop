<?php
$config = array();

/*** Suncore.lv settings ***/
$config['client_id'] = $page->get_setting( 'suncore_client_id' ); // Your suncore.lv client id
$config['client_api'] = $page->get_setting( 'suncore_api_key' ); // Your suncore.lv api key

/*** Payment type [PayPal] ***/
$config['paypal_times'] = array(
	100 	=> 45, 
	200 	=> 95,
	400 	=> 210,
	600 	=> 350,
	800		=> 500,
	1000	=> 750,
);

/*** Payment type [SMS] ***/
$config['sms_times']['lv'] = array(
	14 	=> 15,
	21 	=> 25,
	45 	=> 50,
	71 	=> 80,
	107 => 110,
	135 => 135,
	180 => 170,
	215 => 215,
	285 => 270,
	427 => 380,
	711 => 550,
);