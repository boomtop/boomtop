<?php
$languages = array(
		'lv'=>'Latvian',
		'en'=>'English',
		'ru'=>'Russian'
		);

$language = get_get('subact');
		
if( !in_array( $language, array_keys( $languages ) ) AND $language )
{
	$page->set_page_title( $lang['choose-lang_error'] );
	echo '<h1>' . $lang['choose-lang_error'] . '</h1>';
	echo error( $lang['choose-lang_error-text'] );
}
else
{
	setcookie( "language", $language, time()+3600*24*365, "/" );
	header( "Location: " . BASE );
}
?>