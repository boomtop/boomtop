<h1>
	<i class="icon-search"></i> <?php echo $lang['search_text']; ?> "<?php echo get_get('subact'); ?>"
	<div class="pull-right">
		<input name="search-page" class="search-page" value="<?php echo get_get('subact'); ?>" placeholder="Meklēt lapu..." style="position:relative;top:1px;" type="text" /> 
		<a href="javascript:;" onclick="search_in_top('page')" style="position:relative;top:-4px;" class="btn search-button"><i class="icon-search"></i></a>
	</div>
</h1>
<?php
$page->set_page_title( '' . $lang['search_text'] . ' - ' . get_get('subact') . '' );

if( get_post('search_text') )
{
	header('location: ' . BASE . "/search/" . urlencode( get_post('search_text') ) . "");
}

$search_text = get_get('subact');

if(strlen($search_text) >= 2)
{
	$search = $db->esc( "%$search_text%" );
	$banner_id = "";
	$bannerArray = array();
	$i = 0;
	$site_q = $db->query( "SELECT id FROM pages WHERE title LIKE $search OR description LIKE $search" );
	while($site = $db->fetch( $site_q ))
	{
		$bannerArray[$site['id']] = $site['id'];
	}
	
	foreach($bannerArray as $id)
	{
	$i++;
	if($i == 1)
	{
		$banner_id .= " id='$id' ";
	}else{
		$banner_id .= " OR id='$id' ";
	}
}

$site_res = $db->query( "SELECT * FROM pages WHERE $banner_id ORDER BY id DESC" );

$count = count($bannerArray);

$search_message = $count == 0 ? error( $lang['search_not-fournd'] ):success( "" . $lang['search_found-1'] . " " . (substr($count, -1) == 1 ? $lang['search_found-2']:$lang['search_found-3']) . " <b>$count</b> " . (substr($count, -1) == 1 ? $lang['search_found-4']:$lang['search_found-5']) . " " . $lang['search_found-6'] . "!" );

echo $search_message;

if( count($bannerArray) )
{
echo '<div class="left-content drop-shadow lifted">';
while( $row = $db->fetch( $site_res ) )
{
	if(	$row['place'] == 1 ){ $place_img = '<div id="top_site_rank_1st">' . $row['place'] . '</div>'; }
	elseif( $row['place'] == 2 ){ $place_img = '<div id="top_site_rank_2st">' . $row['place'] . '</div>'; }
	elseif( $row['place'] == 3 ){ $place_img = '<div id="top_site_rank_3st">' . $row['place'] . '</div>'; }
	else{ $place_img = '<div id="top_site_rank_random">' . $row['place'] . '</div>'; }
	$site_banner = $row['banner'] == 'no_banner' ? "style/images/no_banner.jpg":"style/images/banners/" . $row['banner'];
?>
<table class="ipbtable_new" style="width: 570px;">
	<tr><td class="top_row">
<?php

    echo '<div style="float:left; width:40px; padding-top:30px;">
		    ' . $place_img . '
		</div>
        <div style="float:left; width:468px;">
            <span style="color:black;font-size:15px;">
			<a href="' . BASE . '/out/' . $row['id'] . '/" target="_blank" />' . $row['title'] . '</a>
			</span>
			<span style="float:right;"><a style="color:darkblue" href="' . BASE . '/buy-in/' . $row['id'] . '/">' . $lang['more_in'] . ' IN?</a>
			</span><br />
			<a href="' . BASE . '/out/' . $row['id'] . '/" target="_blank" /><img width="468px" height="60px" src="' . BASE . '/' . $site_banner . '" /></a>
			<br />
            <span style="font-size:12px;">' . $row['description'] . '</span>
		</div>
        <div style="float:left; width:45px; padding-top:17px;">
        <table class="toptable" cellspacing="0" style="width: 50px;">
            <tr><td class="top_in" title="IN">' . $row['in'] . '</td></tr>
            <tr><td class="top_out" title="OUT">' . $row['out'] . '</td></tr>
        </table>
		</div>';
		
?>
	</tr></td>
</table>
<?php
}
echo '</div>';
}
}
else
{
	echo '<div class="left-content drop-shadow lifted">';
	echo '<span style="padding-left: 5px;">' . $lang['search_count'] . '</span>';
	echo '</div>';
}
?>