<h1><i class="icon-envelope"></i> <?php echo $lang['contact_send']; ?></h1>
<?php
$page->set_page_title( $lang['contact_send'] );

if( get_post( 'send_message' ) )
{
  if( !get_post( 'name' ) )
  {
	echo error( $lang['contact_name-need'] );
  }
  elseif( !get_post( 'email' ) )
  {
	echo error( $lang['contact_email-need'] );
  }
  elseif( vaild_email( !get_post( 'email' ) ) )
  {
	echo error( $lang['contact_valid-email'] );
  }
  elseif( !get_post( 'subject' ) )
  {
	echo error( $lang['contact_subject-need'] );
  }
  elseif( !get_post( 'description' ) )
  {
	echo error( $lang['contact_subject-descr'] );
  }
  else
  {
  
		$insert_data = array(
						'name'=>get_post('name'),
						'email'=>get_post('email'),
						'subject'=>get_post('subject'),
						'description'=>get_post('description'),
						'sent_time'=>time()
                      );
					  
		$db->insert_array( 'messages', $insert_data );
	echo success( $lang['contact_send-success'] );
  }
}
?>
<div class="left-content drop-shadow lifted">
<form method="post">
	<table class="ipbtable" cellspacing="1" style="width: 570px;margin-bottom: -18px;">
		<tr><td class="row2" align="right" width="95px;"><?php echo $lang['contact_name']; ?> <font color="red">*</font></td><td class="row1"><input type="text" name="name" style="width:290px;position:relative;top:5px;" /></td></tr>
		<tr><td class="row2" align="right" width="95px;"><?php echo $lang['contact_email']; ?> <font color="red">*</font></td><td class="row1"><input type="text" name="email" style="width:290px;position:relative;top:5px;" /></td></tr>
		<tr><td class="row2" align="right" width="95px;"><?php echo $lang['contact_subject']; ?> <font color="red">*</font></td><td class="row1"><input type="text" name="subject" style="width:290px;position:relative;top:5px;" /></td></tr>
		<tr><td class="row2" align="right" width="95px;"><?php echo $lang['contact_text']; ?> <font color="red">*</font></td><td class="row1"><textarea name="description" style="width: 450px; min-height: 100px;position:relative;top:5px;" /></textarea></td></tr>
		<tr><td class="row3"></td><td class="row3"><input class="btn btn-success" type="submit" value="<?php echo $lang['contact_send']; ?>" name="send_message" /></td></tr>
	</table>
</form>
</div>