<h1><i class="icon-info-sign"></i> <?php echo $lang['rules']; ?></h1>
<div class="left-content drop-shadow lifted">
<?php
	$page->set_page_title( $lang['rules'] );
	
	$res = $db->query( "SELECT `rules` FROM rules" );
	$rules = $db->fetch( $res );
	
	echo bb2html($rules['rules']);
?>
</div>