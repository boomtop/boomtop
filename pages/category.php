<?php
	$id = (int)get_get( 'subact' );

	if(!$id)	
    redirect( BASE ); // Redirect if id does not exist
	
	$categ_q = $db->query( "SELECT * FROM categories WHERE id = " . $id );
	$categorijas = $db->fetch( $categ_q );
	
if( $categorijas['id'] )
{
$page->set_page_title( '' . $lang['category'] . ' - ' . $categorijas['title'] );
?>
<h1>
	<i class="icon-folder-open"></i> <?php echo $lang['category']; ?> "<?php echo $categorijas['title'] ?>"
	<div class="pull-right">
		<input name="search-page" class="search-page" value="" placeholder="Meklēt lapu..." style="position:relative;top:1px;" type="text" /> 
		<a href="javascript:;" onclick="search_in_top('page')" style="position:relative;top:-4px;" class="btn search-button"><i class="icon-search"></i></a>
	</div>
</h1>
<div class="left-content drop-shadow lifted">
<?php
$count = $db->count( 'pages', $id );
$limit = 50;
list( $pager, $limit )=pager( $limit, $count, BASE . '/category/' . $categorijas['id'] . '/' . $categorijas['seostring'] . '/page/' );
$res = $db->query( "SELECT * FROM `pages` WHERE category = '" . $id . "' ORDER BY `in` DESC " . $limit );
$i = 0;
while( $row = $db->fetch( $res ) )
{
	if(	$row['place'] == 1 ){ $place_img = '<div id="top_site_rank_1st">' . $row['place'] . '</div>'; }
	elseif( $row['place'] == 2 ){ $place_img = '<div id="top_site_rank_2st">' . $row['place'] . '</div>'; }
	elseif( $row['place'] == 3 ){ $place_img = '<div id="top_site_rank_3st">' . $row['place'] . '</div>'; }
	else{ $place_img = '<div id="top_site_rank_random">' . $row['place'] . '</div>'; }
	$site_banner = $row['banner'] == 'no_banner' ? "style/images/no_banner.jpg":"style/images/banners/" . $row['banner'];
?>
<table class="ipbtable_new" style="width: 570px;<?php if( $i == 0 ) { echo 'border-top: 1px solid #dbdbdb;'; }?>">
	<tr><td class="top_row">
<?php
    echo '<div style="float:left; width:40px; padding-top:30px;">
		    ' . $place_img . '
		</div>
        <div style="float:left; width:468px;">
            <span style="color:black;font-size:15px;">
			<a href="' . BASE . '/out/' . $row['id'] . '/" target="_blank" />' . $row['title'] . '</a>
			</span>
			<span style="float:right;"><a style="color:darkblue" href="' . BASE . '/buy-in/' . $row['id'] . '/">' . $lang['more_in'] . ' IN?</a>
			</span><br />
			<a href="' . BASE . '/out/' . $row['id'] . '/" target="_blank" /><img width="468px" height="60px" src="' . BASE . '/' . $site_banner . '" /></a>
			<br />
            <span style="font-size:12px;">' . $row['description'] . '</span>
		</div>
        <div style="float:left; width:45px; padding-top:17px;">
        <table class="toptable" cellspacing="0" style="width: 50px;">
            <tr><td class="top_in" title="IN">' . $row['in'] . '</td></tr>
            <tr><td class="top_out" title="OUT">' . $row['out'] . '</td></tr>
        </table>
		</div>';
  $i++;
?>
	</tr></td>
</table>
<?php
}

if( $i == 0 ) 
 echo '<span style="padding-left: 5px;">' . $lang['category-empty'] . '</span>';

echo '</div>';

if( $i != 0 )
echo $pager;

}
else
{
	$page->set_page_title( $lang['category-exist'] );
	echo '<h1><i class="icon-folder-open"></i> ' . $lang['category-exist'] . '</h1>';
	echo '<div class="left-content drop-shadow lifted">';
	echo '<span style="padding-left: 5px;">' . $lang['category-exist-text'] . '</span>';
	echo '</div>';
}

?>