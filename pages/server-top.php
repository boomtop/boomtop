<?php
$res = $db->query("SELECT * FROM servers");
while($row=$db->fetch($res))
{
    if($row['last_conn'] + 30 < time())
    {
		if( $row['type'] == 'CS' )
		{
			$server = @query_live('source', $row['hostname'], $row['port'], 'sp');
			update_server_data($row['id'], $server);
		}
		elseif( $row['type'] == 'MC' )
		{
			$Query = new MinecraftQuery( );
				
			try
			{
				$Query->Connect( $row['hostname'], $row['port'], 1 );
			}
			catch( MinecraftQueryException $e )
			{
				//$Error = $e->getMessage();
			}
				
			$Info = $Query->GetInfo();
			
			update_minecraft_server_data( $row['id'], $Info );
		}
    }
}

if( get_get('subact') == 'view' AND get_get('id') )
{
	include ROOT . '/pages/server-top/view.php';
}
else
{
$page->set_page_title( $lang['top-serv'] );
?>
<h1><i class="icon-th-list"></i> <?php echo $lang['top-serv']; ?></a></h1>
<div class="left-content drop-shadow lifted">
				<table class="ipbtable" style="width: 570px;">
					<tbody>
						<tr class="server-top-row" style="background: #e9f3f8;">
							<td class="status-row" style="width: 3%;text-align:center;">#</td>
							<td class="status-row" style="width: 3%;"></td>
							<td class="server-row-top" style="width: 30%;"><?php echo $lang['serv-title']; ?></td>
							<td class="server-name-row" style="width: 15%;text-align:center;"><?php echo $lang['serv-host']; ?></td>
							<td class="reservation" style="width: 10%;"><?php echo $lang['serv-players']; ?></td>
							<td class="reservation" style="width: 15%;text-align:center;"><?php echo $lang['serv-map']; ?></td>
							<td class="reservation" style="width: 10%;"><?php echo $lang['serv-rating']; ?></td>
						</tr>
						<?php
						$count = $db->count( 'servers', 'id' );
						$limit = 50;
						list( $pager, $limit ) = pager( $limit, $count, BASE . '/server-top/page/' );
						$servers = $db->query('SELECT * FROM `servers` ORDER BY `top_place` ASC ' . $limit );
						while( $server = $db->fetch( $servers ) )
						{
							if( $server['top_place'] == 1 )
							{
								$server_place = '<img title="1 vieta" src="' . BASE . '/style/images/first_place.jpg">';
							}
							elseif( $server['top_place'] == 2 )
							{
								$server_place = '<img title="2 vieta" src="' . BASE . '/style/images/second_place.jpg">';
							}
							elseif( $server['top_place'] == 3 )
							{
								$server_place = '<img title="3 vieta" src="' . BASE . '/style/images/third_place.jpg">';
							}
							else
							{
								$server_place = $server['top_place'];
							}
							
							echo '<tr bgcolor="#000">
								<td class="row3" align="center" style="color: #000;">
									' . $server_place . '
								</td>
								<td class="row3" align="center">
									<img title="Counter-Strike 1.6" src="' . BASE . '/style/images/' . $server['type'] . '.png" alt="" style="height:20px;" />
								</td>
								<td class="row3">';
								if( $server['type'] == 'MC' )
								{
									echo '<a style="margin-left: -8px;" href="' . BASE . '/server-top/view/' . $server['id'] . '/' . $server['seo_title'] . '/"><img title="" src="' . BASE . '/style/images/online_' . $server['online'] . '.png">' . ( ( empty( $server['title'] ) ) ? $server['hostname'] : $server['title'] ) . '</a>';
								}
								else
								{
									if( $server['title'] == '' )
									{
										echo '<img title="" src="' . BASE . '/style/images/online_0.png"> ' . $lang['serv-off'];
									}
									else
									{
										echo '<a style="margin-left: -8px;" href="' . BASE . '/server-top/view/' . $server['id'] . '/' . $server['seo_title'] . '/"><img title="" src="' . BASE . '/style/images/online_' . $server['online'] . '.png">' . $server['title'] . '</a>';
									}
								}
								echo '
								</td>
								<td class="row3" align="center" style="color: #000;">
									' . $server['hostname'] . '
								</td>
								<td class="row3" align="center" style="color: #000;"><span style="color:green;">' . $server['players'] . '</span>/<span style="color:red;">' . $server['max_players'] . '</span></td>
								<td class="row3" align="center" style="color: #000;">';
								if( $server['map'] == '' )
								{
									echo '---';
								}
								else
								{
									echo shorten( $server['map'], 11 );
								}
							
								
									
								echo '</td>
								<td class="row3" align="center" style="color: #000;"><img style="height:16px;" title="" src="' . BASE . '/style/images/thumbs_up.png"> <span style="position: relative; top: 3px; left: 1px;">' . $server['rating'] . '</span></td>
							</tr>
							';
						}
						?>
					</tbody>
				</table>
				<?php
				echo '</div>';
				
				echo $pager;
				?>
<?php
}
?>