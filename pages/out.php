<?php
    $id = (int)get_get( 'subact' );
	
	if(!$id)	
    redirect('/'); // Redirect if id does not exist
	
    $time = time();
	
	$ip = $db->esc(IP);

	$query = $db->query( "SELECT * FROM out_log WHERE page_id = " . $id . " AND ip = " . $ip . " AND time >= " . strtotime( 'today' ) . "" );
	
	if( !$db->rows( $query ) )
	{
	
	 // Insert data if rows = 0 witch your IP (last 24 hours)
	    $insert_data = array(
					  'page_id'=>$id,
                      'ip'=>IP,
                      'time'=>$time
                      );					
        $db->insert_array( 'out_log', $insert_data );
		
	 // Update data 
		$db->query( 'UPDATE pages SET `out` = `out` + 1  WHERE id = ' . $id );
	}
	
	$res = $db->query( 'SELECT `url` FROM pages WHERE id = '.$id.'' );
	$row = $db->fetch($res);
	
	$parsed = parse_url( $row['url'] );
	
	if( ! $parsed['scheme'] )
	{
		$redirect_url = 'http://' . $row['url'];
	}
	else
	{
		$redirect_url = $row['url'];
	}
	
	// Redirect to page if id exist
	header( 'location: ' . htmlspecialchars_decode( $redirect_url ) );
	
	// If page with id does not exist
	echo error( $lang['page_not_exist_text'] );
?>