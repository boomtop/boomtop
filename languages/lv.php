<?php 
$lang = array();

/*** NEW LANGUAGE ***/
$lang['dev'] = 'Izstrāde';
$lang['rights'] = 'Visas tiesības aizsargātas.';
$lang['follow_draugiem'] = 'Mēs esam arī draugos';
$lang['info-side'] = 'Informācija';
$lang['vote-for-page-one'] = 'Balsot par mājaslapu var reizi 24 stundās.';
$lang['answer-24h'] = 'Uz jūsu iesūtīto vēstuli, atbilde tiks sniegta 24 stundu laikā.';
$lang['page-add-rules'] = 'Mājaslapas pievienošanas noteikumi';
$lang['page-add-rules-content'] = '<font color="red">*</font> Aizliegts pievienot mājaslapas ar pornogrāfisku saturu.<br />
								   <font color="red">*</font> Aizliegts pievienot mājaslapas ar cieņu aizskarošu,rasistisku vai necenzētu tekstu un tās saturu.<br />
								   <font color="red">*</font> Pēc mājaslapas reģistrācija jāievieto balsošanas kods savā mājaslapā.';
$lang['user-logout'] = 'Iziet';
$lang['user-admin-panel'] = 'Administrācijas panelis';
$lang['have-profile-login'] = 'Tev ir profils? <b>Autorizējies</b>';
$lang['top-pages'] = 'Mājaslapu tops';
$lang['top-serv'] = 'Serveru tops';
$lang['search-pages'] = 'Meklēt lapu...';

/*** Header & Footer ***/
$lang['home'] = 'Sākums';
$lang['newest'] = '10 Jaunākās lapas';
$lang['rules'] = 'Noteikumi';
$lang['contacts'] = 'Sazinies ar mums';
$lang['rights_reserved'] = 'Visas tiesības aizsargātas.';
$lang['site_coded'] = 'Kods no';
$lang['today_in'] = 'Šodien <b>IN:</b>';
$lang['today_out'] = '<b>OUT:</b>';
$lang['all_in'] = 'Kopā <b>IN:</b>';
$lang['all_out'] = '<b>OUT:</b>';

$lang['serv-info'] = '<font color="red">*</font> Servera informācija atjaunojas reizi 30 sekundēs.';
$lang['serv-title'] = 'Nosaukums';
$lang['serv-host'] = 'Adrese';
$lang['serv-players'] = 'Spēlētāji';
$lang['serv-map'] = 'Karte';
$lang['serv-rating'] = 'Reitings';
$lang['serv-place'] = 'Vieta';
$lang['serv-type'] = 'Tips';
$lang['serv-web'] = 'Mājaslapa';
$lang['serv-uptime'] = 'Uptime';
$lang['serv-status'] = 'Statuss';
$lang['serv-status1'] = 'Ieslēgts';
$lang['serv-status2'] = 'IZslēgts';
$lang['serv-rr'] = 'Atjaunots';
$lang['serv-votes'] = 'Balsis';
$lang['serv-buyvotes'] = 'Pirkt balsis';
$lang['serv-url'] = 'Servera URL';
$lang['serv-off'] = 'Serveris ir izslēgts';
$lang['serv-vote'] = 'Balsot par serveri';
$lang['serv-vote2'] = 'Balsot';
$lang['serv-voting'] = 'Paldies, Tava balss ir saņemta!';
$lang['serv-voted'] = 'Tu jau esi balsojis!';
$lang['serv-nick'] = 'Niks';
$lang['serv-frags'] = 'Fragi';
$lang['serv-img'] = 'Bilde';

/*** Sliders ***/
$lang['categories'] = 'Kategorijas';
$lang['for_users'] = 'Lietotājiem';
$lang['hello'] = 'Sveiks';
$lang['admin_panel'] = 'Admina panelis';
$lang['add_page'] = 'Pievienot lapu';
$lang['your_added_pages'] = 'Manas pievienotās lapas';
$lang['waiting'] = 'Uzgaidiet...';
$lang['username'] = 'Lietotājvārds';
$lang['login'] = 'Ienākt';
$lang['register'] = 'Reģistrēties';
$lang['search'] = 'Meklēt';

/*** Need login ***/
$lang['need_login_title'] = 'Kļūda - Nepieciešams ielogoties!';
$lang['need_login_text'] = 'Lai apskatītu šo lapu nepieciešams <a href="' . BASE . '/registration/">reģistrēties</a> vai <a href="' . BASE . '/login/">ielogoties</a>.';

/*** Suncore payment check ***/
$lang['code_accept-1'] = 'Kods pieņemts';
$lang['code_accept-2'] = 'pieskaitīti lapai';
$lang['code_failed'] = 'Kods netika pieņemts. Tas ir nederīgs, jeb jau iztērēts.';
$lang['code_pending'] = 'SMS vēl tiek apstrādāta, lūdzu pamēģini vēlreiz pēc pāris minūtēm.';
$lang['code_aborted'] = 'Servera administratoram jāizmaina php.ini: allow_url_fopen = On; safe_mode = Off;';
$lang['code_server_answer'] = 'Serveris atbildēja ar neparedzētu paziņojumu:';

/*** Time function ***/
$lang['before'] = 'pirms';
$lang['day'] = 'dienas';
$lang['days'] = 'dienām';
$lang['week'] = 'nedēļas';
$lang['weeks'] = 'nedēļām';
$lang['hour'] = 'stundas';
$lang['hours'] = 'stundām';
$lang['minute'] = 'minūtes';
$lang['minutes'] = 'minūtēm';
$lang['sec'] = 'sekundes';
$lang['secs'] = 'sekundēm';
$lang['month'] = 'mēneša';
$lang['months'] = 'mēnešiem';
$lang['year'] = 'gada';
$lang['years'] = 'gadiem';
$lang['century'] = 'gadsimta';
$lang['centuries'] = 'gadsimtiem';

/*** Adminpanel pages ***/
$lang['pages'] = 'Lapas';
$lang['votes'] = 'Balsojumi';
$lang['messages'] = 'Vēstules';
$lang['buyers'] = 'Ienākumi';
$lang['setting'] = 'Uzstādījumi';

/*** Vote for page ***/
$lang['vote_for_page'] = 'Balsot par lapu';
$lang['already_voted'] = 'Jūs šodien jau esiet balsojis par šo lapu!';
$lang['vote_thanks'] = 'Paldies par balsojumu!';
$lang['vote_more_votes-1'] = 'Vēlies vairāk balsis';
$lang['vote_more_votes-2'] = 'lapai? Tad';
$lang['vote_more_votes-3'] = 'spied šeit';
$lang['page_not_exist'] = 'Lapa neeksistē';
$lang['page_not_exist_text'] = 'Lapa ar šādu ID neeksistē!';

/*** TOP 100 ***/
$lang['more_in'] = 'Vairāk';
$lang['sites_empty'] = 'Pašlaik datubāzē nav pievienota neviena lapa!';

/*** Search ***/
$lang['search_text'] = 'Mekeklēšanas rezultāti';
$lang['search_not-fournd'] = 'Diemžēl nekas netika atrasts.';
$lang['search_found-1'] = 'Tika';
$lang['search_found-2'] = 'atrasts';
$lang['search_found-3'] = 'atrasti';
$lang['search_found-4'] = 'saits';
$lang['search_found-5'] = 'saiti';
$lang['search_found-6'] = 'lapā';
$lang['search_count'] = 'Meklēšanas tekstam jābūt vismaz 2 simbolus garam!';

/*** Registration ***/
$lang['signup'] = 'Reģistrācija';
$lang['username_exist'] = 'Šāds lietotājvārds jau ir reģistrēts!';
$lang['username_allowed_symbol'] = 'Lietotājvārds beidzas ar neatļautu simbolu!';
$lang['username_learge'] = 'Ievadītais lietotājvārds ir par garu! 30 zīmes ir atļautais garums';
$lang['username_better_3'] = 'Lietotājvārdam jābūt garākam par 3 simboliem';
$lang['username_need'] = 'Nepieciešams ievadīt lietotājvārdu';
$lang['password_mach'] = 'Ievadītās paroles nesakrīt';
$lang['password_need'] = 'Nepieciešams ievadīt paroli';
$lang['email_exist'] = 'Šo epastu jau kāds izmanto';
$lang['email_valid'] = 'Jūs esiet ievadījis nederīgu e-pasta adresi!';
$lang['email_need'] = 'Nepieciešams ievadīt e-pastu';
$lang['signup_success'] = 'Apsveicu, Tu esi veiksmīgi reģistrējies lapā.<br />Vari <a href="' . BASE . '/login/">ielogoties</a> sistēmā!';
$lang['signup_pass'] = 'Parole';
$lang['signup_pass_again'] = 'Parole atkārtoti';
$lang['signup_email'] = 'E-Pasts';
$lang['signup_submit'] = 'Reģistrēties';
$lang['signup_rules'] = 'Reģistrācijas noteikumi';
$lang['signup_rules-1'] = 'Lietotājvārdā atļauts izmantot ciparus un latīnu burtus (<i>a-z0-9._,-?!<>#</i>)';
$lang['signup_rules-2'] = 'Lietotājvārdam jābūt garākam par <i>3</i> simboliem!';
$lang['signup_rules-3'] = 'Nepieciešams derīgs e-pasts!';
$lang['signup_rules-4'] = 'Lauciņi kuriem klāt ir (<font color="red">*</font>) ir jaizspilda obligāti!';
$lang['signup_denied'] = 'Tu jau esi ielogojies lapā, reģistrācija tev nav pieejama!';
$lang['please_wait'] = 'Lūdzu uzgaidiet...';

/*** My added sites ***/
$lang['my-sites_deleted'] = 'Lapa veiksmīgi izdzēsta!';
$lang['my-sites_delete'] = 'Esi drošs, ka vēlies dzēst šo lapu?';
$lang['my-sites_html_code'] = 'Parādīt šis lapas HTML kodu';
$lang['my-sites_empty'] = 'Pašlaik tu vēl nēsi pievienojis nevienu lapu!';

/*** Edit & delete ***/
$lang['edit'] = 'Labot';
$lang['delete'] = 'Dzēst';

/*** Login ***/
$lang['login_wrong-username'] = 'Lietotājvārds un/vai parole ievadīta nepareizi!';
$lang['login_caps-on'] = 'Tavs <i>Caps Lock</i> taustiņš ir ieslēgts. Pārliecinies, ka raksti paroli pareizi!';
$lang['login_already'] = 'Tu jau esi ielogojies lapā!';

/*** Contacts ***/
$lang['contact_send'] = 'Sūtīt ziņu';
$lang['contact_name-need'] = 'Laukums <i>Vārds</i> ir tukšs!';
$lang['contact_email-need'] = 'Laukums <i>E-pasts</i> ir tukšs!';
$lang['contact_valid-email'] = 'Jūs esiet ievadījis nederīgu e-pasta adresi!';
$lang['contact_subject-need'] = 'Laukums <i>Virsraksts</i> ir tukšs!';
$lang['contact_subject-descr'] = 'Laukums <i>Apraksts</i> ir tukšs!';
$lang['contact_send-success'] = 'Vēstule veiksmīgi izsūtīta! 24h laikā tiks sniegta atbilde uz jūsu vēstuli.';
$lang['contact_name'] = 'Vārds';
$lang['contact_email'] = 'E-pasts';
$lang['contact_subject'] = 'Virsraksts';
$lang['contact_text'] = 'Teksts';

/*** Categorie ***/
$lang['category'] = 'Kategorija';
$lang['category-empty'] = 'Šajā kategorijā nav pievienota neviena lapa!';
$lang['category-exist'] = 'Kategorija neeksistē';
$lang['category-exist-text'] = 'Kategorija ar šādu ID neeksistē!';

/*** Buy IN+ ***/
$lang['buy_in-for-page'] = 'Pirkt lapai';
$lang['buy-in_payment-type'] = 'Apmaksas veids';
$lang['buy-in_choose-in'] = 'Izvēlies IN+ lielumu';
$lang['buy-in_sms-payment'] = '<strong>Lai saņemtu Unlock kodu:</strong> <br />
								sūti tekstu <span class="suncore-sms-marker-price" id="price">SCR500</span> <br />
								uz numuru <span class="suncore-sms-marker-shortcode">159</span>';
$lang['buy-in_paypal-payment'] = 'Lai apmaksātu Unlock koda iegādi izmantojot paypal, 
									atliek tikai izvēlēties summu un spiest uz zemāk esošās pogas:';
$lang['buy-in_unlock_code'] = 'Saņemtais Unlock kods:';
$lang['buy-in_accept'] = 'Apstiprināt';
$lang['buy-in_rules'] = '<div class="suncore-rules-title">Izlasi!</div>
						<ul class="suncore-rules-list suncore-list-dot">
						<li>Nauda pēc IN+ pasūtīšanas netiek atmaksāta;</li>
						<li>Jautājumu gadījumā lūdzu sazināties ar <a href="' . BASE . '/contacts/">administrāciju</strong></li>
						</ul>';

/*** Adminpanel ***/
$lang['admin_choose'] = 'Izvēlies kādu darbību...';
$lang['admin_access-denied'] = 'Pieeja liegta!';
$lang['admin_access-denied-text'] = 'Tev nav tiesību piekļūt šai lapai!';			

/*** Add / Edit page ***/
$lang['add-page_title-need'] = 'Laukums <i>Lapas nosaukums</i> ir tukšs!';
$lang['add-page_url-need'] = 'Laukums <i>Lapas url</i> ir tukšs!';
$lang['add-page_descr-need'] = 'Laukums <i>Apraksts</i> ir tukšs!';
$lang['add-page_cate-need'] = 'Netika izvēlēta lapas kategorija!';
$lang['add-page_counter-need'] = 'Netika izvēlēta skaitītāja stils!';
$lang['add-page_ext-allowed'] = 'Bilde var būt tikai <i>jpg, jpeg, png, gif</i> formāta.';
$lang['add-page_image-size'] = 'Bildes izmērs ir par lielu, maksimālais bildes lielums ir <i>300</i> KB!';
$lang['add-page_ext-url-allowed'] = 'Atļautie banera URL formāti ir <i>jpg, jpeg, gif, png</i>';
$lang['add-page_get-content'] = 'Nav savienojuma ar bildes adresi!';
$lang['add-page_open-file'] = 'Neizdevās atvērt failu!';
$lang['add-page_upload-to-server'] = 'Augšuplādēšana uz servera neizdevās!';
$lang['add-page_succes'] = 'Lapa veiksmīgi pievienota!';
$lang['html_code_for_your_site'] = 'HTML kods priekš tavas lapas';
$lang['copy_this_html_code'] = 'Šo kodu iekopējiet savā lapas saturā, lai lietotāji varētu balsot par lapu!';
$lang['add-page_site-name'] = 'Lapas nosaukums';
$lang['add-page_site-url'] = 'Lapas url';
$lang['add-page_banner'] = 'Baneris';
$lang['add-page_not-required'] = 'Nav obligāts';
$lang['add-page_upload-from-pc'] = 'Augšuplādēt no datora';
$lang['add-page_upload-from-url'] = 'Bildes kopēšana no cita servera [URL]';
$lang['add-page_descr'] = 'Apraksts';
$lang['add-page_category'] = 'Kategorija';
$lang['choose'] = 'Izvēlies';
$lang['counter_style'] = 'Skaitītāja stils';
$lang['edit_site_page'] = 'Labot lapu';
$lang['edit-my-site-success'] = 'Lapa veiksmīgi izlabota! Atpakaļ uz <a href="' . BASE . '/my-sites/">lapu</a> sadaļu';
$lang['edit-site-success'] = 'Lapa veiksmīgi izlabota! Atpakaļ uz <a href="' . BASE . '/admin/pages/">lapu</a> sadaļu';
$lang['add-page_now-banner'] = 'Pašreizējais baneris';
$lang['edit-mysite_denied'] = 'Atvaino, bet Tu šo lapu nevari labot!';
$lang['add-in_site_page'] = 'Pievienot IN';
$lang['add-in-success'] = 'IN pievienots';

/*** 404 Error ***/
$lang['error_404'] = 'Kļūda 404';
$lang['error_404-1'] = 'Ši lapa';
$lang['error_404-2'] = 'neeksistē';

/*** Adminpanel -> votes ***/
$lang['vote_deleted'] = 'Balss veiksmīgi izdzēsta!';
$lang['votes_for_pages'] = 'Balsojumi par lapām';
$lang['page'] = 'Lapa';
$lang['voted'] = 'Nobalsojis';
$lang['options'] = 'Opcijas';
$lang['votes_deleted-page'] = 'dzēsta lapa';
$lang['vote-delete'] = 'Esi drošs, ka vēlies dzēst šo balsojumu?';
$lang['votes-empty'] = 'Pašlaik nav noticis neviens balsojums!';

/*** Adminpanel -> settings ***/
$lang['settings-updated'] = 'Lapas uzstādijumi veiksmīgi laboti!';
$lang['edit_setting'] = 'Labot uzstādijumus';
$lang['lang-lv'] = 'Latviešu';
$lang['lang-en'] = 'Angļu';
$lang['lang-ru'] = 'Krievu';

/*** Adminpanel -> rules ***/
$lang['rules_edited'] = 'Lapas noteikumi veiksmīgi izlaboti!';

/*** Adminpanel -> pages ***/
$lang['pages_all'] = 'Visas lietotāju pievienotās lapas';
$lang['pages_title'] = 'Nosaukums';
$lang['pages_descr'] = 'Apraksts';
$lang['pages_added'] = 'Pievienoja';
$lang['pages_user'] = 'lietotājs';

/*** Adminpanel -> messages ***/
$lang['message-deleted'] = 'Vēstule veiksmīgi izdzēsta!';
$lang['messages_sent'] = 'Iesūtītās vēstules';
$lang['message_unread'] = 'NEIZSLASĪTA VĒSTULE';
$lang['message_id'] = 'Vēstules<br />ID:';
$lang['message_name'] = 'Iesūtītāja vārds';
$lang['message_email'] = 'Iesūtītāja e-pasts';
$lang['message_subject'] = 'Virsraksts';
$lang['message_view'] = 'Apskatīt vēstuli';
$lang['message-delete'] = 'Esi drošs, ka vēlies dzēst šo vēstuli?';
$lang['messages-empty'] = 'Pašlaik nav iesūtīta neviena vēstule!';

/*** Adminpanel -> messages -> view ***/
$lang['message-view_id'] = 'Vēstule';
$lang['view_sent-time'] = 'Iesūtīts';
$lang['view_sender'] = 'Sūtītājs';
$lang['view_descr'] = 'Teksts';
$lang['answer_to-message'] = 'Atbildēt uz vēstuli';
$lang['message-id-exist'] = 'Vēstule ar šādu ID neeksistē!';

/*** Adminpanel -> messages -> reply ***/
$lang['message-answer'] = 'Atbilde uz vēstuli';
$lang['answer-sent-success'] = 'Atbilde vēstulei nosūtīta veiksmīgi!';
$lang['receiver'] = 'Saņēmējs';
$lang['writte'] = 'rakstīja';
$lang['message_answer'] = 'Atbilde';
$lang['sent_message'] = 'Sūtīt vēstuli';

/*** Adminpanel -> categories ***/
$lang['cat-deleted'] = 'Kategorija veiksmīgi izdzēsta!';
$lang['add_cat'] = 'Pievienot kategoriju';
$lang['cat_name'] = 'Kategorijas nosaukums';
$lang['cat_url'] = 'Kategorijas links';
$lang['cat_delete'] = 'Esi drošs, ka vēlies dzēst šo kategoriju?';
$lang['cat-empty'] = 'Pašlaik nav pievienota neviena kategorija!';

/*** Adminpanel -> categories -> create ***/
$lang['creat_cat'] = 'Izveidot kategoriju';
$lang['creat_cat-success'] = 'Kategorija veiksmīgi izveidota!';
$lang['cat-title-empty'] = 'Laukums <i>Kategorijas nosaukums</i> nedrīkst būt tukšs.';

/*** Adminpanel -> categories -> edit ***/
$lang['edit_cat'] = 'Labot kategoriju';
$lang['cat-edit-success'] = 'Kategorija veiksmīgi izlabota! Atpakaļ uz <a href="' . BASE . '/admin/categories/">kategoriju</a> sadaļu';
$lang['cat_id_exist'] = 'Kategorija ar šādu ID neeksistē!';

/*** Adminpanel -> buyers ***/
$lang['all_in_buyers'] = 'Visi IN+ pircēji (SMS / PayPal)';
$lang['all_in_buyers-unlock'] = 'Unlock kods';
$lang['all_in_buyers-price'] = 'Cena';
$lang['buyer'] = 'Pasūtijis';
$lang['buyers-empty'] = 'Pašlaik nav neviens neko pasūtijis lapā!';

/*** Choose language error ***/
$lang['choose-lang_error'] = 'Valoda neeksistē!';
$lang['choose-lang_error-text'] = 'Šāda valoda lapā neeksistē!';

/** Recover password */
$lang['recover-password-subject'] = 'Atjaunot paroli';
$lang['recover-password-msg'] = 'Lai atjaunotu paroli spied uz linka: %link';
$lang['recover-password-not_found'] = 'Ar šādu e-pastu lietotājs netika atrasts';
$lang['recover-password-email-ok'] = 'Paroles atjaunošanas links nosūtīts uz e-pastu!';
$lang['recover-password-ok'] = 'Parole veiksmīgi atjaunota!';
$lang['recover-password-send-mail'] = 'Atjaunot';
$lang['recover-password-info'] = 'Aizmirsi paroli?';