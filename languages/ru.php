<?php 
$lang = array();

/*** NEW LANGUAGE ***/
$lang['dev'] = 'Развитие';
$lang['rights'] = 'Все права защищены.';
$lang['follow_draugiem'] = 'Мы также draugiem.lv';
$lang['info-side'] = 'Информация';
$lang['vote-for-page-one'] = 'Оценить этот сайт может быть один раз в 24 часа.';
$lang['answer-24h'] = 'На вашем письме, направленном им будут даны ответы в течение 24 часов.';
$lang['page-add-rules'] = 'Условия пользования сайтом добавление';
$lang['page-add-rules-content'] = '<font color="red">*</font> Не подключайте к веб-сайтам с порнографическим содержанием.<br />
								   <font color="red">*</font> Не подключайте к веб-сайтам с унизительных, расистских или грубых текстов и его содержание.<br />';
$lang['user-logout'] = 'Выход';
$lang['user-admin-panel'] = 'Aдминистрирования панель';
$lang['have-profile-login'] = 'У вас есть профиль? <b>Войдите</b>';
$lang['top-pages'] = 'Топ вебсаитов';
$lang['top-serv'] = 'Топ сервера';
$lang['search-pages'] = 'Поиск по сайту...';

/*** Header & Footer ***/
$lang['home'] = 'Hачало';
$lang['newest'] = '10 последних страниц';
$lang['rules'] = 'Правила';
$lang['contacts'] = 'Контакт';
$lang['rights_reserved'] = 'Все права защищены.';
$lang['site_coded'] = 'Код в';
$lang['today_in'] = 'Сегодня <b>IN:</b>';
$lang['today_out'] = '<b>OUT:</b>';
$lang['all_in'] = 'Вместе <b>IN:</b>';
$lang['all_out'] = '<b>OUT:</b>';

$lang['serv-info'] = '<font color="red">*</font> Информация о сервере будет восстановлено в течение 30 секунд.';
$lang['serv-title'] = 'Hазвание';
$lang['serv-host'] = 'Aдрес';
$lang['serv-players'] = 'игроки';
$lang['serv-map'] = 'карта';
$lang['serv-rating'] = 'Рейтинг';
$lang['serv-place'] = 'Mесто';
$lang['serv-type'] = 'Тип';
$lang['serv-web'] = 'Веб-сайт';
$lang['serv-uptime'] = 'Uptime';
$lang['serv-status'] = 'Cтатус';
$lang['serv-status1'] = 'включаеть';
$lang['serv-status2'] = 'выключить';
$lang['serv-rr'] = 'Oбновилась';
$lang['serv-votes'] = 'Голосов';
$lang['serv-buyvotes'] = 'Купить голоса';
$lang['serv-url'] = 'Сервер URL';
$lang['serv-off'] = 'Сервер выключен';
$lang['serv-vote'] = 'Голосуйте за сервер';
$lang['serv-vote2'] = 'Голосуйте';
$lang['serv-voting'] = 'Спасибо, Ваш голос был принят!';
$lang['serv-voted'] = 'Вы уже проголосовали!';
$lang['serv-nick'] = 'Ник';
$lang['serv-frags'] = 'Фраги';
$lang['serv-img'] = 'Образ';

/*** Sliders ***/
$lang['categories'] = 'Категории';
$lang['for_users'] = 'Пользователи';
$lang['hello'] = 'Здравствуйте';
$lang['admin_panel'] = 'Aдминистратора';
$lang['add_page'] = 'Добавить страницу';
$lang['your_added_pages'] = 'Добавлен мою страницу';
$lang['waiting'] = 'Подождите ...';
$lang['username'] = 'Имя';
$lang['login'] = 'Bодить';
$lang['register'] = 'Pегистрации';
$lang['search'] = 'Поиск';

/*** Need login ***/
$lang['need_login_title'] = 'Ошибка - Вы должны Войти!';
$lang['need_login_text'] = 'Для просмотра этой страницы необходимо <a href="' . BASE . '/registration/">реестр</a> или <a href="' . BASE . '/login/">войти</a>.';

/*** Suncore payment check ***/
$lang['code_accept-1'] = 'Код принят';
$lang['code_accept-2'] = 'добавлены на сайт';
$lang['code_failed'] = 'Кодекс не был принят. Это является недопустимым или уже потрачены.';
$lang['code_pending'] = 'SMS все еще обрабатывается, повторите попытку через несколько минут.';
$lang['code_aborted'] = 'Администратор сервера необходимо менять в php.ini: allow_url_fopen = On; safe_mode = Off;';
$lang['code_server_answer'] = 'Ответ сервера с неожиданным заявлением:';

/*** Time function ***/
$lang['before'] = 'до';
$lang['day'] = 'день';
$lang['days'] = 'дней';
$lang['week'] = 'неделю';
$lang['weeks'] = 'недель';
$lang['hour'] = 'часов';
$lang['hours'] = 'часов';
$lang['minute'] = 'минут';
$lang['minutes'] = 'минут';
$lang['sec'] = 'секунды';
$lang['secs'] = 'секунды';
$lang['month'] = 'месяц';
$lang['months'] = 'месяцев';
$lang['year'] = 'год';
$lang['years'] = 'лет';
$lang['century'] = 'век';
$lang['centuries'] = 'веков';

/*** Adminpanel pages ***/
$lang['pages'] = 'Страницы';
$lang['votes'] = 'Голосов';
$lang['messages'] = 'Письмо';
$lang['buyers'] = 'Прибылях';
$lang['setting'] = 'Настройки';

/*** Vote for page ***/
$lang['vote_for_page'] = 'Голосуйте на страницу';
$lang['already_voted'] = 'Сегодня вы уже голосовали за этот сайт!';
$lang['vote_thanks'] = 'Спасибо за ваш голос!';
$lang['vote_more_votes-1'] = 'Хотите узнать больше голосов';
$lang['vote_more_votes-2'] = 'страницы? затем';
$lang['vote_more_votes-3'] = 'нажмите здесь';
$lang['page_not_exist'] = 'Страница не существует';
$lang['page_not_exist_text'] = 'Страница с ID не существует!';

/*** TOP 100 ***/
$lang['more_in'] = 'Вольше';
$lang['sites_empty'] = 'В настоящее время база данных не подключена к любой странице!';

/*** Search ***/
$lang['search_text'] = 'Результаты поиска';
$lang['search_not-fournd'] = 'К сожалению ничего не найдено.';
$lang['search_found-1'] = 'Был';
$lang['search_found-2'] = 'найдены';
$lang['search_found-3'] = 'найдены';
$lang['search_found-4'] = 'сайт';
$lang['search_found-5'] = 'ссылка';
$lang['search_found-6'] = 'страница';
$lang['search_count'] = 'Текст для поиска должен быть не менее 2 символов!';

/*** Registration ***/
$lang['signup'] = 'Регистрация';
$lang['username_exist'] = 'Это имя пользователя уже зарегистрирован!';
$lang['username_allowed_symbol'] = 'Имя пользователя заканчивается несанкционированного символ!';
$lang['username_learge'] = 'Имя пользователя Вы ввели слишком длинный! 30 символов допустимая длина';
$lang['username_better_3'] = 'Имя пользователя должно быть длиннее 3 символов';
$lang['username_need'] = 'Вам нужно ввести имя пользователя';
$lang['password_mach'] = 'Пароль не';
$lang['password_need'] = 'Вам нужно ввести пароль';
$lang['email_exist'] = 'Этот адрес уже используется';
$lang['email_valid'] = 'Вы ввели неверный адрес электронной почты!';
$lang['email_need'] = 'Необходимо ввести адрес электронной почты';
$lang['signup_success'] = 'Поздравляю, вы успешно зарегистрировались на странице. <br /> Вы можете <a href="' . BASE . '/login/">войти</a> система!';
$lang['signup_pass'] = 'Пароль';
$lang['signup_pass_again'] = 'Пароль повторно';
$lang['signup_email'] = 'E-Mail';
$lang['signup_submit'] = 'Pегистрации';
$lang['signup_rules'] = 'Регистрация Правила';
$lang['signup_rules-1'] = 'Имя пользователя разрешается использовать цифры и латинские буквы (Ф-z0-9._,-?!<>#</ я>)';
$lang['signup_rules-2'] = 'Имя пользователя должно быть больше, чем <i>3</i> символов!';
$lang['signup_rules-3'] = 'Адрес электронной почты!';
$lang['signup_rules-4'] = 'Поля, которые присутствуют (<font color="red">*</font>) является jaizspilda требуется!';
$lang['signup_denied'] = 'Вы уже вошли в страницу, регистрация не доступны для вас!';
$lang['please_wait'] = 'Подождите...';

/*** My added sites ***/
$lang['my-sites_deleted'] = 'Страница успешно удален!';
$lang['my-sites_delete'] = 'Будьте уверены, что хотите удалить эту страницу?';
$lang['my-sites_html_code'] = 'Показать HTML-код этой страницы';
$lang['my-sites_empty'] = 'Сейчас у вас есть месяц, добавил страницу!';

/*** Edit & delete ***/
$lang['edit'] = 'Правильное';
$lang['delete'] = 'Удалить';

/*** Login ***/
$lang['login_wrong-username'] = 'Имя пользователя и / или пароль введен неверно!';
$lang['login_caps-on'] = 'Ваш Caps <i>блокировки </i> кнопка включена. Убедитесь, что ваш пароль правильно!';
$lang['login_already'] = 'Вы уже вошли в систему страницу!';

/*** Contacts ***/
$lang['contact_send'] = 'Отправить сообщение';
$lang['contact_name-need'] = 'Площадь <i>Название</i> пуст!';
$lang['contact_email-need'] = 'Площадь <i>E-mail</i> пуст!';
$lang['contact_valid-email'] = 'Вы ввели неверный адрес электронной почты!';
$lang['contact_subject-need'] = 'Площадь <i>Титул</i> пуст!';
$lang['contact_subject-descr'] = 'Площадь <i>Описание</i> пуст!';
$lang['contact_send-success'] = 'Письмо успешно отправлено! 24 часов будет предоставлена ​​в ответ на Ваше письмо.';
$lang['contact_name'] = 'Название';
$lang['contact_email'] = 'E-mail';
$lang['contact_subject'] = 'Титул';
$lang['contact_text'] = 'Описание';

/*** Categorie ***/
$lang['category'] = 'Категория';
$lang['category-empty'] = 'Эта категория не прикреплен к любой странице!';
$lang['category-exist'] = 'Категория не существует';
$lang['category-exist-text'] = 'Категория с ID не существует!';

/*** Buy IN+ ***/
$lang['buy_in-for-page'] = 'Купить страницу';
$lang['buy-in_payment-type'] = 'Способ оплаты';
$lang['buy-in_choose-in'] = 'Выберите IN+ размер';
$lang['buy-in_sms-payment'] = '<strong>Для кода разблокировки:</strong><br />
								отправлять текстовые <span class="suncore-sms-marker-price" id="price">SCR500</span> <br />
								число <span class="suncore-sms-marker-shortcode">1897</span>';
$lang['buy-in_paypal-payment'] = 'Разблокировать код, чтобы покрыть покупку с использованием PayPal,
									просто выберите сумму и нажмите на кнопку ниже:';
$lang['buy-in_unlock_code'] = 'Поступило код разблокировки:';
$lang['buy-in_accept'] = 'Подтвердить';
$lang['buy-in_rules'] = '<div class="suncore-rules-title">прочитаны</div>
							<ul class="suncore-rules-list suncore-list-dot">
							<li>Наложенный заказа IN+ не возвращаются</ LI>
							<li>запросы, пожалуйста, свяжитесь с <a href="' . BASE . '/contacts/">администрацией</strong></li>
							</ul>';

/*** Adminpanel ***/
$lang['admin_choose'] = 'Выберите деятельности...';
$lang['admin_access-denied'] = 'Доступ запрещен!';
$lang['admin_access-denied-text'] = 'У вас нет прав для доступа к этой странице!';			

/*** Add / Edit page ***/
$lang['add-page_title-need'] = 'Площадь <i>Название сайтa</i> пуст!';
$lang['add-page_url-need'] = 'Площадь <i>Адрес страницы</i> пуст!';
$lang['add-page_descr-need'] = 'Площадь <i>Описание</i> пуст!';
$lang['add-page_cate-need'] = 'Были выбраны страницу категории!';
$lang['add-page_counter-need'] = 'Были выбраны метр стиль!';
$lang['add-page_ext-allowed'] = 'Изображение может быть только <i>jpg, jpeg, png, gif</i> формате.';
$lang['add-page_image-size'] = 'Размер фотографии слишком велик, максимальный размер изображения составляет <i>300</i> КБ!';
$lang['add-page_ext-url-allowed'] = 'Допустимые форматы включают баннер URL <i>jpg, jpeg, gif, png</i>';
$lang['add-page_get-content'] = 'Нет связи с адресом фотографии!';
$lang['add-page_open-file'] = 'Не удалось открыть файл!';
$lang['add-page_upload-to-server'] = 'Загрузка на сервер не смог!';
$lang['add-page_succes'] = 'Страница успешно добавлен!';
$lang['html_code_for_your_site'] = 'HTML-код страницы';
$lang['copy_this_html_code'] = 'Вставьте этот код в вашу страницу содержания, так что пользователи могут голосовать на странице!';
$lang['add-page_site-name'] = 'Название сайта';
$lang['add-page_site-url'] = 'Адрес страницы';
$lang['add-page_banner'] = 'Знамени';
$lang['add-page_not-required'] = 'Не требуется';
$lang['add-page_upload-from-pc'] = 'Загрузить с компьютера';
$lang['add-page_upload-from-url'] = 'Копирование фотографий с другого сервера [URL]';
$lang['add-page_descr'] = 'Описание';
$lang['add-page_category'] = 'Категория';
$lang['choose'] = 'Выберите';
$lang['counter_style'] = 'Метр стиля';
$lang['edit_site_page'] = 'Редактировать страницу';
$lang['edit-my-site-success'] = 'Страница успешно исправлен! назад к <a href="' . BASE . '/my-sites/">страница</a> pаздел';
$lang['edit-site-success'] = 'Страница успешно исправлен! назад к <a href="' . BASE . '/admin/pages/">страница</a> pаздел';
$lang['add-page_now-banner'] = 'Текущий баннер';
$lang['edit-mysite_denied'] = 'Извините, но вы не можете редактировать эту страницу!';
$lang['add-in_site_page'] = 'Add IN';
$lang['add-in-success'] = 'IN added';

/*** 404 Error ***/
$lang['error_404'] = 'Ошибка 404';
$lang['error_404-1'] = 'Эта страница';
$lang['error_404-2'] = 'не существует';

/*** Adminpanel -> votes ***/
$lang['vote_deleted'] = 'Голос успешно удален!';
$lang['votes_for_pages'] = 'Голосов на листьях';
$lang['page'] = 'Страница';
$lang['voted'] = 'Голос';
$lang['options'] = 'Oпции';
$lang['votes_deleted-page'] = 'удалил страницу';
$lang['vote-delete'] = 'Будьте уверены, что хотите удалить этот голос?';
$lang['votes-empty'] = 'В настоящее время, не было никакого голоса нет!';

/*** Adminpanel -> settings ***/
$lang['settings-updated'] = 'Сайт настройки успешно отремонтировали!';
$lang['edit_setting'] = 'Изменить параметры';
$lang['lang-lv'] = 'Латышский';
$lang['lang-en'] = 'Aнглийский';
$lang['lang-ru'] = 'Pусский';

/*** Adminpanel -> rules ***/
$lang['rules_edited'] = 'Правила сайта успешно исправлен!';

/*** Adminpanel -> pages ***/
$lang['pages_all'] = 'Все пользователи добавили страницу';
$lang['pages_title'] = 'Имя';
$lang['pages_descr'] = 'Oписание';
$lang['pages_added'] = 'Добавил';
$lang['pages_user'] = 'пользователь';

/*** Adminpanel -> messages ***/
$lang['message-deleted'] = 'Письмо успешно удален!';
$lang['messages_sent'] = 'Отправленные по почте';
$lang['message_unread'] = 'Hовое письмо';
$lang['message_id'] = 'Письмо<br />ID:';
$lang['message_name'] = 'Плакат имя';
$lang['message_email'] = 'Отправителя почте.';
$lang['message_subject'] = 'Титул';
$lang['message_view'] = 'Посмотреть письмо';
$lang['message-delete'] = 'Будьте уверены, что хотите удалить это сообщение?';
$lang['messages-empty'] = 'В настоящее время нет поста, ни одна буква!';

/*** Adminpanel -> messages -> view ***/
$lang['message-view_id'] = 'Письмо';
$lang['view_sent-time'] = 'Добавлено';
$lang['view_sender'] = 'Отправителя';
$lang['view_descr'] = 'Текст';
$lang['answer_to-message'] = 'Ответ на письмо';
$lang['message-id-exist'] = 'Письмо ID не существует!';

/*** Adminpanel -> messages -> reply ***/
$lang['message-answer'] = 'Ответ на письмо';
$lang['answer-sent-success'] = 'Ответ на письмо успешно отправлено!';
$lang['receiver'] = 'Получателя';
$lang['writte'] = 'написал';
$lang['message_answer'] = 'Ответить';
$lang['sent_message'] = 'Отправить письмо';

/*** Adminpanel -> categories ***/
$lang['cat-deleted'] = 'Категория успешно удален!';
$lang['add_cat'] = 'Добавить категорию';
$lang['cat_name'] = 'Название категории';
$lang['cat_url'] = 'Категории ссылку';
$lang['cat_delete'] = 'Будьте уверены, что хотите удалить эту категорию?';
$lang['cat-empty'] = 'В настоящее время не подключены к какой-либо категории!';

/*** Adminpanel -> categories -> create ***/
$lang['creat_cat'] = 'Создание категории';
$lang['creat_cat-success'] = 'Категория успешно создана!';
$lang['cat-title-empty'] = 'Площадь <i>Название категории</i> не может быть пустым.';

/*** Adminpanel -> categories -> edit ***/
$lang['edit_cat'] = 'Изменить категорию';
$lang['cat-edit-success'] = 'Категория успешно обновлен! назад к <a href="' . BASE . '/admin/categories/">категория</a> название';
$lang['cat_id_exist'] = 'Категория с ID не существует!';

/*** Adminpanel -> buyers ***/
$lang['all_in_buyers'] = 'Все IN+ покупателей (SMS / PayPal)';
$lang['all_in_buyers-unlock'] = 'Код';
$lang['all_in_buyers-price'] = 'Цена';
$lang['buyer'] = 'По заказу';
$lang['buyers-empty'] = 'Есть в настоящее время нет никого заказов!';

/*** Choose language error ***/
$lang['choose-lang_error'] = 'Язык не существует!';
$lang['choose-lang_error-text'] = 'Такая страница языка не существует!';

/** Recover password */
$lang['recover-password-subject'] = 'Восстановить пароль';
$lang['recover-password-msg'] = 'Для восстановления пароля нажмите на ссылку: %link';
$lang['recover-password-not_found'] = 'Не удалось найти пользователя с таким э-почте';
$lang['recover-password-email-ok'] = 'Ссылка для восстановление пароля отправлена на э-почте!';
$lang['recover-password-ok'] = 'Пароль успешно восстановлен!';
$lang['recover-password-send-mail'] = 'Восстановить';
$lang['recover-password-info'] = 'Забыли пароль?';