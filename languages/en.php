<?php 
$lang = array();

/*** NEW LANGUAGE ***/
$lang['dev'] = 'Development';
$lang['rights'] = 'All rights reserved.';
$lang['follow_draugiem'] = 'We are in draugiem.lv';
$lang['info-side'] = 'Information';
$lang['vote-for-page-one'] = 'Vote this website may be once every 24 hours.';
$lang['answer-24h'] = 'On your letter will be answered within 24 hours.';
$lang['page-add-rules'] = 'Website Terms adding';
$lang['page-add-rules-content'] = '<font color="red">*</font> Do not add websites with pornographic content.<br />
								   <font color="red">*</font> Do not add websites with degrading, racist or rude texts.<br />';
$lang['user-logout'] = 'Logout';
$lang['user-admin-panel'] = 'Administration panel';
$lang['have-profile-login'] = 'Do you have a profile? <b>Sign in</b>';
$lang['top-pages'] = 'Homepage top';
$lang['top-serv'] = 'Server top';
$lang['search-pages'] = 'Search page...';

/*** Header & Footer ***/
$lang['home'] = 'Home';
$lang['newest'] = '10 newest pages';
$lang['rules'] = 'Rules';
$lang['contacts'] = 'Contacts';
$lang['rights_reserved'] = 'All rights reserved.';
$lang['site_coded'] = 'Coded by';
$lang['today_in'] = 'Today <b>IN:</b>';
$lang['today_out'] = '<b>OUT:</b>';
$lang['all_in'] = 'All <b>IN:</b>';
$lang['all_out'] = '<b>OUT:</b>';

$lang['serv-info'] = '<font color="red">*</font> Server information is restored in every 30 seconds.';
$lang['serv-title'] = 'Title';
$lang['serv-host'] = 'Adress';
$lang['serv-players'] = 'Players';
$lang['serv-map'] = 'Map';
$lang['serv-rating'] = 'Rating';
$lang['serv-place'] = 'Place';
$lang['serv-type'] = 'Type';
$lang['serv-web'] = 'Website';
$lang['serv-uptime'] = 'Uptime';
$lang['serv-status'] = 'Status';
$lang['serv-status1'] = 'Online';
$lang['serv-status2'] = 'Offline';
$lang['serv-rr'] = 'Refreshed';
$lang['serv-votes'] = 'Votes';
$lang['serv-buyvotes'] = 'Buy votes';
$lang['serv-url'] = 'Server URL';
$lang['serv-off'] = 'Server is offline';
$lang['serv-vote'] = 'Vote for server';
$lang['serv-vote2'] = 'Vote';
$lang['serv-voting'] = 'Thank you, Your vote has been received!';
$lang['serv-voted'] = 'You have already voted!';
$lang['serv-nick'] = 'Nick';
$lang['serv-frags'] = 'Frags';
$lang['serv-img'] = 'Image';

/*** Sliders ***/
$lang['categories'] = 'Categories';
$lang['for_users'] = 'User panel';
$lang['hello'] = 'Hello';
$lang['admin_panel'] = 'Admin panel';
$lang['add_page'] = 'Add page';
$lang['your_added_pages'] = 'My added pages';
$lang['waiting'] = 'Wait...';
$lang['username'] = 'Username';
$lang['login'] = 'Login';
$lang['register'] = 'Register';
$lang['search'] = 'Search';

/*** Need login ***/
$lang['need_login_title'] = 'Error - Need login!';
$lang['need_login_text'] = 'To view this page need to <a href="' . BASE . '/registration/">register</a> or <a href="' . BASE . '/login/">login</a>.';

/*** Suncore payment check ***/
$lang['code_accept-1'] = 'Code accepted';
$lang['code_accept-2'] = 'added to site';
$lang['code_failed'] = 'Code was not accepted. It is invalid or already spent.';
$lang['code_pending'] = 'SMS is still being processed, please try again in a few minutes.';
$lang['code_aborted'] = 'Server administrator to be changed in php.ini: allow_url_fopen = On; safe_mode = Off;';
$lang['code_server_answer'] = 'The server responded with an unexpected announcement:';

/*** Time function ***/
$lang['before'] = 'before';
$lang['day'] = 'day';
$lang['days'] = 'days';
$lang['week'] = 'week';
$lang['weeks'] = 'weeks';
$lang['hour'] = 'hour';
$lang['hours'] = 'hours';
$lang['minute'] = 'minute';
$lang['minutes'] = 'minutes';
$lang['sec'] = 'second';
$lang['secs'] = 'seconds';
$lang['month'] = 'month';
$lang['months'] = 'months';
$lang['year'] = 'year';
$lang['years'] = 'years';
$lang['century'] = 'century';
$lang['centuries'] = 'centuries';

/*** Adminpanel pages ***/
$lang['pages'] = 'Pages';
$lang['votes'] = 'Votes';
$lang['messages'] = 'Messages';
$lang['buyers'] = 'Income';
$lang['setting'] = 'Settings';

/*** Vote for page ***/
$lang['vote_for_page'] = 'Vote the page';
$lang['already_voted'] = 'Today you`re already voted for this page!';
$lang['vote_thanks'] = 'Thanks for vote!';
$lang['vote_more_votes-1'] = 'Need more votes';
$lang['vote_more_votes-2'] = 'page? Then';
$lang['vote_more_votes-3'] = 'click here';
$lang['page_not_exist'] = 'Page does not exist';
$lang['page_not_exist_text'] = 'Page with the ID does not exist!';

/*** TOP 100 ***/
$lang['more_in'] = 'More';
$lang['sites_empty'] = 'Currently, the database is empty!';

/*** Search ***/
$lang['search_text'] = 'Search results';
$lang['search_not-fournd'] = 'Nothing was found.';
$lang['search_found-1'] = 'Was';
$lang['search_found-2'] = 'found';
$lang['search_found-3'] = 'found';
$lang['search_found-4'] = 'site';
$lang['search_found-5'] = 'sites';
$lang['search_found-6'] = 'in page';
$lang['search_count'] = 'Search text must be at least 2 characters long!';

/*** Registration ***/
$lang['signup'] = 'Registration';
$lang['username_exist'] = 'This username is already registered!';
$lang['username_allowed_symbol'] = 'Username ends with unauthorized symbol!';
$lang['username_learge'] = 'Username you have entered is too long! 30 characters are permitted length';
$lang['username_better_3'] = 'Username must be longer than 3 characters';
$lang['username_need'] = 'Need to enter the username';
$lang['password_mach'] = 'The entered password`s dont`t match';
$lang['password_need'] = 'Need to enter password';
$lang['email_exist'] = 'This email is already use';
$lang['email_valid'] = 'You have entered an invalid e-mail address!';
$lang['email_need'] = 'Need to enter the e-mail';
$lang['signup_success'] = 'Congratulations, you`ve successfully registered on the page. <br /> You can <a href="' . BASE . '/login/">login</a> in page!';
$lang['signup_pass'] = 'Password';
$lang['signup_pass_again'] = 'Password again';
$lang['signup_email'] = 'E-mail';
$lang['signup_submit'] = 'Register';
$lang['signup_rules'] = 'Registration rules';
$lang['signup_rules-1'] = 'Username allowed to use numbers latin letters (<i> a-z0-9._,-?!<>#</ i>)';
$lang['signup_rules-2'] = 'Username must be longer than <i> 3 </ i> characters!';
$lang['signup_rules-3'] = 'Need a valid e-mail!';
$lang['signup_rules-4'] = 'Fields which are present (<font color="red">*</ font>) is required!';
$lang['signup_denied'] = 'You have already logged in page, registration is not available for you!';
$lang['please_wait'] = 'Please wait...';

/*** My added sites ***/
$lang['my-sites_deleted'] = 'Page successfully deleted!';
$lang['my-sites_delete'] = 'Be sure that you want to delete this page?';
$lang['my-sites_html_code'] = 'Show this page HTML code';
$lang['my-sites_empty'] = 'Currently you dont`t have added any sites!';

/*** Edit & delete ***/
$lang['edit'] = 'Edit';
$lang['delete'] = 'Delete';

/*** Login ***/
$lang['login_wrong-username'] = 'Username and / or password is entered incorrectly!';
$lang['login_caps-on'] = 'Your <i>Caps Lock</i> button is turned on. Make sure that your password correctly!';
$lang['login_already'] = 'You have already logged in page!';

/*** Contacts ***/
$lang['contact_send'] = 'Send a message';
$lang['contact_name-need'] = 'Field <i>Name</i> is empty!';
$lang['contact_email-need'] = 'Field <i>E-mail</i> is empty!';
$lang['contact_valid-email'] = 'You have entered an invalid e-mail address!';
$lang['contact_subject-need'] = 'Field <i>Subject</i> is empty!';
$lang['contact_subject-descr'] = 'Field <i>Description</i> is empty!';
$lang['contact_send-success'] = 'Letter sent successfully! 24 hours will be provided in response to your letter.';
$lang['contact_name'] = 'Name';
$lang['contact_email'] = 'E-mail';
$lang['contact_subject'] = 'Subject';
$lang['contact_text'] = 'Description';

/*** Categorie ***/
$lang['category'] = 'Category';
$lang['category-empty'] = 'This category is empty!';
$lang['category-exist'] = 'Category does not exist';
$lang['category-exist-text'] = 'Category with the ID does not exist!';

/*** Buy IN+ ***/
$lang['buy_in-for-page'] = 'Buy for page';
$lang['buy-in_payment-type'] = 'Payment type';
$lang['buy-in_choose-in'] = 'Choose IN+ size';
$lang['buy-in_sms-payment'] = '<strong>For Unlock code:</strong><br />
								send text <span class="suncore-sms-marker-price" id="price">SCR500</span><br />
								to number <span class="suncore-sms-marker-shortcode">1897</span>';
$lang['buy-in_paypal-payment'] = 'To purchase Unlock Code, choose your price and press the button below';
$lang['buy-in_unlock_code'] = 'Received Unlock Code:';
$lang['buy-in_accept'] = 'Approve';
$lang['buy-in_rules'] = '<div class="suncore-rules-title">Read!</div>
						<ul class="suncore-rules-list suncore-list-dot">
						<li>We do not accept refunds;</li>
						<li>If you are any questions please contact with <a href="' . BASE . '/contacts/">administration</strong></li>
						</ul>';

/*** Adminpanel ***/
$lang['admin_choose'] = 'Choose an activity...';
$lang['admin_access-denied'] = 'Access Denied!';
$lang['admin_access-denied-text'] = 'You do not have access to this page!';			

/*** Add / Edit page ***/
$lang['add-page_title-need'] = 'Field <i>Page name</i> is empty!';
$lang['add-page_url-need'] = 'Field <i>Page url</i> is empty!';
$lang['add-page_descr-need'] = 'Field <i>Description</i> is empty!';
$lang['add-page_cate-need'] = 'Please choose page category!';
$lang['add-page_counter-need'] = 'Please choose page counter style!';
$lang['add-page_ext-allowed'] = 'Image may only be <i>jpg, jpeg, png, gif</i> format.';
$lang['add-page_image-size'] = 'Photo size is too large, the maximum image size is <i>300</i> KB!';
$lang['add-page_ext-url-allowed'] = 'Acceptable banner URL formats is <i>jpg, jpeg, gif, png</i>';
$lang['add-page_get-content'] = 'No connection with the pictures address!';
$lang['add-page_open-file'] = 'Failed to open file!';
$lang['add-page_upload-to-server'] = 'Upload to the server failed!';
$lang['add-page_succes'] = 'Page added successfully!';
$lang['html_code_for_your_site'] = 'HTML code for your page';
$lang['copy_this_html_code'] = 'Paste this code in your page content!';
$lang['add-page_site-name'] = 'Page name';
$lang['add-page_site-url'] = 'Page url';
$lang['add-page_banner'] = 'Banner';
$lang['add-page_not-required'] = 'Not required';
$lang['add-page_upload-from-pc'] = 'Upload from PC';
$lang['add-page_upload-from-url'] = 'Copy picture from another server [URL]';
$lang['add-page_descr'] = 'Description';
$lang['add-page_category'] = 'Category';
$lang['choose'] = 'Choose';
$lang['counter_style'] = 'Counter style';
$lang['edit_site_page'] = 'Edit page';
$lang['edit-my-site-success'] = 'Page successfully edited! Back to <a href="' . BASE . '/my-sites/">page</a> section';
$lang['edit-site-success'] = 'Page successfully edited! Back to <a href="' . BASE . '/admin/pages/">page</a> section';
$lang['add-page_now-banner'] = 'Current banner';
$lang['edit-mysite_denied'] = 'Sorry, but you can not edit this page!';
$lang['add-in_site_page'] = 'Add IN';
$lang['add-in-success'] = 'IN added';

/*** 404 Error ***/
$lang['error_404'] = 'Error 404';
$lang['error_404-1'] = 'This page';
$lang['error_404-2'] = 'does not exist';

/*** Adminpanel -> votes ***/
$lang['vote_deleted'] = 'Vote successfully deleted!';
$lang['votes_for_pages'] = 'Votes for pages';
$lang['page'] = 'Page';
$lang['voted'] = 'Voted';
$lang['options'] = 'Options';
$lang['votes_deleted-page'] = 'deleted page';
$lang['vote-delete'] = 'Be sure that you want to delete this vote?';
$lang['votes-empty'] = 'Currently, votes database is empty!';

/*** Adminpanel -> settings ***/
$lang['settings-updated'] = 'Site settings successfully updated!';
$lang['edit_setting'] = 'Edit settings';
$lang['lang-lv'] = 'Latvian';
$lang['lang-en'] = 'English';
$lang['lang-ru'] = 'Russian';

/*** Adminpanel -> rules ***/
$lang['rules_edited'] = 'Site rules successfully updated!';

/*** Adminpanel -> pages ***/
$lang['pages_all'] = 'All users added pages';
$lang['pages_title'] = 'Name';
$lang['pages_descr'] = 'Description';
$lang['pages_added'] = 'Added';
$lang['pages_user'] = 'user';

/*** Adminpanel -> messages ***/
$lang['message-deleted'] = 'Message successfully deleted!';
$lang['messages_sent'] = 'Inbox messages';
$lang['message_unread'] = 'UNREAD MESSAGE';
$lang['message_id'] = 'Message<br />ID:';
$lang['message_name'] = 'Sender name';
$lang['message_email'] = 'Sender e-mail';
$lang['message_subject'] = 'Subject';
$lang['message_view'] = 'View the message';
$lang['message-delete'] = 'Be sure that you want to delete this message?';
$lang['messages-empty'] = 'Currently messages database is empty!';

/*** Adminpanel -> messages -> view ***/
$lang['message-view_id'] = 'Message';
$lang['view_sent-time'] = 'Sended';
$lang['view_sender'] = 'Sender';
$lang['view_descr'] = 'Text';
$lang['answer_to-message'] = 'Answer to message';
$lang['message-id-exist'] = 'Message to the ID does not exist!';

/*** Adminpanel -> messages -> reply ***/
$lang['message-answer'] = 'The answer to the message';
$lang['answer-sent-success'] = 'The answer to the message sent successfully!';
$lang['receiver'] = 'Receiver';
$lang['writte'] = 'writte';
$lang['message_answer'] = 'Answer';
$lang['sent_message'] = 'Send message';

/*** Adminpanel -> categories ***/
$lang['cat-deleted'] = 'Category successfully deleted!';
$lang['add_cat'] = 'Add a category';
$lang['cat_name'] = 'Category name';
$lang['cat_url'] = 'Category link';
$lang['cat_delete'] = 'Be sure that you want to delete this category?';
$lang['cat-empty'] = 'Currently category database is empty!';

/*** Adminpanel -> categories -> create ***/
$lang['creat_cat'] = 'Create a category';
$lang['creat_cat-success'] = 'Category successfully created!';
$lang['cat-title-empty'] = 'Field <i>Category name</i> is empty.';

/*** Adminpanel -> categories -> edit ***/
$lang['edit_cat'] = 'Edit category';
$lang['cat-edit-success'] = 'Category successfully updated! Back to <a href="' . BASE . '/admin/categories/">category</a> section';
$lang['cat_id_exist'] = 'Category with the ID does not exist!';

/*** Adminpanel -> buyers ***/
$lang['all_in_buyers'] = 'All IN+ buyers (SMS / PayPal)';
$lang['all_in_buyers-unlock'] = 'Unlock code';
$lang['all_in_buyers-price'] = 'Price';
$lang['buyer'] = 'Sended';
$lang['buyers-empty'] = 'Currently buyers database is empty!';

/*** Choose language error ***/
$lang['choose-lang_error'] = 'Language does not exist!';
$lang['choose-lang_error-text'] = 'Such language page does not exist!';

/** Recover password */
$lang['recover-password-subject'] = 'Recover your password';
$lang['recover-password-msg'] = 'To recover your password press on link: %link';
$lang['recover-password-not_found'] = 'Could not find user with that email';
$lang['recover-password-email-ok'] = 'Password recovery link sent to given email!';
$lang['recover-password-ok'] = 'Password successfully recovered!';
$lang['recover-password-send-mail'] = 'Recover';
$lang['recover-password-info'] = 'Forgot password?';