<?php
define( 'TOP_SITE', 1 );
defined( 'TOP_SITE' ) or die( 'Access restricted' );

ini_set( 'display_errors', true );
ini_set( 'display_startup_errors', true );
error_reporting( E_ALL ^ E_NOTICE ^ E_DEPRECATED ^ E_COMPILE_WARNING );

/*** Force https ***/
if( $_SERVER["HTTPS"] != 'on' )
{
    header( 'Location: https://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] );
}

require 'configuration.php';

/*** Main includes ***/
include ROOT . '/includes/class/page.php';
include ROOT . '/includes/class/mysql.php';
include ROOT . '/includes/class/users.php';
include ROOT . '/includes/class/application.php';
include ROOT . '/includes/class/main_suncore.php';
include ROOT . '/includes/functions.php';
include ROOT . '/includes/minecraftquery.php';
include ROOT . '/includes/server_functions.php';

/*** Start session ***/
session_start();

/*** Create class ***/
$db = new database( DB_HOST, DB_USER, DB_PASS, DB_BASE );
$users = new users();
$page = new page();

/*** Define other constants ***/
define( 'WEBSITE_NAME', $page->get_setting( 'site_title' ) );
define( 'SITE_THEME', 'TopSite' );

/*** Load website language ***/
$lang = load_language();

/*** Load cookie language file ***/
if(get_cookie('language'))
{
	include ROOT . '/languages/' . get_cookie('language') . '.php';
}

/*** Page work from any folder ***/
$segments = explode( '/', BASE );
$segments_real = count( $segments );
$segments_del  = $segments_real - 2;
$segments = explode( '/', $_SERVER['REQUEST_URI'] );

for( $i=1;$i<=$segments_del;$i++ )
{
	array_shift( $segments );
}

$_SERVER['REQUEST_URI'] = '/' . implode( '/', $segments );
$segments = split_to_segments( $_SERVER['REQUEST_URI'] );
segments_to_get( $segments );

/*** Load site theme ***/
$page->LOAD_THEME( ROOT . "/themes/" . SITE_THEME . '/index.php' );

/*** Load site pages ***/
$page->load_page( 'admin', array( 'path' => ROOT . '/pages/' . 'admin.php' ) );
$page->load_page( 'logout' , array( 'path' => ROOT . '/pages/' . 'logout.php' ) );
$page->load_page( 'out' , array( 'path' => ROOT . '/pages/' . 'out.php' ) );
$page->load_page( 'vote' , array( 'path' => ROOT . '/pages/' . 'vote.php' ) );
$page->load_page( 'contacts' , array( 'path' => ROOT . '/pages/' . 'contacts.php' ) );
$page->load_page( 'login' , array( 'path' => ROOT . '/pages/' . 'login.php' ) );
$page->load_page( 'search' , array( 'path' => ROOT . '/pages/' . 'search.php' ) );
$page->load_page( 'top-100' , array( 'path' => ROOT . '/pages/' . 'top-100.php' ) );
$page->load_page( 'lang' , array( 'path' => ROOT . '/pages/' . 'lang.php' ) );
$page->load_page( 'category' , array( 'path' => ROOT . '/pages/' . 'category.php' ) );
$page->load_page( 'buy-in' , array( 'path' => ROOT . '/pages/' . 'buy-in.php' ) );
$page->load_page( 'buy-votes' , array( 'path' => ROOT . '/pages/' . 'buy-votes.php' ) );
$page->load_page( 'my-sites' , array( 'path' => ROOT . '/pages/' . 'my-sites.php' ) );
$page->load_page( 'rules' , array( 'path' => ROOT . '/pages/' . 'rules.php' ) );
$page->load_page( 'add-page' , array( 'path' => ROOT . '/pages/' . 'add-page.php' ) );
$page->load_page( 'add-server' , array( 'path' => ROOT . '/pages/' . 'add-server.php' ) );
$page->load_page( 'server-top' , array( 'path' => ROOT . '/pages/' . 'server-top.php' ) );
$page->load_page( 'registration' , array( 'path' => ROOT . '/pages/' . 'registration.php' ) );
$page->load_page( 'recover-password' , array( 'path' => ROOT . '/pages/' . 'recover-password.php' ) );

/*** Load adminpanel pages ***/
$page->load_admin_page( 'categories', $lang['categories'] );
$page->load_admin_page( 'pages', $lang['pages'] );
$page->load_admin_page( 'servers', 'Serveri' );
$page->load_admin_page( 'votes', $lang['votes'] );
$page->load_admin_page( 'messages', '' . $lang['messages'] . ' (' . check_admin_messages() . ')' );
$page->load_admin_page( 'rules', $lang['rules'] );
$page->load_admin_page( 'buyers', $lang['buyers'] );
$page->load_admin_page( 'settings', $lang['setting'] );

/*** Load javascripts ***/
$page->add_javascript( BASE . '/style/js/jquery-1.7.1.min.js' );
$page->add_javascript( BASE . '/style/js/bootstrap.min.js' );
$page->add_javascript( BASE . '/style/js/topsite.js?c=' . filemtime( ROOT . '/style/js/topsite.js' ) );
$page->add_javascript( BASE . '/style/js/suncore-functions.js' );
$page->add_javascript( BASE . '/style/js/suncore-in-buy.js' );

/*** Update pages places ***/
update_pages_place();

/*** Load page content ***/
ob_start();

if( get_get( 'act' ) && array_key_exists( get_get( 'act' ), $page->site_pages ) )
{
	require $page->site_pages[ get_get( 'act' ) ][ 'path' ];
}
elseif( get_get( 'act' ) && !array_key_exists( get_get( 'act' ), $page->site_pages))
{
	redirect( BASE );
}
else
{
	require $page->site_pages[ 'top-100' ][ 'path' ];
}

$page->set_page_content( ob_get_contents() );
ob_end_clean();
