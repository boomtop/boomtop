<?php
defined( 'TOP_SITE' ) or die( 'Access restricted' );
$this->add_stylesheet( $this->SITE_THEME_URL . '/bootstrap.min.css?c=' . filemtime( ROOT . '/themes/' . SITE_THEME . '/bootstrap.min.css' ) );
$this->add_stylesheet( $this->SITE_THEME_URL . '/main.css?c=' . filemtime( ROOT . '/themes/' . SITE_THEME . '/main.css' ) );
//$this->add_stylesheet( $this->SITE_THEME_URL . '/shadows.css?c=' . filemtime( ROOT . '/themes/' . SITE_THEME . '/shadows.css' ) );
$this->add_stylesheet( $this->SITE_THEME_URL . '/suncore/suncore-lightgrey.css?c=' . filemtime( ROOT . '/themes/' . SITE_THEME . '/suncore/suncore-lightgrey.css' ) );
$this->add_stylesheet( $this->SITE_THEME_URL . '/suncore/suncore-in-buy.css?c=' . filemtime( ROOT . '/themes/' . SITE_THEME . '/suncore/suncore-in-buy.css' ) );
global $db, $users, $lang;

$languages_list = array(
	'lv'=>'Latviski',
	'en'=>'English',
	'ru'=>'Pусский'
);
?>
<!DOCTYPE HTML>
<html lang="en-US">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title><?php echo WEBSITE_NAME . ' :: ' . $this->page_title; ?></title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<?php
		echo $this->load_stylesheet( true );
		echo $this->load_javascript( true );
		?>
		<script type="text/javascript">
		var base_url = '<?php echo BASE; ?>';
		</script>
		<!--[if IE]>
		<link href="/style/ie.css" rel="stylesheet" type="text/css" />
		<![endif]-->
	</head>
	<body>
		<div class="navbar navbar-inverse drop-shadow lifted">
			<div class="navbar-inner">
				<div id="wrapper">
					<a class="brand" href="<?php echo BASE; ?>">
						<img src="<?php echo $this->SITE_THEME_URL; ?>/images/logo.png" style="position:absolute;width:160px;margin-left:-12px;margin-top:-13px;" alt="" />
					</a>
					<div class="nav-collapse nav-inverse-collapse" style="margin-left:150px;">
						<ul class="nav">
							<li <?php echo ( $_SERVER['REQUEST_URI'] == '/' OR get_get( 'act' ) == 'category' ) ? 'class="active"' : null; ?>><a href="<?php echo BASE; ?>"><i class="icon-tasks icon-white"></i> <?php echo $lang['top-pages']; ?></a></li>
							<li <?php echo ( get_get( 'act' ) == 'server-top' ) ? 'class="active"' : null; ?>><a href="<?php echo BASE; ?>/server-top/"><i class="icon-th-list icon-white"></i> <?php echo $lang['top-serv']; ?></a></li>
						</ul>
						<ul class="nav pull-right">
							<li class="divider-vertical"></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><img src="<?php echo BASE; ?>/style/images/<?php echo ( get_cookie('language') == null ) ? 'lv' : get_cookie('language'); ?>.gif" alt="" /> <?php echo ( get_cookie('language') == null ) ? 'Latviski' : $languages_list[ get_cookie('language') ]; ?> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<?php foreach( $languages_list as $lang_code => $lang_title ): ?>
									<li><a href="<?php echo BASE; ?>/lang/<?php echo $lang_code; ?>/"><img src="<?php echo BASE; ?>/style/images/<?php echo $lang_code; ?>.gif" alt="" /> <span style="position: relative;top: 1px;"><?php echo $lang_title; ?></span></a></li>
									<?php endforeach; ?>
								</ul>
							</li>
							<li class="divider-vertical"></li>
							<?php if( ! IS_USER ): ?>
							<li <?php echo ( get_get( 'act' ) == 'registration' ) ? 'class="active"' : null; ?>><a href="<?php echo BASE; ?>/registration/"><i class="icon-plus icon-white"></i> <?php echo $lang['register']; ?></a></li>
							<li class="divider-vertical"></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> <?php echo $lang['have-profile-login']; ?> <b class="caret"></b></a>
								<ul class="dropdown-menu dropdown-menu-login">
									<li>
										<form action="<?php echo BASE; ?>/login/" method="post">
											<fieldset>
												<label>
													<span><?php echo $lang['username']; ?></span><br />
													<input name="username" type="text" />
												</label>
												<label>
													<span><?php echo $lang['signup_pass']; ?></span>
													<input name="password" type="password" />
												</label>
											</fieldset>
											<div class="login-btn">
												<input type="submit" name="login" class="btn login-button btn-success" value="<?php echo $lang['login']; ?>" />
												<a class="recover-password-link" href="<?php echo HOME;?>/recover-password/"><?php echo $lang['recover-password-subject']; ?></a>
												<div class="clear"></div>
											</div>
										</form>
									</li>
								</ul>
							</li>
							<?php
							else:
							$user = $users->info();
							//$my_servers = $db->rows( $db->query('SELECT * FROM `servers` WHERE `creator_id` = ' . (int)get_cookie('user_id') ) );
							?>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-user icon-white"></i> <?php echo $user['username']; ?> <b class="caret"></b></a>
								<ul class="dropdown-menu">
									<?php if( $user[ 'admin_access' ] == 1 ): ?>
									<li><a href="<?php echo BASE; ?>/admin/"><i class="icon-wrench"></i> <?php echo $lang['user-admin-panel']; ?></a></li>
									<li class="divider"></li>
									<?php endif; ?>
									<li><a href="<?php echo BASE; ?>/add-page/"><i class="icon-plus"></i> <?php echo $lang['add_page']; ?></a></li>
									<li><a href="<?php echo BASE; ?>/add-server/"><i class="icon-plus"></i> Pievienot serveri</a></li>
									<li><a href="<?php echo BASE; ?>/my-sites/"><i class="icon-tasks"></i> <?php echo $lang['your_added_pages']; ?> (<strong><?php echo (int)$user['my_sites']; ?></strong>)</a></li>
									<!--<li><a href="<?php echo BASE; ?>/my-servers/"><i class="icon-th-list"></i> Mani pievienotie serveri (<strong><?php echo (int)$my_servers; ?></strong>)</a></li>-->
								</ul>
							</li>
							<li class="divider-vertical"></li>
							<li><a href="<?php echo BASE; ?>/logout/"><i class="icon-off icon-white"></i> <?php echo $lang['user-logout']; ?></a></li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div><!-- /.navbar-inner -->
		</div><!-- /.navbar navbar-inverse -->
		<div id="wrapper">
			<div class="left-side">
				<?php if( get_get( 'act' ) == 'registration' ): ?>
				<h1><i class="icon-info-sign"></i> <?php echo $lang['signup_rules']; ?></h1>
				<div class="left-content drop-shadow lifted" style="margin-bottom: 5px;">
					* <?php echo $lang['signup_rules-1']; ?><br />
					* <?php echo $lang['signup_rules-2']; ?><br />
					* <?php echo $lang['signup_rules-3']; ?><br />
					* <?php echo $lang['signup_rules-4']; ?>
				</div>
				<?php endif; ?>
				<?php if( get_get( 'act' ) == 'add-page' ): ?>
				<h1><i class="icon-info-sign"></i> <?php echo $lang['page-add-rules']; ?></h1>
				<div class="left-content drop-shadow lifted" style="margin-bottom: 5px;">
					<?php echo $lang['page-add-rules-content']; ?>
				</div>
				<?php endif; ?>
				<?php if( get_get( 'act' ) == 'contacts' ): ?>
				<h1><i class="icon-info-sign"></i> <?php echo $lang['info-side']; ?></h1>
				<div class="left-content drop-shadow lifted" style="margin-bottom: 5px;">
					<font color="red">*</font> <?php echo $lang['answer-24h']; ?>
				</div>
				<?php endif; ?>
				<?php if( get_get( 'act' ) == 'server-top' ): ?>
				<h1><i class="icon-info-sign"></i> <?php echo $lang['info-side']; ?></h1>
				<div class="left-content drop-shadow lifted" style="margin-bottom: 5px;">
					<?php echo $lang['serv-info']; ?>
				</div>
				<?php endif; ?>
				<?php if( get_get( 'act' ) == 'vote' ): ?>
				<h1><i class="icon-info-sign"></i> <?php echo $lang['info-side']; ?></h1>
				<div class="left-content drop-shadow lifted" style="margin-bottom: 5px;">
					<font color="red">*</font> <?php echo $lang['vote-for-page-one']; ?>
				</div>
				<?php endif; ?>
				<?php if( $_SERVER['REQUEST_URI'] == '/' OR get_get( 'act' ) == 'category' ): ?>
				<h1><i class="icon-folder-open"></i> <?php echo $lang['categories']; ?></h1>
				<div class="left-content drop-shadow lifted" style="margin-bottom: 5px;">
				<ul class="nav nav-tabs nav-stacked" style="margin-bottom: 1px;">
					<?php
					$cat_q = $db->query( "SELECT * FROM categories ORDER BY `id` ASC" );
					while( $cat = $db->fetch( $cat_q ) )
					{
						$pages_count = $db->rows( $db->query( 'SELECT * FROM pages WHERE `category` = ' . (int)$cat['id'] ) );
						
						if( get_get( 'act' ) == 'category' )
						{
							$active = ( $cat['id'] == (int)get_get( 'subact' ) ) ? 'class="active"' : null;
						}
					?>
					<li <?php echo $active; ?>><a href="<?php echo BASE ?>/category/<?php echo $cat['id']; ?>/<?php echo $cat['seostring']; ?>/"><span class="category-title"><i class="icon-hand-right"></i> <?php echo $cat['title']; ?> (<strong><?php echo (int)$pages_count; ?></strong>)</span></a></li>
					<?php
					}
					?>
				</ul>
				</div>
				<?php endif; ?>
				<h1><i class="icon-share-alt"></i> <?php echo WEBSITE_NAME; ?> @ Facebook</h1>
				<div class="left-content drop-shadow lifted">
					<div class="fb-page"
						data-href="https://www.facebook.com/boomtopsite" 
						data-width="340"
						data-hide-cover="false"
						data-show-facepile="true">
					</div>
				</div>
				<div id="fb-root"></div>
				<script>
					(function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  js.src = "//connect.facebook.net/lv_LV/sdk.js#xfbml=1&version=v2.5";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
				</script>
			</div>
			<div class="right-side">
				<?php echo $this->page_content; ?>
			</div>
			<div id="footer">
				<div class="left">
					&copy; <?php echo WEBSITE_NAME; ?> <?php echo date('Y'); ?> (<?php echo $lang['dev']; ?>: <a href="skype:kaspars.kreislers?chat">KasparsK</a>). <?php echo $lang['rights']; ?>
				</div>
				<div class="right">
					<a href="<?php echo BASE; ?>/rules/"><?php echo $lang['rules']; ?></a> - 
					<a href="<?php echo BASE; ?>/contacts/"><?php echo $lang['contacts']; ?></a>
				</div>
			</div><!-- /#footer -->
		</div><!-- /#wrapper -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			ga('create', 'UA-74595205-1', 'auto');
			ga('send', 'pageview');
		</script>
	</body>
</html>