function capLock(e){
	kc = e.keyCode?e.keyCode:e.which;
	sk = e.shiftKey?e.shiftKey:((kc == 16)?true:false);
	if(((kc >= 65 && kc <= 90) && !sk)||((kc >= 97 && kc <= 122) && sk))
	document.getElementById('divMayus').style.visibility = 'visible';
	else
	document.getElementById('divMayus').style.visibility = 'hidden';
}

function showhide(id){
  if(document.getElementById(id).style.display=="none")
    document.getElementById(id).style.display="block";
  else
    document.getElementById(id).style.display="none";
}

function search_in_top( type )
{
	var search_text = $('.search-page').val();
		
	window.location = base_url + '/search/' +  search_text + '/';
}

$(function() {
  $('.upload_type a').click(function() {
     var upload_class = $(this).attr('class');
     $('.upload_type .upload-item').hide();
     $('.upload_type #'+upload_class).show();
     return false;
  });
  
  	$('.btn').click(function()
	{
		if( $( this ).hasClass('disabled') )
		{
			return false;
		}
		
		$( this ).val('Lūdzu uzgaidiet...').addClass('disabled');
	});
});