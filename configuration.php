<?php
/*** Database settings ***/
define( 'DB_HOST', 'localhost' ); // MySQL hostname
define( 'DB_USER', 'boomtop' ); // MySQL username
define( 'DB_PASS', 'kd7H4LS8qlUF1Ssw' ); // MySQL password
define( 'DB_BASE', 'boomtop' ); // MySQL database

/*** Define base constants ***/
define( 'ROOT', dirname( __FILE__ ) );
define( 'NAME', basename( ROOT ) );
define( 'HOME','http' . ( ( !empty($_SERVER['HTTPS'] ) ) ? 's' : '') . '://' . (!empty($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : 'boomtop.net') );
define( 'BASE', HOME .  ( basename( $_SERVER['DOCUMENT_ROOT'] ) == NAME ? '' : '/' . NAME ) );
define( 'IP', isset( $_SERVER['HTTP_CF_CONNECTING_IP'] ) ? $_SERVER['HTTP_CF_CONNECTING_IP'] : (isset( $_SERVER['REMOTE_ADDR'] ) ? $_SERVER['REMOTE_ADDR'] : ''));

date_default_timezone_set( 'Europe/Riga' );
